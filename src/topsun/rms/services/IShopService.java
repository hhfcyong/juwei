package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.Item;
import topsun.rms.entity.Shop;
import topsun.rms.entity.UserRole;

/**
 * 自定义服务
 * 
 * @author zy
 *
 */
public interface IShopService extends IBaseService<Shop>
{
	public List<Shop> findShopByFilter(HqlFilter hqlFilter);
  
}
