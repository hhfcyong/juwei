package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.DocumentState;
import topsun.rms.entity.StorageReceipt;;

/**
 * 状态  Dao
 * 
 * @author Tang
 *
 */

@Repository("documentStateDao")  
@Transactional
public class DocumentStateDao extends BaseDao<DocumentState> {

}
