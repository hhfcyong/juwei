package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.DataDictionaryType;

/**
 * 数据字典类型 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("dataDictionaryTypeService")
public class DataDictionaryTypeService extends BaseService<DataDictionaryType>
{
	@Resource(name = "dataDictionaryTypeDao")  
	public void setDao(IBaseDao<DataDictionaryType> dao) 
	{  
		super.setDao(dao);  
	}
}
