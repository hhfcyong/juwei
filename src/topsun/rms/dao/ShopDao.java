package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Shop;

/**
 * 店铺 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("shopDao")
@Transactional  
public class ShopDao extends BaseDao<Shop> 
{
	
}
