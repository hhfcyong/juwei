<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    var entityid;
    var model = "add";
    var resourcetype;
    var resourceid;
    //表格界面调用
	var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model =="edit") {
				url = topsun.contextPath + '/imsbase/res/update.do';
			} else {
				url = topsun.contextPath + '/imsbase/res/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			
			if(typeof(resourceid)!="undefined") {
				jsonString =  jsonString  +',' +resourceid;
				}
		
			jsonString = jsonString +',' +resourcetype+'}';
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');		// close the dialog
					  $grid.datagrid('reload');	// reload the user data 		           
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	//表格界面调用(treeGrid)
	var treesubmitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if ( model =="edit") {
				url = topsun.contextPath + '/imsbase/res/update.do';
			} else {
				url = topsun.contextPath + '/imsbase/res/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			
			if(typeof(resourceid)!="undefined") {
				jsonString =  jsonString  +',' +resourceid;
				}
		
			jsonString = jsonString +',' +resourcetype+'}';
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');		// close the dialog
					  $grid.treegrid('reload');	// reload the user data 		           
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	var treeCancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.treegrid('reload');
	};
	var showIcons = function() {
		var dialog = parent.topsun.modalDialog({
			title : '浏览小图标',
			url : topsun.contextPath + '/style/icons.jsp',
			buttons : [ {
				text : '确定',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.selectIcon(dialog, $('#iconCls'));
				}
			} ]
		});
	};
	function getById(id)
    {
		 $.messager.progress({
				text : '数据加载中....'
			});
		$.post(topsun.contextPath + '/imsbase/res/getbyid.do', {
			id : id
		}, function(result) {
			var obj = result.obj;
			if (obj.id != undefined ) {
				if(model == "edit"){
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'url' : obj.url,
						'description' : obj.description,
						'iconCls' : obj.iconCls,
						'sortno' : obj.sortno,
						'code' : obj.code
					});
				}
				if(model == "copy"){
					$('form').form('load', {
						'name' : obj.name,
						'url' : obj.url,
						'description' : obj.description,
						'iconCls' : obj.iconCls,
						'sortno' : obj.sortno,
						'code' : obj.code
					});
				}
				$('#resourcetype').combobox('select', obj.resourcetype.id );
				if(obj.resource !=null){
					$('#resource_id').combotree('setValue', obj.resource.id );
				}
				
			}
			$.messager.progress('close');
		}, 'json');
    }
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		
		$('#resourcetype').combobox({
			onSelect:function(row){
				resourcetype = '"resourcetype":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#resource_id').combotree({
			onSelect:function(row){
				resourceid = '"resource":{"id":"'+ row.id+'"}' ;
			}
		});
		
	});
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%">
	<form method="post" class="form">
			<table class="table" style="width: 100%;font-size:12px;" cellpadding="5" >
				<tr>
					<th>编号</th>
					<td><input name="id"  /></td>
					<th>资源名称</th>
					<td><input name="name" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
					<th>编码</th>
					<td><input name="code" class="easyui-validatebox" data-options="required:true" /></td>
					<th>资源路径</th>
					<td><input name="url" /></td>
					
				</tr>
			 	<tr>
			 		<th>资源类型</th>
					<td><select  id="resourcetype"  class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/restype/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>上级资源</th>
					<td><select id="resource_id" class="easyui-combotree" data-options="editable:false,idField:'id',textField:'text',parentField:'pid',url:'<%=contextPath%>/imsbase/res/getMainMenu.do'" style="width: 155px;"></select><img class="iconImg ext-icon-cross" onclick="$('#resource_id').combotree('clear');" title="清空" /></td>
					
				</tr>
				 
				<tr>
					<th>资源图标</th>
					<td><input id="iconCls" name="iconCls"  style="padding-left: 18px; width: 134px;" /><img class="iconImg ext-icon-zoom" onclick="showIcons();" title="浏览图标" />&nbsp;<img class="iconImg ext-icon-cross" onclick="$('#iconCls').val('');$('#iconCls').attr('class','');" title="清空" /></td>
					<th>顺序</th>
					<td><input name="sortno" class="easyui-numberbox" data-options="required:true,min:0,max:100000" style="width: 155px;" value="100" /></td>
					
				</tr>
				<tr>
					<th>资源描述</th>
					<td><textarea name="description"></textarea></td>
				</tr>
			</table>
	</form>
</body>
</html>