package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.ResourceType;

/**
 * 资源类型Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("resourceTypeDao")
@Transactional  
public class ResourceTypeDao extends BaseDao<ResourceType> 
{

}
