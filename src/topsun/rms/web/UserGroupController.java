package topsun.rms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.UserGroup;
import topsun.rms.entity.UserRole;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IUserGroupService;
import topsun.rms.services.IUserRoleService;

/**
 * 用户组
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/usergroup")
public class UserGroupController extends BaseController<UserGroup>
{
	@Resource(name = "userGroupService")  
	public void setService(IBaseService<UserGroup> Service1) 
	{  
		super.setService(Service1);
	}
	
	@RequestMapping(value = "/grantrole", method = RequestMethod.POST)
	public void grandRole( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		IUserGroupService customService = (IUserGroupService)service;
		
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.grantRole(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/deleterole", method = RequestMethod.POST)
	public void deleteRole( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		IUserGroupService customService = (IUserGroupService)service;
		
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.deleteRole(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	
	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public void addUser( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IUserGroupService customService = (IUserGroupService)service;
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.addUser(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
	public void deleteUser( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IUserGroupService customService = (IUserGroupService)service;
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.deleteUser(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	
	@RequestMapping(value = "/getbyuserid", method = RequestMethod.POST)
	public void getUserGroupByUserId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_user#id_S_EQ", id);
		List<UserGroup> roles = ((IUserGroupService) service).findUserGroupByFilter(hqlFilter);
		writeJson(roles,request,response);
	}
	
	@RequestMapping(value = "/getbyuserroleid", method = RequestMethod.POST)
	public void getUserGroupByUserRoleId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_userrole#id_S_EQ", id);
		List<UserGroup> roles = ((IUserGroupService) service).findUserGroupByFilterRole(hqlFilter);
		writeJson(roles,request,response);
	}
}
