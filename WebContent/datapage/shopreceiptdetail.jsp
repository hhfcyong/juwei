<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
String storeReceiptId =request.getParameter("id");
String typeId =request.getParameter("typeid");
request.setAttribute("storeReceiptId", storeReceiptId);
request.setAttribute("typeId", typeId);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>门店收货明细</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	function reload() {
		$('#dgdetail').datagrid('load', topsun.serializeObject($('#mysearch')));
	}
	function formatObject(val, row) {
		return val.name;
	}
	function formatObjectId(val, row) {
		return val.itemcode;
	}
	function onDblClickRow(rowIndex, rowData) {
		topsun.editFun($('#dgdetail'), '/datapage/shopreceiptdetailform.jsp');
	}
	//排序时触发
	function onSortColumn(sort, order) {
		sortName = sort;
		sortOrder = order;
	}
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
	};
	$(function(){
		  var id = Number("${storeReceiptId}");
		  $('#dgdetail').datagrid({
              title:'收货单明细',
              fit:true,  
              fitColumns:true,  
              striped:true,
              iconCls:"yxb-icon datagrid-head",
              url:'shopreceiptdetail/list.do',
              queryParams:{'QUERY_t#storeReceipt.id_I_EQ':id},
              sortName: 'id',
              sortOrder: 'asc',
              remoteSort: false,
              pagination:true,
              pageNumber:1,
              pageSize:20,
              rownumbers:true,
              toolbar:"#toolbar",
              loadMsg:"数据正在加载,请耐心的等待..." 
           });	
	})
	
	
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dgdetail" SelectOnCheck="false" CheckOnSelect="false" singleSelect="true" data-options="onDblClickRow:onDblClickRow"
				>
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
					<th field="id" hidden="true"></th>
				    <th field="item" width="100" formatter="formatObjectId">单品编码</th>
					<th field="itemName" width="100" >单据名称</th>
					<th field="amount" width="100">收货金额</th>
					<th field="unitId" width="100" >单位</th>
					<th field="price" width="100" >单价</th>
					<th field="deliverQuantity" width="100" >发货数量</th>
					<th field="inStoreQuantity" width="100" >入库数</th>
					<th field="freezeQuantity" width="100" >冻结数</th>
					<th field="differenceQuantity" width="100" >差异数</th>
				</tr>
			</thead>
		</table>
	</div>
	
  	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('dgdetail'),'/datapage/shopreceiptdetailform.jsp?storeReceiptId=${storeReceiptId}&typeId=${typeId}')">添加</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.editFun($('#dgdetail'),'/datapage/shopreceiptdetailform.jsp')">编缉</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dgdetail'),'/datapage/shopreceiptdetail/remove.do')">删除</a>
	</div>
	
  </body>
</html>
