package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Promotion;
import topsun.rms.entity.User;
import topsun.rms.entity.UserRole;
import topsun.rms.entity.Item;;
/**
 * 员工服务实现
 * 
 * @author zy
 *
 */
@Service("promotionService")
public class PromotionService extends BaseService<Promotion> implements IPromotionService
{  
	@Resource(name = "itemDao")  
	private IBaseDao<Item> customDao;
	
	@Resource(name = "promotionDao")  
	public void setDao(IBaseDao<Promotion> dao) 
	{  
		super.setDao(dao);  
	}
	@Override
	public void addItem(int id, String userIds) 
	{
		Promotion promotion = getById(id);
		if (promotion != null) 
		{
			for (String userId : userIds.split(",")) 
			{
				if (StringUtils.isBlank(userId))  continue;
				
				Item item = customDao.getById(userId);
				if (item != null)
				{
					promotion.getItems().add(item);
				}
			}
			super.save(promotion);
		}
	}
	@Override
	public void deleteRole(int id,String roleIds)
	{
		Promotion promotion = getById(id);
		if(promotion != null)
		{
			for(String roleId :roleIds.split(",") )
			{
				if (StringUtils.isBlank(roleId))  continue;
				
				Item item = customDao.getById(roleId);
				if (item != null)
				{
					promotion.getItems().remove(item);
				}
			}
			super.save(promotion);
		}
	}
}
