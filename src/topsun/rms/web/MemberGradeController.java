package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.MemberGrade;
import topsun.rms.services.IBaseService;

/**
 * 会员等级 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("membership/membergrade")
public class MemberGradeController extends BaseController<MemberGrade>
{
	@Resource(name = "memberGradeService")  
	public void setService(IBaseService<MemberGrade> Service1) 
	{  
		super.setService(Service1);
	}
}
