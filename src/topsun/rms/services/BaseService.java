package topsun.rms.services;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;

/**
 *  定义IBaseService的通用操作的实现
 * 
 * @author yuxiaobei
 * 
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class BaseService<T> implements IBaseService<T> {
	
	
	private Class<T> clazz;

	/**
	 * 通过构造方法指定DAO的具体实现类
	 */
	public BaseService() 
	{
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		clazz = (Class<T>) type.getActualTypeArguments()[0];
	}
	
	/**
	 * 注入BaseDao
	 */
	private IBaseDao<T> baseDao;
	@Resource
	public void setDao(IBaseDao<T> dao)
	{
		this.baseDao = dao;
	}

	@Override
	public Serializable save(T o) 
	{
		return baseDao.save(o);
	}
	
	@Override
	public void delete(Serializable id)
	{
		baseDao.delete(id);
	}
	
	@Override
	public void delete(T o)
	{
		baseDao.delete(o);
	}

	@Override
	public void update(T o) 
	{
		baseDao.update(o);
	}
	
	@Override
	public void updateByParams(HqlFilter hqlFilter)
	{
		if(hqlFilter.getParams().size() ==0 || hqlFilter.getWhereHql().indexOf(" set ") < 0 ) return;
		
		String hql = "update " + clazz.getName() + " t " ;
		executeHql(hql + hqlFilter.getWhereHql(), hqlFilter.getParams());
	}
	
	@Override
	public void merge(T entity)
	{
		baseDao.merge(entity);
	}

	@Override
	public void saveOrUpdate(T o) 
	{
		baseDao.saveOrUpdate(o);
	}

	@Override
	public T getById(Serializable id) {
		return baseDao.getById(id);
	}

	@Override
	public T getByHql(String hql) {
		return baseDao.getByHql(hql);
	}

	@Override
	public T getByHql(String hql, Map<String, Object> params) {
		return baseDao.getByHql(hql, params);
	}
	
	@Override
	public T getByHql(String hql, Object... params)
	{
		return baseDao.getByHql(hql, params);
	}

	@Override
	public List<T> find() 
	{
		return baseDao.find();
	}

	@Override
	public List<T> find(String hql) {
		return baseDao.find(hql);
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params) 
	{
		return baseDao.find(hql, params);
	}
	
	@Override
	public List<T> find(String hql, Object... params) 
	{
		return baseDao.find(hql, params);
	}
	
	@Override
	public List<T> findByFilter(HqlFilter hqlFilter)
	{
		String hql = "select distinct t from " + clazz.getName() + " t";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}

	@Override
	public List<T> find(String hql, int page, int rows) {
		return baseDao.find(hql, page, rows);
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params, int page, int rows) {
		return baseDao.find(hql, params, page, rows);
	}
	
	@Override
	public List<T> find(int page, int rows, String hql, Object... params) 
	{
		return baseDao.find(page, rows,hql, params);
	}
	
	@Override
	public List<T> findByFilter(HqlFilter hqlFilter, int page, int rows)
	{
		String hql = "select distinct t from " + clazz.getName() + " t";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams(), page, rows);
	}

	@Override
	public Long count() 
	{
		return baseDao.count();
	}

	@Override
	public Long count(String hql) {
		return baseDao.count(hql);
	}

	@Override
	public Long count(String hql, Map<String, Object> params) {
		return baseDao.count(hql, params);
	}
	
	
	@Override
	public Long count(String hql, Object... params) 
	{
		return baseDao.count(hql, params);
	}
	
	@Override
	public Long countByFilter(HqlFilter hqlFilter)
	{
		String hql = "select count(distinct t) from " + clazz.getName() + " t";
		return count(hql + hqlFilter.getWhereHql(), hqlFilter.getParams());
	}

	@Override
	public int executeHql(String hql) 
	{
		return baseDao.executeHql(hql);
	}

	@Override
	public int executeHql(String hql, Map<String, Object> params) 
	{
		return baseDao.executeHql(hql, params);
	}

	@Override
	public List findBySql(String sql) 
	{
		return baseDao.findBySql(sql);
	}

	@Override
	public List findBySql(String sql, int page, int rows) 
	{
		return baseDao.findBySql(sql, page, rows);
	}

	@Override
	public List findBySql(String sql, Map<String, Object> params) 
	{
		return baseDao.findBySql(sql, params);
	}

	@Override
	public List findBySql(String sql, Map<String, Object> params, int page, int rows) 
	{
		return baseDao.findBySql(sql, params, page, rows);
	}

	@Override
	public int executeSql(String sql) {
		return baseDao.executeSql(sql);
	}

	@Override
	public int executeSql(String sql, Map<String, Object> params) {
		return baseDao.executeSql(sql, params);
	}
	
	@Override
	public int executeSql(String sql, Object... params) 
	{
		return baseDao.executeSql(sql, params);
	}

	@Override
	public int executeHql(String hql, Object... params) 
	{
		return baseDao.executeHql(hql, params);
	}

	@Override
	public BigInteger countBySql(String sql) {
		return baseDao.countBySql(sql);
	}

	@Override
	public BigInteger countBySql(String sql, Map<String, Object> params) {
		return baseDao.countBySql(sql, params);
	}

	@Override
	public BigInteger countBySql(String sql, Object... params) 
	{
		return baseDao.countBySql(sql, params);
	}

	@Override
	public Object UniqueResult(String hql) 
	{
		return baseDao.UniqueResult(hql);
	}

	@Override
	public Object UniqueResult(String hql, Map<String, Object> params)
	{
		return baseDao.UniqueResult(hql, params);
	}

	@Override
	public Object UniqueResult(String hql, Object... params)
	{
		return baseDao.UniqueResult(hql, params);
	}
}
