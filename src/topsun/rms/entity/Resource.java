package topsun.rms.entity;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
/**
 * 资源 实体模型
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Resource implements java.io.Serializable
{
	private String pid;// 虚拟属性，用于获得当前资源的父资源ID
	
	private String id;
	private String name;
	private String description;
	private String url;
	private String iconCls;
	private Long sortno;
	private String code;
	private ResourceType resourcetype;
	private Resource resource;
	private Set<Resource> resources = new HashSet<Resource>(0);
	private Set<UserRole> userroles = new HashSet<UserRole>(0);
	
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
//		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public Long getSortno() {
		return sortno;
	}
	public void setSortno(Long sortno) {
		this.sortno = sortno;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public ResourceType getResourcetype() {
		return resourcetype;
	}
	public void setResourcetype(ResourceType resourcetype) {
		this.resourcetype = resourcetype;
	}
	public Resource getResource() {
		return resource;
	}
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	public Set<UserRole> getUserroles() {
		return userroles;
	}
	public void setUserroles(Set<UserRole> userroles) {
		this.userroles = userroles;
	}
	
	public String getPid() 
	{
		if (resource != null && !StringUtils.isBlank(resource.getId())) 
		{
			return resource.getId();
		}
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}
}
