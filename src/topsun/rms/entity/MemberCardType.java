package topsun.rms.entity;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 会员卡类型
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class MemberCardType implements java.io.Serializable
{
	private String id;
	private String name;
	private int validdate;
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getValiddate() {
		return validdate;
	}
	public void setValiddate(int validdate) {
		this.validdate = validdate;
	}
	
}
