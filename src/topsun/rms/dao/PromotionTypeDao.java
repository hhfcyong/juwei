package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.PromotionType;;

/**
 * 资源类型Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("promotionTypeDao")
@Transactional  
public class PromotionTypeDao extends BaseDao<PromotionType> 
{

}
