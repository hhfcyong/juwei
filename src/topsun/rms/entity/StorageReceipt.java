package topsun.rms.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 仓库收货 实体模型
 * @author Tang
 *
 */

@SuppressWarnings("serial")
public class StorageReceipt implements java.io.Serializable
{
	private int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String storageReceiptNo; 
	private User deliver;
	private Date deliverDate;
	private String deliverDate2;
	
	public String getDeliverDate2() {
		return deliverDate2;
	}

	public void setDeliverDate2(String deliverDate2) {
		this.deliverDate2 = deliverDate2;
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setDeliverDate(formatter.parse(deliverDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getNewDate2() {
		return newDate2;
	}

	public void setNewDate2(String newDate2) {
		this.newDate2 = newDate2;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setNewDate(formatter.parse(newDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getAuditDate2() {
		return auditDate2;
	}

	public void setAuditDate2(String auditDate2) {
		this.auditDate2 = auditDate2;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setAuditDate(formatter.parse(auditDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Warehouse storage;

	private User receipt;
	private User creater;
	private Date newDate;
	private String newDate2;
	private String remark;
	private DocumentState documentState;

	private User audit;
	private Date auditDate;
	private String auditDate2;
	
	public String getStorageReceiptNo() {
		return this.storageReceiptNo;
	}
	
	public void setStorageReceiptNo(String storageReceiptNo) {
		this.storageReceiptNo=storageReceiptNo;
	}
	
	public User getDeliver() {
		return this.deliver;
	}
	
	public void setDeliver(User deliver) {
		this.deliver=deliver;
	}
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getDeliverDate() {
		return this.deliverDate;
	}
	
	public void setDeliverDate(Date deliverDate) {
		this.deliverDate=deliverDate;
	}
	
	public Warehouse getStorage() {
		return storage;
	}

	public void setStorage(Warehouse storage) {
		this.storage = storage;
	}
	
	public User getReceipt() {
		return this.receipt;
	}
	
	public void setReceipt(User receipt) {
		this.receipt=receipt;
	}
	
	public User getCreater() {
		return this.creater;
	}
	
	public void setCreater(User creater) {
		this.creater=creater;
	}
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getNewDate() {
		return this.newDate;
	}
	
	public void setNewDate(Date newDate) {
		this.newDate=newDate;
	}
	
	public String getRemark() {
		return this.remark;
	}
	
	public void setRemark(String remark) {
		this.remark=remark;
	}
	
	public DocumentState getDocumentState() {
		return this.documentState;
	}
	
	public void setDocumentState(DocumentState documentState) {
		this.documentState=documentState;
	}
	
	public User getAudit() {
		return this.audit;
	}
	
	public void setAudit(User audit) {
		this.audit=audit;
	}
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getAuditDate() {
		return this.auditDate;
	}
	
	public void setAuditDate(Date auditDate) {
		this.auditDate=auditDate;
	}
	
}
