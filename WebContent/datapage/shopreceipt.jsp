<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<% 
	String contextPath = request.getContextPath();
	SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>门店收货单</title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
		var selectform = function($dialog, $grid, userid,type) {
			if ($('form').form('validate')) {
				
				var arr =$('#dg').datagrid('getChecked');
				if(arr.length <=0){
					$.messager.show({
						title:'提示信息!',
						msg:'至少选择一行记录进行删除!'
					});
					return;
				}
				
				var ids = '';
				for(var i =0 ;i<arr.length;i++){
					ids += arr[i].id + ',' ;
				}
				var userid = userid;
			
				$.post(url ,
						{id:userid,ids:ids} , function(result){
						if (result.success){
							 $dialog.dialog('destroy');		 
							 $grid.datagrid('reload');	 	
						}else {
							$.messager.show({	 
								title: 'Error',
								msg: 'error'
							});
						}
				},'json');
			}
		};
		
		topsun.viewDetailFun = function($grid, uri) {
			var arr =$grid.datagrid('getSelections');
			if(arr.length != 1){
					$.messager.show({
						title:'提示信息!',
						msg:'只能选择一行记录!'
					});
			}else if(arr.length == 1){
				
			  var row = $grid.datagrid('getSelected');
			  if (row){
				  var dialog = parent.topsun.modalDialog({
						title : '门店收货单明细',
						iconCls : 'icon-edit',
						url : topsun.contextPath + uri+'?id='+row.id+'&typeid='+row.shop.shoptype,
						buttons : [ 
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $grid);
							}
						} ]
					});
			   }
			}
			
		};

		topsun.editStateFun = function($grid, uri, state){

			var arr =$grid.datagrid('getChecked');
			if(arr.length <=0){
				$.messager.show({
					title:'提示信息!',
					msg:'至少选择一行记录进行删除!'
				});
				return;
			} else {
				$.messager.confirm('提示信息' , '确认更改状态?' , function(r){
						if(r){
								var ids = '';
								for(var i =0 ;i<arr.length;i++){
									ids += arr[i].id + ',' ;
								}
								ids = ids.substring(0 , ids.length-1);
								$.post(topsun.contextPath + uri ,
										{ids:ids,idtype:'1','UPDATE_t#documentState#documentStateId_I_EQ':state,idname:'id'}, function(result){
										if (result.success){
											$grid.datagrid('reload');
											$grid.datagrid('unselectAll');
											$.messager.show({
												title:'提示信息!' , 
												msg:'操作成功!'
											});
										}else {
											$.messager.show({	// show error message
												title: 'Error',
												msg: 'error'
											});
										}
								},'json');
								
						} 
				});
			}
			
		};
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/datapage/shopreceiptform.jsp');
		}
				
		//formate date
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}

		//formate documentState
		function formatDocumentState(val,row){ return val.documentStateName; }
		//formate user
		function formatUser(val,row){ 
			if (val != undefined) {
				return val.name; 
			}
			else{
				return "";
			}
		}
		//formate storage
		function formatStorage(val,row){ return val.name; }
		function formatType(val,row){ return val.name; }
		function formatShop(val,row){ return val.name; }
		function formatStoreOrder(val,row){ 
			if (val != undefined) {
				return val.storageDeliverNo; 
			}
			else{
				return "";
			}
			
		}
		var Common = {			  

			    //EasyUI用DataGrid用日期格式化
			    DateFormatter: function (val, row) {
			        if (val == undefined) {
			            return "";
			        }			        

			        val = val.substr(0, val.length - 9);			        
			        return val;
			    }
			};
		$(function(){
			$('#selbarea').combobox({
				onSelect:function(row){
					
					 $('#selprovince').combobox('clear');
					 $('#selcity').combobox('clear');
					 $('#selcounty').combobox('clear');
					 $('#selshop').combobox('clear');
					 $('#selcity').combobox('loadData', '');
					 $('#selcounty').combobox('loadData', '');
					 
					 var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+row.code;
					 $('#selprovince').combobox('reload', url);
					
				}
			});
			$('#selprovince').combobox({
				onSelect:function(row){
					 
					 $('#selcity').combobox('clear');
					 $('#selcounty').combobox('clear');
					 $('#selshop').combobox('clear');
					 $('#selcounty').combobox('loadData', '');
					 
					 var url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+row.code;
					 $('#selcity').combobox('reload', url);
				}
			});
			
			$('#selcity').combobox({
				onSelect:function(row){
					 $('#selcounty').combobox('clear');
					 $('#selshop').combobox('clear');
					 
					 var url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+row.code;
					 $('#selcounty').combobox('reload', url);
				}
			});
			$('#selcounty').combobox({
				onSelect:function(row){
					 $('#selshop').combobox('clear');
					 var url = topsun.contextPath + '/datapage/shop/listbypcode.do?pcode='+row.code;
					 $('#selshop').combobox('reload', url);
				}
			});
		})
	</script>
</head>
<body class="easyui-layout" style="width: 100%;height:100%"  >
    <div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>搜索条件</legend>
			<form id="mysearch" method="post">
					单据号:<input name="QUERY_t#storeReceiptNo_S_LK" class="easyui-validatebox" value="" />
					状态:<select  name="QUERY_t#documentState#documentStateId_I_EQ"  class="easyui-combobox" 
								 data-options="valueField:'documentStateId',
											   textField:'documentStateName',
											   url:'<%=contextPath%>/document/state/listall.do',
											   panelHeight:'auto'" style="width: 155px;"></select>
					<p></p>											                        
					新建日:<input class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" name="QUERY_t#newDate_D_GE"></input>
						 <span>&nbsp;至  &nbsp;</span><input class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" name="QUERY_t#newDate_D_LE"></input>	
					审核日:<input class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" name="QUERY_t#auditDate_D_GE"></input> 
						 <span>&nbsp;至  &nbsp;</span><input class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" name="QUERY_t#auditDate_D_LE"></input>
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch').form('clear');reload();" >清空</a>
					<p></p>
					<span>&nbsp;&nbsp;</span>大区:<select id="selbarea" class="easyui-combobox"
						value="" class="easyui-combobox" data-options="editable:false,valueField:'code',
																					textField:'name',
																					url:'<%=contextPath%>/datapage/barea/listall.do'"  style="width: 155px;"></select>
					省份:<select id="selprovince" class="easyui-combobox"
						value="" class="easyui-combobox"  data-options="editable:false,valueField:'code',textField:'name'"  style="width: 155px;"></select>
					</span> <span style="margin-left: 2%;"> 市:<select id="selcity" data-options="editable:false,valueField:'code',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 155px;"></select>
					</span> <span> 城区:<select id="selcounty" data-options="editable:false,valueField:'code',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 155px;"></select>
					</span> <span> 门店:<select id="selshop" name="QUERY_t#shop.id_S_EQ" data-options="editable:false,valueField:'id',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"  style="width: 155px;"></select>
					</span>						 	 																																								
					
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="门店收货" class="easyui-datagrid" url="shopreceipt/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >			
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				<th field="id" hidden="true"></th>
				    <th field="storeReceiptNo" width="200">单据号</th>
					<th field="documentState" width="150" formatter="formatDocumentState">单据状态</th>
					<th field="storageDeliver" width="200" formatter="formatStoreOrder">发货单号</th>
					<th field="type" width="200" formatter="formatType">发货类型</th>
				    <th field="receiptUser" width="200" formatter="formatUser">收货人</th>
					<th field="receiptDate" width="250">收货日期</th>
					<th field="warehouse" width="200" formatter="formatStorage">发货方</th>
					<th field="shop" width="200" formatter="formatShop">接收门店</th>
				    <th field="newUser" width="250" formatter="formatUser">创建人</th>
					<th field="newDate" width="250" >创建日期</th>
				    <th field="auditUser" width="250" formatter="formatUser">审核人</th>
					<th field="auditDate" width="250">审核日期</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("ShopReceipt.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/datapage/shopreceiptform.jsp')">新增</a></td>
				<%}%>			
				<%if (securityUtil.havePermission("ShopReceipt.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/datapage/shopreceiptform.jsp')">编辑</a></td>
				<%}%>
				<%if (securityUtil.havePermission("ShopReceipt.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/datapage/shopreceiptform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("ShopReceipt.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.viewDetailFun($('#dg'),'/datapage/shopreceiptdetail.jsp')">查看明细</a></td>
				<%}%>
				<%if (securityUtil.havePermission("ShopReceipt.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dg'),'/datapage/shopreceipt/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("ShopReceipt.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.editStateFun($('#dg'),'/datapage/shopreceipt/updateByParams.do',5)">同意</a></td>
				<%}%>				
				<%if (securityUtil.havePermission("ShopReceipt.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.editStateFun($('#dg'),'/datapage/shopreceipt/updateByParams.do',4)">打回</a></td>
				<%}%>				
				<%if (securityUtil.havePermission("ShopReceipt.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editStateFun($('#dg'),'/datapage/shopreceipt/updateByParams.do',3)">撤消</a></td>
				<%}%>
			</tr>
		</table>		
	</div>	
</body>
</html>