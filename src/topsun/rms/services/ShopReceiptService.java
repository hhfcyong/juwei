package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.ShopReceipt;

/**
 * 自定义门店收货服务类
 * 
 * @author Tang
 *
 */
@Service("shopReceiptService")
public class ShopReceiptService  extends BaseService<ShopReceipt>
{
	@Resource(name = "shopReceiptDao")  
	public void setDao(IBaseDao<ShopReceipt> dao) 
	{  
		super.setDao(dao);  
	}
}
