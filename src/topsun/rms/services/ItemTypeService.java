package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.ItemType;

/**
 * 单品类型服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("itemTypeService")
public class ItemTypeService extends BaseService<ItemType>
{
	@Resource(name = "itemTypeDao")  
	public void setDao(IBaseDao<ItemType> dao) 
	{  
		super.setDao(dao);  
	}
}
