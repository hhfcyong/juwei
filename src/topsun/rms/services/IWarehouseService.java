package topsun.rms.services;

import topsun.rms.entity.Warehouse;


public interface IWarehouseService extends IBaseService<Warehouse> 
{
	/**
	 * 最大编号
	 * 
	 */
	public int getMaxCode();
	
}
