package topsun.rms.services;

import topsun.rms.entity.BArea;
import topsun.rms.entity.Bpca;

public interface IBpcaService extends IBaseService<BArea> 
{
	/**
	 * 获取所有区域信息
	 * 
	 * @return
	 */
	public Bpca getAllBpca();
}
