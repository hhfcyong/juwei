package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.County;

/**
 * 区县 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("countyDao")
@Transactional  
public class CountyDao extends BaseDao<County> 
{
	
}
