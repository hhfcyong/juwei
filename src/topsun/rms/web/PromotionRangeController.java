package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.PromotionRange;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("promotionpage/prorange")
public class PromotionRangeController extends BaseController<PromotionRange>
{
	@Resource(name = "promotionRangeService")  
	public void setService(IBaseService<PromotionRange> Service1) 
	{  
		super.setService(Service1);
	}
	
}
