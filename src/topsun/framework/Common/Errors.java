package topsun.framework.Common;

/**
 * 错误号
 * @author yuxiaobei
 *
 */
public class Errors 
{
	  public static final int OK = 0;
	  public static final int INTERNALERROR = 1;
	  public static final int ACCESSDENY = 2;
	  public static final int INVALIDDATA = 3;
}
