package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.StorageReceiptDetail;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("storage/receiptDetail")
public class StorageReceiptDetailController extends BaseController<StorageReceiptDetail> 
{
	@Resource(name = "storageReceiptDetailService")  
	public void setService(IBaseService<StorageReceiptDetail> Service1) 
	{  
		super.setService(Service1);		
	}
		
}