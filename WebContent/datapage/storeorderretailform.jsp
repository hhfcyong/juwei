<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	String storeOrderId =request.getParameter("storeOrderId");
	request.setAttribute("storeOrderId", storeOrderId);
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	request.setAttribute("currentUser", currentUser);
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var itemId;
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/storeorderretail/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/storeorderretail/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			var storeOrderId = $("#storeOrderId").val();
			if(storeOrderId != '')
				jsonString =  jsonString  +',' +'"storeOrder":{"id":"'+ storeOrderId+'"}';
			if(typeof(itemId)!="undefined") 
				jsonString =  jsonString  +',' +itemId;
			
			jsonString = jsonString + "}";
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	$.messager.alert("提示", "保存成功！", "info", function () {
		        		$dialog.dialog('destroy');	
		        		$grid.datagrid('reload');
		        	});
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$('.easyui-numberbox').attr("disabled","disabled");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		$("#amount").blur(function(){
			var price  = parseFloat($("#price").val());
			var amount  = Number($("#amount").val());
			$("#orderQuantity").val(price*amount);
		});
		$('#itemId').combobox({
			onSelect:function(row){
				 $.post(topsun.contextPath + '/datapage/item/getbyid.do', {
						id : row.id
					}, function(result) {
						var obj = result.obj;
						if (obj.id != undefined) {
							$('form').form('load', {
								'itemName' : obj.name,
								'unitName' : obj.unit,
								'price' : obj.costprice
							});
							var price  = parseFloat($("#price").val());
							var amount  = Number($("#amount").val());
							$("#orderQuantity").val(price*amount);
						}
					}, 'json');
				 itemId = '"item":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
		 });
		 $.post(topsun.contextPath + '/datapage/storeorderretail/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'itemName' : obj.itemName,
						'unitName' : obj.unitName,
						'price' : obj.price,
						'orderQuantity':obj.orderQuantity,
						'amount':obj.amount
					});
					 $("#itemId").combobox('select',obj.item.id);
					 $("#storeOrderId").val(obj.storeOrder.id);
					 $(":input[name='id']").val(obj.id);
				     $.messager.progress('close');
				}
				if(model =="copy"){
					$('.easyui-numberbox').val('');
					entityid = "";
				}
				
			}, 'json');
		
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div  data-options="border:false"  >
			<form class="form" method="post">
				<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
				    <input type="hidden" name="id" />
					<th>单据编码：</th>
					<td><select id="itemId" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'itemcode',url:'<%=contextPath%>/datapage/item/listall.do'"  style="width: 155px;"></select></td>
					<th>单据名称：</th>
					<td><input name="itemName" class="easyui-validatebox"  value=""  /></td>
				</tr>
				<tr>
					<th>订货数量：</th>
					<td><input id="amount" precision="0" name="amount" class="easyui-numberbox"  value="1"  /></td>
					<th>单位：</th>
					<td><input  name="unitName" class="easyui-validatebox"  value=""   /></td>
				</tr>
				<tr>
					<th>订货价：</th>
					<td><input id="price" name="price" class="easyui-validatebox"  value=""  readonly="readonly"></td>
					<th>订货金额：</th>
					<td><input class="easyui-validatebox" id="orderQuantity" name="orderQuantity"  value="" />
				</tr>
				<input value="${storeOrderId}" type="hidden" id="storeOrderId">
				
			</table>
			</form>
	</div>
	
</body>
</html>