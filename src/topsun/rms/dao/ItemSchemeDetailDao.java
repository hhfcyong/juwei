package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.ItemSchemeDetail;

/**
 * 预定义方案明细  Dao
 * 
 * @author Tang
 *
 */

@Repository("itemSchemeDetailDao")  
@Transactional
public class ItemSchemeDetailDao extends BaseDao<ItemSchemeDetail> 
{

}
