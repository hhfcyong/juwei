package topsun.rms.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class StoreOrder implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String storeOrderNo;
	private Type type;
	private Shop shop;
	private Date orderDate;
	private String orderDate2;
	private User createUser;
	private User orderUser;
	private User auditUser;
	private Date newDate;
	private String newDate2;
	private String remark;
	private Date auditDate;
	private String auditDate2;
	private DocumentState documentState;
	private Set<StoreOrderRetail> storeOrderRetails = new HashSet<StoreOrderRetail>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStoreOrderNo() {
		return storeOrderNo;
	}
	public void setStoreOrderNo(String storeOrderNo) {
		this.storeOrderNo = storeOrderNo;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public User getCreateUser() {
		return createUser;
	}
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	public User getOrderUser() {
		return orderUser;
	}
	public void setOrderUser(User orderUser) {
		this.orderUser = orderUser;
	}
	public Date getNewDate() {
		return newDate;
	}
	public void setNewDate(Date newDate) {
		this.newDate = newDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public User getAuditUser() {
		return auditUser;
	}
	public void setAuditUser(User auditUser) {
		this.auditUser = auditUser;
	}
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public DocumentState getDocumentState() {
		return documentState;
	}
	public void setDocumentState(DocumentState documentState) {
		this.documentState = documentState;
	}
	public Set<StoreOrderRetail> getStoreOrderRetails() {
		return storeOrderRetails;
	}
	public void setStoreOrderRetails(Set<StoreOrderRetail> storeOrderRetails) {
		this.storeOrderRetails = storeOrderRetails;
	}
	
	public String getOrderDate2() {
		return orderDate2;
	}
	public void setOrderDate2(String orderDate2) {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setOrderDate(formatter.parse(orderDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.orderDate2 = orderDate2;
	}
	public String getNewDate2() {
		return newDate2;
	}
	public void setNewDate2(String newDate2) {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setNewDate(formatter.parse(newDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.newDate2 = newDate2;
	}
	public String getAuditDate2() {
		return auditDate2;
	}
	public void setAuditDate2(String auditDate2) {
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setAuditDate(formatter.parse(auditDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.auditDate2 = auditDate2;
	}
	
}
