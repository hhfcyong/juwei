package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.StorageDeliver;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("storage/deliver")
public class StorageDeliverController extends BaseController<StorageDeliver> 
{
	@Resource(name = "storageDeliverService")  
	public void setService(IBaseService<StorageDeliver> Service1) 
	{  
		super.setService(Service1);		
	}
}
