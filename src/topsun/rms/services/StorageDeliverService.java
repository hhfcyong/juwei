package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.StorageDeliver;
import topsun.rms.entity.StorageReceipt;

/**
 * 自定义仓库收货服务类
 * 
 * @author Tang
 *
 */
@Service("storageDeliverService")
public class StorageDeliverService extends BaseService<StorageDeliver>
{
	@Resource(name = "storageDeliverDao")  
	public void setDao(IBaseDao<StorageDeliver> dao) 
	{  
		super.setDao(dao);  
	}
}
