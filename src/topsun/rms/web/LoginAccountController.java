package topsun.rms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.LoginAccount;
import topsun.rms.services.IBaseService;

/**
 * 登录账号
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/loginaccount")
public class LoginAccountController extends BaseController<LoginAccount> 
{
	@Resource(name = "loginAccountService")  
	public void setService(IBaseService<LoginAccount> Service1) 
	{  
		super.setService(Service1);
	}
	
	@Override
	public void save(HttpEntity<LoginAccount> model, HttpServletRequest request, HttpServletResponse response)
	{
		LoginAccount lg = model.getBody();
		lg.setName(lg.getId());
		super.save(model, request, response);
	}
	
	
	@RequestMapping(value = "/getbyuserid", method = RequestMethod.POST)
	public void getAccountByUserId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_user#id_S_EQ", id.toString());
		List<LoginAccount> loginAccounts = service.findByFilter(hqlFilter);
		writeJson(loginAccounts,request,response);
	}
}
