package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.StoreOrder;
import topsun.rms.entity.Type;
@Repository("storeOrderDao")
public class StoreOrderDao extends BaseDao<StoreOrder> {

}
