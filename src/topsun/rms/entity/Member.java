package topsun.rms.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 会员
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Member implements java.io.Serializable
{
	private String id;
	private String name;
	private String code;
	private int sex;
	private Date birthday;
	private String agegroup;
	private String mobile;
	private String phone;
	private String qq;
	private String microletter;
	private String othercontactway;
	private String address;
	private String email;
	private String idnumber;
	private String createora;
	private String createuser;
	private Date createdate;
    private int point;	
    private String pw;
    private MemberGrade membergrade;
	private Set<MemberCard> membercards = new HashSet<MemberCard>(0);
	
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getAgegroup() {
		return agegroup;
	}
	public void setAgegroup(String agegroup) {
		this.agegroup = agegroup;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getMicroletter() {
		return microletter;
	}
	public void setMicroletter(String microletter) {
		this.microletter = microletter;
	}
	public String getOthercontactway() {
		return othercontactway;
	}
	public void setOthercontactway(String othercontactway) {
		this.othercontactway = othercontactway;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIdnumber() {
		return idnumber;
	}
	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}
	public String getCreateora() {
		return createora;
	}
	public void setCreateora(String createora) {
		this.createora = createora;
	}
	public String getCreateuser() {
		return createuser;
	}
	public void setCreateuser(String createuser) {
		this.createuser = createuser;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public MemberGrade getMembergrade() {
		return membergrade;
	}
	public void setMembergrade(MemberGrade membergrade) {
		this.membergrade = membergrade;
	}
	public Set<MemberCard> getMembercards() {
		return membercards;
	}
	public void setMembercards(Set<MemberCard> membercards) {
		this.membercards = membercards;
	}
	
	
}
