package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.Resource;
import topsun.rms.entity.User;

/**
 * 资源服务
 * 
 * @author yuxiaobei
 *
 */
public interface IResourceService extends IBaseService<Resource>
{
	/**
	 * 获得资源树，或者combotree(只查找菜单类型的节点)
	 * 
	 * @param hqlFilter
	 * 					过滤条件
	 * @return
	 * 			List
	 */
	public List<Resource> getMainMenu(HqlFilter hqlFilter);
	
	/**
	 * 获得资源树，或者combotree(只查找菜单类型的节点)
	 * 
	 * @param hqlFilter
	 * 					过滤条件
	 * @return
	 * 			List
	 */
	public List<Resource> getMenu(HqlFilter hqlFilter);
	
	
	/**
	 * 通过过滤条件（Roleid）  获取资源
	 * 
	 * @param hqlFilter
	 * 			过滤条件
	 * @return
	 */
	public List<Resource> findResByFilter(HqlFilter hqlFilter); 
}
