package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Employee;

/**
 * 员工 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("employeeDao")
@Transactional  
public class EmployeeDao extends BaseDao<Employee> 
{
	
}
