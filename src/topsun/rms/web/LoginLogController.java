package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.LoginLog;
import topsun.rms.services.IBaseService;

/**
 * 
 * 登录日志 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/loginlog")
public class LoginLogController extends BaseController<LoginLog>
{
	@Resource(name = "loginLogService")  
	public void setService(IBaseService<LoginLog> Service1) 
	{  
		super.setService(Service1);
	}
	
}
