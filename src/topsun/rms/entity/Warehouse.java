package topsun.rms.entity;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 仓库
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Warehouse implements java.io.Serializable
{
	private String id;
	private String name;
	private int code;
//	private String department;
	private String phone;
	private String address;
	
	private Oranization oranization;
	
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
//	public String getDepartment() {
//		return department;
//	}
//	public void setDepartment(String department) {
//		this.department = department;
//	}
	public Oranization getOranization() {
		return oranization;
	}
	public void setOranization(Oranization oranization) {
		this.oranization = oranization;
	}
	
}
