package topsun.rms.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 预定义方案
 * 
 * @author Tang
 *
 */
@SuppressWarnings("serial")
public class ItemScheme {
	private int id;
	private String itemSchemeNo;
	private String itemSchemeName;
	private DocumentState documentState;
	private User formulateUser;
	private Date formulateDate;
	private String formulateDateString;
	private Date startDate;
	private String startDateString;
	private Date endDate;
	private String endDateString;
	private int automatic;
	private int frequency;
	private Date executionDate;
	private String executionDateString;
	private County areaId;
	private String remark;
	private User auditUser;
	private Date auditDate;
	private String auditDateString;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemSchemeNo() {
		return itemSchemeNo;
	}
	public void setItemSchemeNo(String itemSchemeNo) {
		this.itemSchemeNo = itemSchemeNo;
	}
	public String getItemSchemeName() {
		return itemSchemeName;
	}
	public void setItemSchemeName(String itemSchemeName) {
		this.itemSchemeName = itemSchemeName;
	}
	public DocumentState getDocumentState() {
		return documentState;
	}
	public void setDocumentState(DocumentState documentState) {
		this.documentState = documentState;
	}
	public User getFormulateUser() {
		return formulateUser;
	}
	public void setFormulateUser(User formulateUser) {
		this.formulateUser = formulateUser;
	}
	public Date getFormulateDate() {
		return formulateDate;
	}
	public void setFormulateDate(Date formulateDate) {
		this.formulateDate = formulateDate;
	}
	public String getFormulateDateString() {
		return formulateDateString;
	}
	public void setFormulateDateString(String formulateDateString) {
		this.formulateDateString = formulateDateString;

		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setFormulateDate(formatter.parse(formulateDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getStartDateString() {
		return startDateString;
	}
	public void setStartDateString(String startDateString) {
		this.startDateString = startDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setStartDate(formatter.parse(startDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getEndDateString() {
		return endDateString;
	}
	public void setEndDateString(String endDateString) {
		this.endDateString = endDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setEndDate(formatter.parse(endDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public int getAutomatic() {
		return automatic;
	}
	public void setAutomatic(int automatic) {
		this.automatic = automatic;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public Date getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	public String getExecutionDateString() {
		return executionDateString;
	}
	public void setExecutionDateString(String executionDateString) {
		this.executionDateString = executionDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setExecutionDate(formatter.parse(executionDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public County getAreaId() {
		return areaId;
	}
	public void setAreaId(County areaId) {
		this.areaId = areaId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public User getAuditUser() {
		return auditUser;
	}
	public void setAuditUser(User auditUser) {
		this.auditUser = auditUser;
	}
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public String getAuditDateString() {
		return auditDateString;
	}
	public void setAuditDateString(String auditDateString) {
		this.auditDateString = auditDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setAuditDate(formatter.parse(auditDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
