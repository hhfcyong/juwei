<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<% 
String contextPath = request.getContextPath();
SecurityUtil securityUtil = new SecurityUtil(session);
%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>预定义要货方案</title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
		var selectform = function($dialog, $grid, userid,type) {
			if ($('form').form('validate')) {
				
				var arr =$('#dg').datagrid('getChecked');
				if(arr.length <=0){
					$.messager.show({
						title:'提示信息!',
						msg:'至少选择一行记录进行删除!'
					});
					return;
				}
				
				var ids = '';
				for(var i =0 ;i<arr.length;i++){
					ids += arr[i].id + ',' ;
				}
				var userid = userid;
			
				$.post(url ,
						{id:userid,ids:ids} , function(result){
						if (result.success){
							 $dialog.dialog('destroy');		 
							 $grid.datagrid('reload');	 	
						}else {
							$.messager.show({	 
								title: 'Error',
								msg: 'error'
							});
						}
				},'json');
			}
		};
		
		topsun.viewDetailFun = function($grid, uri) {
			var arr =$grid.datagrid('getSelections');
			if(arr.length != 1){
					$.messager.show({
						title:'提示信息!',
						msg:'只能选择一行记录!'
					});
			}else if(arr.length == 1){
				
			  var row = $grid.datagrid('getSelected');
			  if (row){
				  var dialog = parent.topsun.modalDialog({
						title : '预定义要货明细',
						iconCls : 'icon-edit',
						url : topsun.contextPath + uri+'?id='+row.id,
						buttons : [ 
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $grid);
							}
						} ]
					});
			   }
			}
			
		};
		topsun.editStateFun = function($grid, uri, state){

			var arr =$grid.datagrid('getChecked');
			if(arr.length <=0){
				$.messager.show({
					title:'提示信息!',
					msg:'至少选择一行记录进行删除!'
				});
				return;
			} else {
				$.messager.confirm('提示信息' , '确认更改状态?' , function(r){
						if(r){
								var ids = '';
								for(var i =0 ;i<arr.length;i++){
									ids += arr[i].id + ',' ;
								}
								ids = ids.substring(0 , ids.length-1);
								$.post(topsun.contextPath + uri ,
										{ids:ids,idtype:'1','UPDATE_t#documentState#documentStateId_I_EQ':state,idname:'id'}, function(result){
										if (result.success){
											$grid.datagrid('reload');
											$grid.datagrid('unselectAll');
											$.messager.show({
												title:'提示信息!' , 
												msg:'操作成功!'
											});
										}else {
											$.messager.show({	// show error message
												title: 'Error',
												msg: 'error'
											});
										}
								},'json');
								
						} 
				});
			}
			
		};
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/datapage/itemschemeform.jsp');
		}

		//formate documentState
		function formatDocumentState(val,row){ return val.documentStateName; }
		
		function formatArea(val,row){ return val.name; }
		
		//formate user
		function formatUser(val,row){ 
			if (val != undefined) {
				return val.name; 
			}
			else{
				return "";
			}
		}
				
		//formate date
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
		
		//formate storage
		function formatAutomatic(automatic){ 
			if (automatic == undefined) {
	            return "";
	        }else if(automatic==1){
	        	return "否";
	        }else if(automatic==2){
	        	return "是"; 
	        }		
		}
			
		var Common = {			  

			    //EasyUI用DataGrid用日期格式化
			    DateFormatter: function (val, row) {
			        if (val == undefined) {
			            return "";
			        }			        

			        val = val.substr(0, val.length - 9);			        
			        return val;
			    }
			};
		

		$(function(){
			$('#selbarea').combobox({
				onSelect:function(row){
					
					 $('#selprovince').combobox('clear');
					 $('#selcity').combobox('clear');
					 $('#selcounty').combobox('clear');
					 $('#selcity').combobox('loadData', '');
					 $('#selcounty').combobox('loadData', '');
					 
					 var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+row.code;
					 $('#selprovince').combobox('reload', url);
					
				}
			});
			$('#selprovince').combobox({
				onSelect:function(row){
					 
					 $('#selcity').combobox('clear');
					 $('#selcounty').combobox('clear');
					 $('#selcounty').combobox('loadData', '');
					 
					 var url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+row.code;
					 $('#selcity').combobox('reload', url);
				}
			});
			
			$('#selcity').combobox({
				onSelect:function(row){
					 $('#selcounty').combobox('clear');
					 
					 var url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+row.code;
					 $('#selcounty').combobox('reload', url);
				}
			});			
		})
		
	</script>
</head>
<body class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>搜索条件</legend>
				<form id="mysearch" method="post">
					方案号:<input name="QUERY_t#itemSchemeNo_S_LK" class="easyui-validatebox" value="" />
					方案名:<input name="QUERY_t#itemSchemeName_S_LK" class="easyui-validatebox" value="" />
	                                                        制定日期:<input class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" name="QUERY_t#formulateDate_D_GE"></input>
						 <span>&nbsp;至  &nbsp;</span><input class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" name="QUERY_t#formulateDate_D_LE"></input> 
					<br><br>
					
					<span>大区:</span> <span style="margin-left: 0.5%;"> 
					<select id="selbarea" class="easyui-combobox"
						value="" class="easyui-combobox" data-options="editable:false,valueField:'code',
																					textField:'name',
																					url:'<%=contextPath%>/datapage/barea/listall.do'"  style="width: 155px;"></select>
					</span> <span style="margin-left: 1%;"> 
					<label style="width: 100px;">省份:</label><select id="selprovince" class="easyui-combobox"
						value="" class="easyui-combobox"  data-options="editable:false,valueField:'code',textField:'name'"  style="width: 155px;"></select>
					</span> <span style="margin-left: 2.2%;"> 地市:<select id="selcity" data-options="editable:false,valueField:'code',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 155px;"></select>
					</span> <span> 城区:<select id="selcounty" name="QUERY_t#areaId.id_I_EQ" data-options="editable:false,valueField:'id',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 155px;"></select>	 																																								
						<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
						<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch').form('clear');reload();" >清空</a>
				</form>
		</fieldset>
	</div>
	
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="预定义要货" class="easyui-datagrid" url="itemscheme/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >			
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
					<th field="id" hidden="true"></th>
				    <th field="itemSchemeNo" width="200">方案号</th>
				    <th field="itemSchemeName" width="200">方案名</th>
					<th field="documentState" width="150" formatter="formatDocumentState">单据状态</th>
				    <th field="areaId" width="200" formatter="formatArea">使用城区</th>
				    <th field="formulateUser" width="200" formatter="formatUser">制定人</th>
					<th field="formulateDate" width="250">制定日期</th>
					<th field="startDate" width="250">开始日期</th>
					<th field="endDate" width="250">结束日期</th>
				    <th field="automatic" width="150" formatter="formatAutomatic">是否自动执行</th>
					<th field="frequency" width="150" >执行频率</th>
					<th field="executionDate" width="250">执行时间</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%//if (securityUtil.havePermission("ItemScheme.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/datapage/itemschemeform.jsp')">新增</a></td>
				<%//}%>			
				<%//if (securityUtil.havePermission("ItemScheme.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/datapage/itemschemeform.jsp')">编辑</a></td>
				<%//}%>
				<%//if (securityUtil.havePermission("ItemScheme.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/datapage/itemschemeform.jsp')">查看</a></td>
				<%//}%>
				<%//if (securityUtil.havePermission("ItemScheme.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.viewDetailFun($('#dg'),'/datapage/itemschemedetail.jsp')">查看明细</a></td>
				<%//}%>
				<%//if (securityUtil.havePermission("ItemScheme.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dg'),'/datapage/itemscheme/remove.do')">删除</a></td>
				<%//}%>
				
				<%if (securityUtil.havePermission("ItemScheme.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.editStateFun($('#dg'),'/datapage/itemscheme/updateByParams.do',5)">同意</a></td>
				<%}%>				
				<%if (securityUtil.havePermission("ItemScheme.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.editStateFun($('#dg'),'/datapage/itemscheme/updateByParams.do',4)">打回</a></td>
				<%}%>				
				<%if (securityUtil.havePermission("ItemScheme.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editStateFun($('#dg'),'/datapage/itemscheme/updateByParams.do',3)">撤消</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
</body>
</html>