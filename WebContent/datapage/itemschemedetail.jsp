<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
String itemSchemeId =request.getParameter("id");
request.setAttribute("itemSchemeId", itemSchemeId);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">

	function reload() {
		$('#dgDetail').datagrid('load', topsun.serializeObject($('#mysearch')));
	}
	function formatObject(val, row) {
		return val.name;
	}
	function formatObjectId(val, row) {
		return val.itemcode;
	}

	function onDblClickRow(rowIndex, rowData) {
		topsun.editFun($('#dgDetail'), '/datapage/itemschemedetailform.jsp');
	}
	//排序时触发
	function onSortColumn(sort, order) {
		sortName = sort;
		sortOrder = order;
	}
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
	};
	$(function(){
		  var id = Number("${itemSchemeId}");
		  $('#dgDetail').datagrid({
	          title:'预定义方案要货明细',
	          fit:true,  
	          fitColumns:true,  
	          striped:true,
	          iconCls:"yxb-icon datagrid-head",
	          url:'itemschemedetail/list.do',
	          queryParams:{'QUERY_t#itemScheme.id_I_EQ':id},
	          sortName: 'id',
	          sortOrder: 'asc',
	          remoteSort: false,
	          pagination:true,
	          pageNumber:1,
	          pageSize:20,
	          rownumbers:true,
	          toolbar:"#toolbar",
	          loadMsg:"数据正在加载,请耐心的等待..." 
	       });	
	})
</script>
</head>
<body class="easyui-layout" style="width: 100%;height:100%" >
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
    	  <table id="dgDetail" SelectOnCheck="false" CheckOnSelect="false" singleSelect="true" data-options="onDblClickRow:onDblClickRow">
    	  
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
				<thead>
					<tr>
						<th field="id" hidden="true"></th>				    
						<th field="item" width="250" formatter="formatObjectId">单品编码</th>
						<th field="itemName" width="200">单品名称</th>
					    <th field="quantity" width="200">要货数量</th>				    
						<th field="unitId" width="200">单位</th>
						<th field="tradePrice" width="200">批经价</th>
					    <th field="costPrice" width="200">成本价</th>
					</tr>
				</thead>
			</table>
        </div>
  	
  	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dgDetail'),'/datapage/itemschemedetailform.jsp?itemSchemeId=${itemSchemeId}')">添加</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.editFun($('#dgDetail'),'/datapage/itemschemedetailform.jsp')">编缉</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.viewFun($('#dgDetail'),'/datapage/itemschemedetailform.jsp')">查看</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dgDetail'),'/datapage/itemschemedetail/remove.do')">删除</a>
	</div>

</body>
</html>