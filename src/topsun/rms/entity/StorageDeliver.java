package topsun.rms.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 仓库发货 实体模型
 * @author Tang
 *
 */
@SuppressWarnings("serial")
public class StorageDeliver 
{
	private int id;
	private String storageDeliverNo;
	private Warehouse warehouse;
	private User deliverUser;
	private Date deliverDate;
	private String deliverDateString;
	private Shop shop;
	private User newUser;
	private Date newDate;
	private String newDateString;
	private String remark;
	private DocumentState documentState;
	private Type type;
	private User auditUser;
	private Date auditDate;
	private String auditDateString;
	private StoreOrder storeOrder;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStorageDeliverNo() {
		return storageDeliverNo;
	}
	public void setStorageDeliverNo(String storageDeliverNo) {
		this.storageDeliverNo = storageDeliverNo;
	}
	public Warehouse getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	public User getDeliverUser() {
		return deliverUser;
	}
	public void setDeliverUser(User deliverUser) {
		this.deliverUser = deliverUser;
	}
	
	public Date getDeliverDate() {
		return deliverDate;
	}
	public void setDeliverDate(Date deliverDate) {
		this.deliverDate = deliverDate;
	}

	public String getDeliverDateString() {
		return deliverDateString;
	}
	public void setDeliverDateString(String deliverDateString) {
		this.deliverDateString = deliverDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setDeliverDate(formatter.parse(deliverDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public User getNewUser() {
		return newUser;
	}
	public void setNewUser(User newUser) {
		this.newUser = newUser;
	}
	public Date getNewDate() {
		return newDate;
	}
	public void setNewDate(Date newDate) {
		this.newDate = newDate;
	}
	public String getNewDateString() {
		return newDateString;
	}
	public void setNewDateString(String newDateString) {
		this.newDateString = newDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setNewDate(formatter.parse(newDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public DocumentState getDocumentState() {
		return documentState;
	}
	public void setDocumentState(DocumentState documentState) {
		this.documentState = documentState;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public User getAuditUser() {
		return auditUser;
	}
	public void setAuditUser(User auditUser) {
		this.auditUser = auditUser;
	}
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public String getAuditDateString() {
		return auditDateString;
	}
	public void setAuditDateString(String auditDateString) {
		this.auditDateString = auditDateString;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setAuditDate(formatter.parse(auditDateString));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public StoreOrder getStoreOrder() {
		return storeOrder;
	}
	public void setStoreOrder(StoreOrder storeOrder) {
		this.storeOrder = storeOrder;
	}
}
