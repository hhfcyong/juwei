<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    
    var model = "add";
    var entityid;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/imsbase/role/update.do';
				
			} else {
				url = topsun.contextPath + '/imsbase/role/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  if (model == "add"){
			        		 getById(data.obj);
			        	 }
			        	 else{
			        		 getById(entityid );
		        		 }
						 model = 'edit';
						 var bSuccess = addResFun1();
							 
						
		          },   
		          error : function(data) {   
		        	  parent.$.messager.show({
							title: '提示',
							msg: '操作失败'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	var addRoleFun = function() {
		if ($(':input[name="id"]').val().length <= 0) {
			$.messager.show({
				title: '提示',
				msg: '请先保存用户'
			});
			return;
		}
      var id =''+ $(':input[name="id"]').val();
	  var dialog1 = parent.topsun.modalDialog({
			title : '选择信息',
			url : topsun.contextPath + '/imsbase/usermanager.jsp',
			buttons : [ {
				text : '选择',
				iconCls : 'icon-edit',
				handler : function() {
					dialog1.find('iframe').get(0).contentWindow.selectform(dialog1, $('#dguserrole'), id,'role');
				}
			} ]
		});
	};
	
	var removeRoleFun = function() {
		var id = $(':input[name="id"]').val();
		var arr =$('#dguserrole').datagrid('getSelections');
		if(arr.length <=0){
			$.messager.show({
				title:'提示信息!',
				msg:'至少选择一行记录进行删除!'
			});
			return;
		} else {
			$.messager.confirm('提示信息' , '确认删除?' , function(r){
					if(r){
							var ids = '';
							for(var i =0 ;i<arr.length;i++){
								ids += arr[i].id + ',' ;
							}
							ids = ids.substring(0 , ids.length-1);
							$.post(topsun.contextPath + '/imsbase/role/deleteuser.do' ,
									{id:id,ids:ids} , function(result){
									if (result.success){
										$('#dguserrole').datagrid('reload');
										$('#dguserrole').datagrid('unselectAll');
									}else {
										$.messager.show({	 
											title: 'Error',
											msg: 'error'
										});
									}
							},'json');
							
					} 
			});
		}
	};
	

	   function addResFun1() {
		var nodes = $('#treeRes').tree('getChecked', [ 'checked', 'indeterminate' ]);
		var ids = [];
		for (var i = 0; i < nodes.length; i++) {
			ids.push(nodes[i].id);
		}
		$.post(topsun.contextPath + '/imsbase/role/addres.do' ,
				{id:entityid,ids:ids.join(',')} , function(result){
				if (result.success){
					return true;
				}else {
					$.messager.show({	 
						title: 'Error',
						msg: 'error'
					});
					return false;
				}
		},'json');
	}
	
	var addResFun = function() {
		if ($(':input[name="id"]').val().length <= 0) {
			$.messager.show({
				title: '提示',
				msg: '请先保存'
			});
			return;
		}
      var id =''+ $(':input[name="id"]').val();
	  var dialog1 = parent.topsun.modalDialog({
			title : '选择信息',
			url : topsun.contextPath + '/imsbase/resource.jsp',
			buttons : [ {
				text : '选择',
				iconCls : 'icon-edit',
				handler : function() {
					dialog1.find('iframe').get(0).contentWindow.selectform(dialog1, $('#dgres'), id);
				}
			} ]
		});
	};
	
	var removeResFun = function() {
		var id = $(':input[name="id"]').val();
		var arr =$('#dgres').datagrid('getSelections');
		if(arr.length <=0){
			parent.$.messager.show({
				title:'提示信息!',
				msg:'至少选择一行记录进行删除!'
			});
			return;
		} else {
			$.messager.confirm('提示信息' , '确认删除?' , function(r){
					if(r){
							var ids = '';
							for(var i =0 ;i<arr.length;i++){
								ids += arr[i].id + ',' ;
							}
							ids = ids.substring(0 , ids.length-1);
							$.post(topsun.contextPath + '/imsbase/role/deleteres.do' ,
									{id:id,ids:ids} , function(result){
									if (result.success){
										$('#dgres').datagrid('reload');
										$('#dgres').datagrid('unselectAll');
									}else {
										$.messager.show({	 
											title: 'Error',
											msg: 'error'
										});
									}
							},'json');
							
					} 
			});
		}
	};
	//initialize
	$(function() {
		entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		if(model =="edit"){
			$('#treeRes').tree({
				onLoadSuccess : function(node, data) {
					$.post(topsun.contextPath + '/imsbase/res/getbyroleid.do?id='+entityid, {
						id : entityid
					}, function(result) {
						if (result) {
							for (var i = 0; i < result.length; i++) {
								var node = $('#treeRes').tree('find', result[i].id);
								if (node) {
									var isLeaf = $('#treeRes').tree('isLeaf', node.target);
									if (isLeaf) {
										$('#treeRes').tree('check', node.target);
									}
								}
							}
						}
					}, 'json');
				}
			});
		}
		
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/imsbase/role/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'sortno' : obj.sortno,
						'description': obj.description
					});
				}
				if(model =="edit"){
					$('#dguserrole').datagrid({
						url:topsun.contextPath + '/imsbase/user2/getbyroleid.do?id='+obj.id
				   });
					$('#dgres').datagrid({
						url:topsun.contextPath + '/imsbase/res/getbyroleid.do?id='+obj.id
				   });
					$('#dgusergroup').datagrid({
						url:topsun.contextPath + '/imsbase/usergroup/getbyuserroleid.do?id='+obj.id
				   });
				}
				
				if(model =="copy"){
					$(':input[name="id"]').val('');
				}
				
				$.messager.progress('close');
			}, 'json');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:100px;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				
				 <tr>
					<th>编号:</th>
					<td><input class="easyui-validatebox" type="text" name="id" value="<%=id%>"></input></td>
					<th><label>角色名:</label></th>
					<td><input class="easyui-validatebox" type="text" name="name" data-options="required:true"></input></td>
				</tr>
				<tr>
					<th>排序值:</th>
					<td><input name="sortno" class="easyui-numberbox" data-options="required:true,min:0,max:100000" value="100"></input></td>
					<th>资源描述:</th>
					<td><textarea name="description"></textarea></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="mainPanle" region="center" style="background: #eee; overflow-y:hidden"  >
      <div id="tabs" class="easyui-tabs"  fit="true" border="false" >
    	<div title="拥有用户" style="padding:5px;overflow:hidden; " >
    	  <table id="dguserrole"  class="easyui-datagrid"  
					toolbar="#toolbar" fit="true" striped="true"
					 pagination="true"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">用户名</th>
					</tr>
				</thead>
			</table>
        </div>
        <div title="拥有用户组" style="padding:5px;overflow:hidden; " >
    	  <table id="dgusergroup"  class="easyui-datagrid"  
					fit="true" striped="true"
					 pagination="true"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">用户组名</th>
					</tr>
				</thead>
			</table>
        </div>
        <!-- 
        <div title="拥有资源" style="padding:5px;overflow:hidden; " >
    	  <table id="dgres"  class="easyui-datagrid"  
					 fit="true" striped="true"
					toolbar="#restoolbar" 
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">资源名称</th>
					</tr>
				</thead>
			</table>
        </div>
         -->
        <div title="拥有资源" style="padding:5px;overflow:auto; " >
        	<fieldset>
			<ul  id="treeRes" class="easyui-tree" data-options="idField:'id',textField:'text',parentField:'pid',url:'<%=contextPath%>/imsbase/res/getallrestree.do',method:'post',animate:true,checkbox:true"></ul>
			</fieldset>
        </div>
   	  </div>
  	</div>
  	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addRoleFun()">添加</a>
		 <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeRoleFun()">删除</a>
	</div>
	<div id="restoolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addResFun()">添加</a>
		 <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeResFun()">删除</a>
	</div>
</body>
</html>