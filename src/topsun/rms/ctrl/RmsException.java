package topsun.rms.ctrl;

import topsun.rms.common.RmsErrors;

/**
 * 自定义Rms异常
 * @author yuxiaobei
 *
 */
public class RmsException extends Exception
{
	private int nIMError = RmsErrors.OK;
	
	public RmsException(int nErrorCode)
	{
		super();
		this.nIMError  = nErrorCode;
	}
	
	
	public RmsException(int nErrorCode,String strInfo)
	{
		super(strInfo);
		this.nIMError  = nErrorCode;
	}
	
	
	/**
	 * 获取错误代码
	 * @return
	 */
	public int getErrorCode()
	{
		return this.nIMError;
	}

}
