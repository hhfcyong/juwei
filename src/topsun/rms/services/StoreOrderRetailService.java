package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.StoreOrderRetail;

@Service("storeOrderRetailService")
@Transactional
public class StoreOrderRetailService extends BaseService<StoreOrderRetail>{

	@Resource(name = "storeOrderRetailDao")  
	public void setDao(IBaseDao<StoreOrderRetail> dao) 
	{  
		super.setDao(dao);  
	}
}
