<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
	String contextPath = request.getContextPath();
	SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>组织机构管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
		var url;
	
		var addFun = function() {
			var dialog = topsun.modalDialog({
				title : '添加信息',
				iconCls : 'icon-add',
				url : topsun.contextPath + '/imsbase/oranizationform.jsp',
				buttons : [ {
					text : '添加',
					iconCls : 'icon-ok',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.treesubmitForm(dialog, $('#dg'));
					}
				} ,
				{
					text : '关闭',
					iconCls : 'icon-cancel',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.treeCancleform(dialog, $('#dg'));
					}
				}  ]
			});
		};
		var editFun = function() {
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length != 1){
				$.messager.show({
					title:'提示信息!',
					msg:'只能选择一行记录进行修改!'
				});
			}else{
			  var row = $('#dg').datagrid('getSelected');
			  if (row){
				  var dialog = topsun.modalDialog({
						title : '编辑资源信息',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/imsbase/oranizationform.jsp?id=' + row.id,
						buttons : [ {
							text : '编辑',
							iconCls : 'icon-ok',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.treesubmitForm(dialog, $('#dg'));
							}
						} ,
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.treeCancleform(dialog, $('#dg'));
							}
						}  ]
					});
			   }
			}
			
		};
		
		var viewFun = function() {
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length == 1){
			  var row = $('#dg').datagrid('getSelected');
			  if (row){
				  var dialog = topsun.modalDialog({
						title : '编辑资源信息',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/imsbase/oranizationform.jsp?id=' + row.id,
						buttons : [ 
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.treeCancleform(dialog, $('#dg'));
							}
						}  ]
					});
			   }
			}
			
		};
		var copyFun = function() {
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length == 1){
			  var row = $('#dg').datagrid('getSelected');
			  if (row){
				  var dialog = topsun.modalDialog({
						title : '复制资源信息',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/imsbase/oranizationform.jsp?showmodel=copy&id=' + row.id,
						buttons : [ {
							text : '编辑',
							iconCls : 'icon-ok',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.treesubmitForm(dialog, $('#dg'));
							}
						} ,
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.treeCancleform(dialog, $('#dg'));
							}
						}  ]
					});
			   }
			}
			
		};
		//删除obj
		function removeFun(){
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length <=0){
				$.messager.show({
					title:'提示信息!',
					msg:'至少选择一行记录进行删除!'
				});
				return;
			} else {
				$.messager.confirm('提示信息' , '确认删除?' , function(r){
						if(r){
								var ids = '';
								for(var i =0 ;i<arr.length;i++){
									ids += arr[i].id + ',' ;
								}
								ids = ids.substring(0 , ids.length-1);
								$.post(topsun.contextPath + '/imsbase/ora/remove.do' ,
										{ids:ids} , function(result){
										if (result.success){
											$('#dg').treegrid('reload');
											$('#dg').treegrid('unselectAll');
											refreshZTree();
											$.messager.show({
												title:'提示信息!' , 
												msg:'操作成功!'
											});
										}else {
											$.messager.show({	// show error message
												title: 'Error',
												msg: 'error'
											});
										}
								},'json');
								
						} 
				});
			}
			
		}
		//根据查询条件 重载datagird
		function reload()
		{
			$('#dg').treegrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		 function onDblClickRow(rowIndex, rowData){
				editFun();
			}
		 exportExcel = function($grid, uri, $form){
				$.messager.confirm('提示信息' , '确认导出所有数据?' ,  function(r){
					if(r){
						var params = 'exportmodel=all';
						if(typeof(sortName)!="undefined" && sortName!='' && typeof(sortOrder)!="undefined" && sortOrder!=''  ){
							params+='&sort='+sortName+'&order='+sortOrder;
						}
						var formparams = '';
						//增加过滤条件
						if($form !='undefined' && $form!='' ){
							formparams = $form.serialize();  
						}
						var url =topsun.contextPath + uri +'?'+params+'&'+formparams;  
				 	 	window.location.href = url;
						
					} 
				}
				);
			};
		 $(function(){
				/*
				$('#dg').treegrid({
					parentField:'pid',
					onDblClickRow:function(rowIndex, rowData){
						editFun();
					}
				});
				*/
			});
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" style="padding: 5px" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					机构名称:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch')[0].reset();reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center" border="false" onselectstart="return false;" style="-moz-user-select:none;" >
		<table id="dg" title="组织机构管理" class="easyui-treegrid"  
		        idField ="id" treeField ="name" iconCls="yxb-icon datagrid-head"    
				url="ora/listallex.do" fit="true" sortname="sortno" sortorder="asc"
				toolbar="#toolbar"  data-options="onDblClickRow:onDblClickRow,parentField:'pid'"
				rownumbers="true" fitColumns="true" 
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
						<tr>
							<th field="name" >组织机构名称</th>
						</tr>
					</thead>
			<thead>
				<tr>
					<th field="code" width="40"  >编码</th>
					<th field="description" width="50" >资源描述</th>
					<th field="sortno" width="50">排序值</th>
				</tr>
			</thead>
		</table>
	</div>
				
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("resourcemanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addFun()">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editFun()">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeFun()">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("resourcemanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="viewFun()">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("resourcemanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="copyFun()">复制</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="exportExcel($('#dg'),'/imsbase/ora/exportexcel.do',$('#mysearch'))">导出</a></td>
				<%}%>
				
			</tr>
		</table>
	</div>
	
	
  </body>
</html>
