package topsun.rms.entity;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 单品分类
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class ItemType implements java.io.Serializable
{
	private String id;
	private String name;
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
