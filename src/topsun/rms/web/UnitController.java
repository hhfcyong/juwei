package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.Unit;
import topsun.rms.services.IBaseService;

/**
 * 单位  Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("dic/unit")
public class UnitController extends BaseController<Unit>
{
	@Resource(name = "unitService")  
	public void setService(IBaseService<Unit> Service1) 
	{  
		super.setService(Service1);
	}
	
}
