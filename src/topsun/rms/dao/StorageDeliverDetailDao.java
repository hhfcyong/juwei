package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.StorageDeliverDetail;

/**
 * 仓库发货明细  Dao
 * 
 * @author Tang
 *
 */

@Repository("storageDeliverDetailDao")  
@Transactional
public class StorageDeliverDetailDao extends BaseDao<StorageDeliverDetail>{

}
