<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
String storeOrderId =request.getParameter("id");
request.setAttribute("storeOrderId", storeOrderId);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>要货单明细</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	function reload() {
		$('#dg').datagrid('load', topsun.serializeObject($('#mysearch')));
	}
	function formatObject(val, row) {
		return val.name;
	}
	function formatObjectId(val, row) {
		return val.itemcode;
	}
	function onDblClickRow(rowIndex, rowData) {
		topsun.editFun($('#dg1'), '/datapage/storeorderretailform.jsp');
	}
	//排序时触发
	function onSortColumn(sort, order) {
		sortName = sort;
		sortOrder = order;
	}
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
	};
	$(function(){
		  var id = Number("${storeOrderId}");
		  $('#dg1').datagrid({
              title:'要货单明细',
              fit:true,  
              fitColumns:true,  
              striped:true,
              iconCls:"yxb-icon datagrid-head",
              url:'storeorderretail/list.do',
              queryParams:{'QUERY_t#storeOrder.id_I_EQ':id},
              sortName: 'id',
              sortOrder: 'asc',
              remoteSort: false,
              pagination:true,
              pageNumber:1,
              pageSize:20,
              rownumbers:true,
              toolbar:"#toolbar",
              loadMsg:"数据正在加载,请耐心的等待..." 
           });	
	})
	
	
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg1" SelectOnCheck="false" CheckOnSelect="false" singleSelect="true" data-options="onDblClickRow:onDblClickRow"
				>
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
					<th field="id" hidden="true"></th>
				    <th field="item" width="100" formatter="formatObjectId">单品编码</th>
					<th field="itemName" width="100" >单据名称</th>
					<th field="amount" width="100">订货数量</th>
					<th field="unitName" width="100" >单位</th>
					<th field="price" width="100" >订货价</th>
					<th field="orderQuantity" width="100" >订货金额</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("storeorderretailmanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg1'),'/datapage/storeorderretailform.jsp?storeOrderId=${storeOrderId}')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("storeorderretailmanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg1'),'/datapage/storeorderretailform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("storeorderretailmanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dg1'),'/datapage/storeorderretail/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("storeorderretailmanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg1'),'/datapage/storeorderretailform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("storeorderretailmanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg1'),'/datapage/storeorderretailform.jsp')">复制</a></td>
				<%}%>
			<%-- 	<%if (securityUtil.havePermission("storeorderretailmanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/datapage/type/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%> --%>
			</tr>
		</table>
		
	</div>
	
  </body>
</html>
