package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.PromotionRange;;;

/**
 * 资源类型Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("promotionRangeDao")
@Transactional  
public class PromotionRangeDao extends BaseDao<PromotionRange> 
{

}
