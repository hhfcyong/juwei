package topsun.rms.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.Warehouse;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IWarehouseService;

/**
 * 仓库
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/warehouse")
public class WarehouseController extends BaseController<Warehouse>
{
	@Resource(name = "warehouseService")  
	public void setService(IBaseService<Warehouse> Service1) 
	{  
		super.setService(Service1);
	}

	@Override
	public void save(HttpEntity<Warehouse> model, HttpServletRequest request, HttpServletResponse response) {
		
		IWarehouseService warehouseService = (IWarehouseService)service;
		int maxCode = warehouseService.getMaxCode();
		model.getBody().setCode(maxCode+1);
		
		super.save(model, request, response);
	}
	
	
}
