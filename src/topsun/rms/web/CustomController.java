package topsun.rms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.LoginAccount;
import topsun.rms.entity.User;
import topsun.rms.entity.UserGroup;
import topsun.rms.entity.UserRole;
import topsun.rms.model.RetJson;
import topsun.rms.model.SessionInfo;
import topsun.rms.services.IBaseService;
import topsun.rms.services.ICustomService;
import topsun.rms.utility.IpUtil;

/**
 * user2
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/user2")
public class CustomController extends BaseController<User> 
{
	
	@Resource(name = "customService")  
	public void setService(IBaseService<User> Service1) 
	{  
		super.setService(Service1);
	}
	
	@RequestMapping(value = "/grantrole", method = RequestMethod.POST)
	public void grandRole( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		ICustomService customService = (ICustomService)service;
		
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.grantRole(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/deleterole", method = RequestMethod.POST)
	public void deleteRole( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		ICustomService customService = (ICustomService)service;
		
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.deleteRole(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/getbyroleid", method = RequestMethod.POST)
	public void getRoleByUserId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_role#id_S_EQ", id);
		List<User> users = ((ICustomService) service).findUserByFilter(hqlFilter);
		writeJson(users,request,response);
	}
	
	@RequestMapping(value = "/getbyusergroupid", method = RequestMethod.POST)
	public void getUserByUserGroupId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_usergroup#id_S_EQ", id);
		List<User> users = ((ICustomService) service).findUserByFilterGroup(hqlFilter);
		writeJson(users,request,response);
	}
	
	/**
	 * 登录
	 * @param model 
	 * 				登录账号
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public void login(HttpEntity<LoginAccount> model, HttpServletRequest request, HttpServletResponse response)
	{
		LoginAccount loginAccount = model.getBody();
		
		HqlFilter hqlFilter = new HqlFilter();
		hqlFilter.addFilter("QUERY_loginaccount#id_S_EQ", loginAccount.getId());
		hqlFilter.addFilter("QUERY_loginaccount#pwd_S_EQ", loginAccount.getPwd());
		
		User user =((ICustomService)service).findUserByLoginAccountFilter(hqlFilter);
		
		RetJson json = new RetJson();
		if (user != null) 
		{
			json.setSuccess(true);

			SessionInfo sessionInfo = new SessionInfo();
			Hibernate.initialize(user.getUserroles());
			for (UserRole role : user.getUserroles())
			{
				Hibernate.initialize(role.getResources());
			}
			
			Hibernate.initialize(user.getUsergroups());
			for(UserGroup ug : user.getUsergroups())
			{
				Hibernate.initialize(ug.getUserroles());
				
				for (UserRole role : ug.getUserroles())
				{
					Hibernate.initialize(role.getResources());
				}
			}
			user.setIp(IpUtil.getIpAddr(request));
			user.setLoginAccountName(loginAccount.getId());
			sessionInfo.setUser(user);
			
			request.getSession().setAttribute("sessionInfo", sessionInfo);
		} else {
			json.setMsg("用户名或密码错误！");
		}
		writeJson(json, request, response);
	}
	
	/**
	 * 退出
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public void logout( HttpServletRequest request, HttpServletResponse response)
	{
		if (request.getSession() != null) {
			request.getSession().invalidate();
		}
		RetJson json = new RetJson();
		json.setSuccess(true);
		writeJson(json, request, response);
	}
	
	
	
	
}
