package topsun.rms.ctrl;

import java.util.Hashtable;

import topsun.framework.utility.StringHelper;
import topsun.framework.web.ISRFExWebContext;

/**
 * 远程行为上下文对象
 * @author yuxiaobei
 *
 */
public class RemoteAction implements IRemoteAction
{
	protected String strAction = "";
	protected Hashtable<String,String> paramMap = new Hashtable<String,String>();
	protected String strRemoteAddress= "";
	protected boolean bFromServer = false;
	protected String strContent = "";
	
	public String getAction()
	{
		return this.strAction;
	}
	
	public void setAction(String strAction)
	{
		this.strAction = strAction;
	}

	public Hashtable<String,String>  getParams()
	{
		return paramMap;
	}
	public String getParam(String strParamName, String strDefault)
	{
		strParamName = strParamName.toUpperCase();
		if(paramMap.containsKey(strParamName))
			return paramMap.get(strParamName);
		return strDefault;
	}
	
	public void setParam(String strParamName, String strValue)
	{
		strParamName = strParamName.toUpperCase();
		if(StringHelper.IsNullOrEmpty(strValue))
		{
			paramMap.remove(strParamName);
		}
		else
		{
			paramMap.put(strParamName, strValue);
		}
	}
	

	public String getRemoteAddress()
	{
		return strRemoteAddress;
	}
	
	public void setRemoteAddress(String strRemoteAddress)
	{
		this.strRemoteAddress = strRemoteAddress;
	}

	public boolean isFromServer()
	{
		return bFromServer;
	}
	
	
	public void setFromServer(boolean bFromServer)
	{
		this.bFromServer = bFromServer;
	}
	
	
	/**
	 * 获取内容
	 * @return
	 */
	public String getContent()
	{
		if(!StringHelper.IsNullOrEmpty(this.strContent))
			return this.strContent;
		return this.getParam("CONTENT", "");
	}
	
	/**
	 * 设置内容
	 * @param strContent
	 */
	public void setContent(String strContent)
	{
		this.strContent  = strContent;
	}
	
	
	/**
	 * 从上下文中构造远程行为
	 */
	public void FromWebContext(ISRFExWebContext webContext) throws Exception
	{
		String strAction = webContext.GetParamValue("RMSACTION");
		 strAction = webContext.GetPostValue("RMSACTION");
		if(StringHelper.IsNullOrEmpty(strAction))
		{
			throw new Exception("没有指定远程行为");
		}
		this.setAction(strAction);
//		this.setRemoteAddress(webContext.getRemoteAddr());
		
		Hashtable<String,String> params = webContext.GetParams();
		for(String strKey:params.keySet())
		{
			if(StringHelper.Compare(strKey, "RMSACTION", true)!=0)
			{
				this.setParam(strKey, params.get(strKey));
			}
		}
		
		String strContent = webContext.GetPostValue("content");
		if(!StringHelper.IsNullOrEmpty(strContent))
		{
			this.setContent(strContent);
		}
	}

	
	
	/**
	 * 复制
	 * @return
	 */
	public void FromRemoteAction(IRemoteAction remoteActionClone)
	{
		//IMRemoteAction imRemoteAction = new IMRemoteAction();
		this.setAction(remoteActionClone.getAction());
		this.setContent(remoteActionClone.getContent());
		Hashtable<String,String> params = remoteActionClone.getParams();
		for(String strKey:params.keySet())
		{
			this.setParam(strKey, params.get(strKey));
		}
	}
	

}
