package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.Item;
import topsun.rms.entity.UserRole;

/**
 * 自定义服务
 * 
 * @author zy
 *
 */
public interface IItemService extends IBaseService<Item>
{
   public List<Item> findItemByFilter(HqlFilter hqlFilter);

}
