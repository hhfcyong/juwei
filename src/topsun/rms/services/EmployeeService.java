package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Employee;

/**
 * 员工服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("employeeService")
public class EmployeeService extends BaseService<Employee>
{
	@Resource(name = "employeeDao")  
	public void setDao(IBaseDao<Employee> dao) 
	{  
		super.setDao(dao);  
	}

}
