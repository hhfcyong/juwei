package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.Type;
import topsun.rms.entity.UserGroup;
import topsun.rms.services.IBaseService;
import topsun.rms.services.TypeService;
/**
 * 单据类型controller
 * @author fei
 *
 */
@Controller
@RequestMapping("datapage/type")
public class TypeController extends BaseController<Type>{
	@Resource(name = "typeService")  
	public void setService(IBaseService<Type> service) 
	{  
		super.setService(service);
	}

}
