package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.User;
import topsun.rms.entity.UserRole;

/**
 * 自定义服务
 * 
 * @author yuxiaobei
 *
 */
public interface ICustomService extends IBaseService<User>
{
	/**
	 * 修改用户角色
	 * 
	 * @param id
	 *            用户ID
	 * @param roleIds
	 *            角色IDS
	 */
	public void grantRole(String id, String roleIds);
	
	/**
	 * 删除用户角色
	 * @param id
	 * 
	 * @param roleIds
	 */
	public void deleteRole(String id, String roleIds);
	
	/**
	 *  查询角色所拥有的用户
	 * 
	 * @param hqlFilter
	 * 
	 * @return
	 */
	public List<User> findUserByFilter(HqlFilter hqlFilter); 
	
	/**
	 *  查询用户组所拥有的用户
	 * 
	 * @param hqlFilter
	 * 
	 * @return
	 */
	public List<User> findUserByFilterGroup(HqlFilter hqlFilter); 
	
	/**
	 * 查询登录账号对应的账号
	 * 
	 * @param hqlFilter
	 * 
	 * @return
	 */
	public User findUserByLoginAccountFilter(HqlFilter hqlFilter);
}
