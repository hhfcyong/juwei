package topsun.rms.web;

import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.DataDictionary;
import topsun.rms.entity.Member;
import topsun.rms.entity.Shop;
import topsun.rms.model.Pager;
import topsun.rms.services.IBaseService;

/**
 * 
 * 会员 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("membership/member")
public class MemberController extends BaseController<Member>
{
	protected Hashtable<String, String> agegroupList = new Hashtable<>();
	
	@Resource(name = "memberService")  
	public void setService(IBaseService<Member> Service1) 
	{  
		super.setService(Service1);
	}
	
	@Resource(name = "dataDictionaryService")   
	private IBaseService<DataDictionary> dataDictionaryService;
	
	/**
	 * get字典信息
	 */
	private void getDictionarys()
	{
		HqlFilter hqlFilter = new HqlFilter();
		hqlFilter.addFilter("QUERY_t#datadictionarytype#id_S_EQ", "agegroup");
		List<DataDictionary> dictionarys = dataDictionaryService.findByFilter(hqlFilter);
		for (DataDictionary dataDictionary : dictionarys) 
		{
			if(dataDictionary.getDatadictionarytype() == null) continue;
			if(!agegroupList.containsKey(dataDictionary.getId()))
			{
				agegroupList.put(dataDictionary.getId(), dataDictionary.getName());
			}
		}
	}
	
	@Override
	protected void grid(HttpServletRequest request, HttpServletResponse response) 
	{
		getDictionarys();  //temp
		parseRequest(request);
		
		Pager pager = new Pager();
		HqlFilter hqlFilter = new HqlFilter(request);
		pager.setTotal(service.countByFilter(hqlFilter));
		
		List<Member> members = service.findByFilter(hqlFilter, page, rows);
		//字典转换
		for (Member member : members)
		{
			String agegroup  = member.getAgegroup();
			if(StringUtils.isBlank(agegroup)) continue;
			if(agegroupList.containsKey(agegroup)) 
			{
				member.setAgegroup(agegroupList.get(agegroup));
			}
		}
		
		pager.setRows(members);
		writeJson(pager, request, response);
	}
}
