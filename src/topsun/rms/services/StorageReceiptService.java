package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Employee;
import topsun.rms.entity.StorageReceipt;

/**
 * 自定义仓库收货服务类
 * 
 * @author Tang
 *
 */
@Service("storageReceiptService")
public class StorageReceiptService extends BaseService<StorageReceipt> implements IStorageReceiptService
{
	@Resource(name = "storageReceiptDao")  
	public void setDao(IBaseDao<StorageReceipt> dao) 
	{  
		super.setDao(dao);  
	}
}
