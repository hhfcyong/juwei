package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.ResourceType;

/**
 * 资源类型服务 实现
 * 
 * @author yuxiaobei
 *
 */
@Service("resourceTypeService")
public class ResourceTypeService extends BaseService<ResourceType> 
{
	@Resource(name = "resourceTypeDao")  
	public void setDao(IBaseDao<ResourceType> dao) 
	{  
		super.setDao(dao);  
	}
}
