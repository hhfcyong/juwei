package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.PromotionType;;

/**
 * 资源类型服务 实现
 * 
 * @author zy
 *
 */
@Service("promotionTypeService")
public class PromotionTypeService extends BaseService<PromotionType> 
{
	@Resource(name = "promotionTypeDao")  
	public void setDao(IBaseDao<PromotionType> dao) 
	{  
		super.setDao(dao);  
	}
}
