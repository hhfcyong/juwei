package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.Promotion;
import topsun.rms.entity.UserRole;

/**
 * 自定义服务
 * 
 * @author zy
 *
 */
public interface IPromotionService extends IBaseService<Promotion>
{
   
	public void addItem(int id, String userIds);
	public void deleteRole(int id,String roleIds);
	}
