<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var user;
    var promotionType;
    var promotionRange;
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/promotionpage/pro/update.do';
				
			} else {
				url = topsun.contextPath + '/promotionpage/pro/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			jsonString=jsonString+','+user+','+promotionType;
			
			jsonString = jsonString +',' +promotionRange+'}';
			
			//alert(jsonString);
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
		     		 $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		$('#user').combobox({
			onSelect:function(row){
				user = '"user":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#promotionType').combobox({
			onSelect:function(row){
				promotionType = '"promotionType":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#promotionRange').combobox({
			onSelect:function(row){
				promotionRange = '"promotionRange":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/promotionpage/pro/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'promotionNO' : obj.promotionNO,
						'promotionName':obj.promotionName,
						'description':obj.description,
						'formulateDate2':obj.formulateDate,
						'value1':obj.value1,
						'value2':obj.value2
					});
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}				
				$('#user').combobox('select', obj.user.id );
				$('#promotionType').combobox('select', obj.promotionType.id );
				$('#promotionRange').combobox('select', obj.promotionRange.id );
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:500px;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>方案序号</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>方案编号</th>
					<td><input name="promotionNO" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
				    <th>方案名称</th>
				    <td><input name="promotionName" class="easyui-validatebox" data-options="required:true" /></td>
				    <th>方案描述</th>
				    <td><input name="description" class="easyui-validatebox" data-options="required:false" /></td>
				</tr>
				<tr>
				    <th>制定人</th>
				    <td><select  id="user"  class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/user2/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>
				    <th>制定日期</th>
				    <td><input name="formulateDate2" class="easyui-datetimebox" data-options="required:true" /></td>
				</tr>
				<tr>
				    <th>促销类型</th>
			       <td><select  id="promotionType"  class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/promotionpage/protype/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>
                    <th>促销范围</th>
                    <td><select  id="promotionRange"  class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/promotionpage/prorange/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>          
				</tr>
				<tr>
				   <th>值1</th>
				   <td><input name="value1" class="easyui-validatebox" data-options="required:true" /></td>
				   <th>值2</th>
				   <td><input name="value2" class="easyui-validatebox" data-options="required:false" /></td>
				</tr>
			</table>
		</form>
	</div>
	
</body>
</html>