<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>单品管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	var selectform = function($dialog, $grid, id, type) {		
		
		if ($('form').form('validate')) {
			

			var arr =$('#dg').datagrid('getChecked');
			if(arr.length <=0){
				$.messager.show({
					title:'提示信息!',
					msg:'至少选择一行记录!'
				});
				return;
			}
			
			var ids = '';
			for(var i =0 ;i<arr.length;i++){
				ids += arr[i].id + ',' ;
			}
			var roleid = id;
			var url;
			if(type=='item'){
			
				url = topsun.contextPath + '/promotionpage/pro/addItem.do';
			
			}else{
			//	url = topsun.contextPath + '/imsbase/usergroup/adduser.do';
			}

			$.post(url ,
					{id:roleid,ids:ids} , function(result){
					if (result.success){
						 $dialog.dialog('destroy');		 
						 $grid.datagrid('reload');	 	
					}else {
						$.messager.show({	 
							title: 'Error',
							msg: 'error'
						});
					}
			},'json');
		}
	};
		
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/datapage/itemform.jsp');
		}
		 //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		
		$(function(){
			
		/* 不重复定义，会造成重复加载
		$('#dg').datagrid({
				onDblClickRow:function(rowIndex, rowData){
					editFun();
				},
				url:topsun.contextPath+'/imsbase/user2/list.do'
			});
		*/
		});
	
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
				<label style="margin: 0px 0px 0px 10px;">商品编码:</label> <input name="QUERY_t#itemcode_S_LK" class="easyui-validatebox" style="width: 100px; "  />
				<label style="margin: 0px 0px 0px 10px;">简称:</label> <input name="QUERY_t#sname_S_LK" class="easyui-validatebox"  style="width: 100px; " />
				<label style="margin: 0px 0px 0px 10px;">描述:</label> <input name="QUERY_t#description_S_LK" class="easyui-validatebox" style="width: 100px; "  />
				<label style="margin: 0px 0px 0px 10px;">分类:</label> <select name="QUERY_t#itemtype_S_EQ" class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/itemtype/listall.do',panelHeight:'auto'" style="width: 100px;  "></select>
				<label style="margin: 0px 0px 0px 10px;">单位:</label> <select name="QUERY_t#unit_S_EQ" class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/dic/unit/listall.do',panelHeight:'auto'" style="width: 100px;"></select>
				
				<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
				<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch').form('clear');;reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="单品管理" class="easyui-datagrid" url="item/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
					<th field="itemcode" width="200">商品编码</th>
					<th field="description" width="200">商品描述</th>
					<th field="sname" width="200">简称</th>
					<th field="specification" width="200">规格</th>
					
					<th field="expirationdate" width="200">保质期限</th>
					<th field="unit" width="200">计量单位</th>
					<th field="itemtype" width="200">所属分类</th>
					<th field="costprice" width="200">成本价</th>
					
					<th field="wholesaleprice" width="200">批发价</th>
					<th field="saleprice" width="200">零售价</th>
					<th field="skucode" width="200">SKU</th>
					<th field="enable" width="200">状态</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("usermanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/datapage/itemform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usermanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/datapage/itemform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usermanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/datapage/item/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usermanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/datapage/itemform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usermanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg'),'/datapage/itemform.jsp')">复制</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usermanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/datapage/item/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
	
  </body>
</html>
