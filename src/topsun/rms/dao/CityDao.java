package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.City;

/**
 * 城市 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("cityDao")
@Transactional  
public class CityDao extends BaseDao<City> 
{
	
}
