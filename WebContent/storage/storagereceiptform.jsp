<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	request.setAttribute("currentUser", currentUser);
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var deliver;
    var receipt;
    var documentState;
    var storage;
    var audit;
    var creater;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/storage/receipt/update.do';
				
			} else {
				url = topsun.contextPath + '/storage/receipt/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			var createUserId = '"creater":{"id":"'+ $("#createrId").val()+'"}' ;
			jsonString=jsonString+','+deliver+','+receipt;	
			if(typeof(createUserId)!="undefined")
			    jsonString = jsonString +',' +createUserId;
			if(typeof(audit)!="undefined")
				jsonString=jsonString+','+audit;
	
			jsonString = jsonString +',' +documentState+','+storage+'}';

			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
			     		 $grid.datagrid('reload');
		          },   
		          error: function(XMLHttpRequest, textStatus, errorThrown) {
                      alert(XMLHttpRequest.status);
                      alert(XMLHttpRequest.readyState);
                      alert(textStatus);
                  }   
		        });   
		}
	};

	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};		
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		$('#deliver').combobox({
			onSelect:function(row){
				deliver = '"deliver":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#receipt').combobox({
			onSelect:function(row){
				receipt = '"receipt":{"id":"'+ row.id+'"}' ;
			}
		});


		$('#creater').combobox({
			onSelect:function(row){
				creater = '"creater":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#audit').combobox({
			onSelect:function(row){
				audit = '"audit":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#documentState').combobox({
			onSelect:function(row){
				documentState = '"documentState":{"documentStateId":"'+ row.documentStateId+'"}' ;
			}
		});
		$('#storage').combobox({
			onSelect:function(row){
				storage = '"storage":{"id":"'+ row.id+'"}' ;
			}
		});
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="storageReceiptNo"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/storage/receipt/getbyid.do', {
			 id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'storageReceiptNo' : obj.storageReceiptNo,
						'deliverDate2' : obj.deliverDate,
						'newDate2' : obj.newDate,
						'remark' : obj.remark,
						'auditDate2' : obj.auditDate
					});
					$("#documentState").combobox('select',obj.documentState.documentStateId);
					$("#deliver").combobox('select',obj.deliver.id);
					$("#receipt").combobox('select',obj.receipt.id);					
					$("#storage").combobox('select',obj.storage.id);
					$("#audit").combobox('select',obj.audit.id);
				}
				
			}, 'json');
		 $.messager.progress('close');
     }	
	
	//formate date
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}

</script>
</head>
<body class="easyui-layout" style="width: 100%;height:100%" >	
	<div data-options="border:false">
		<form method="post" class="form">
		<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="10" >
				<tr>
					<input type="hidden" name="id" />
					<th>单据号</th>
					<td><input name="storageReceiptNo"  class="easyui-validatebox" value="" /></td>
					<th>单据状态</th>
					<td><select id="documentState" style="width: 155px;" class="easyui-combobox" data-options="required:true,
																					editable:false,
																					valueField:'documentStateId',
																					textField:'documentStateName',
																					url:'<%=contextPath%>/document/state/listall.do',
																					panelHeight:'auto'" style="width: 155px;"></select></td>
					
				</tr>			
				<tr>
					<th>发货人</th>
					<td><select  id="deliver"  class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/user2/listall.do',panelHeight:'auto'" style="width: 155px;"></select>
					</td>	
					<th>发货日期</th>
					<td><input class="easyui-datetimebox" name="deliverDate2" required="true"></input></td>
																									
				</tr>	
				<tr>
					<th>接收仓库</th>
					<td><select id="storage" class="easyui-combobox" data-options="required:true,
																					editable:false,
																					valueField:'id',
																					textField:'name',
																					url:'<%=contextPath%>/datapage/warehouse/listall.do',
																					panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>接收人</th>
					<td><select id="receipt" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/user2/listall.do',panelHeight:'auto'" style="width: 155px;"></select>
					</td>
				</tr>		
				<tr>
					<th>新建人</th>
					<td><input name="creater" class="easyui-validatebox" value="${currentUser.name}" disabled="disabled"/>
					<input type="hidden" value="${currentUser.id}" id="createrId"/></td>
					<th>新建日期</th>
					<td><input class="easyui-datetimebox" name="newDate2" required="true"></td>

					
				</tr>			
				<tr>
					<th>备注</th>
					<td><input name="remark"  class="easyui-validatebox" value=""></input></td>
					<th>审核人</th>
					<td><select id="audit" class="easyui-combobox"  data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/user2/listall.do'" style="width: 155px;"></select></td>
					
				</tr>
				<tr>
					<th>审核日期</th>
					<td><input class="easyui-datetimebox" name="auditDate2"></td>
				</tr>
			</table>			
		</form>
	</div>
	
</body>
</html>