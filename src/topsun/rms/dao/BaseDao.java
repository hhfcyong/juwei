package topsun.rms.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.transaction.annotation.Transactional;

/**
 * BaseDao 定义DAO的通用操作的实现
 * 
 * @author yuxiaobei
 * 
 */
@SuppressWarnings({"unchecked","rawtypes"})
@Transactional  
public class BaseDao<T> implements IBaseDao<T> 
{


	private Class<T> clazz;

	/**
	 * 通过构造方法指定DAO的具体实现类
	 */
	public BaseDao() 
	{
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		clazz = (Class<T>) type.getActualTypeArguments()[0];
	}
	/**
	 * 向DAO层注入SessionFactory
	 */
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	/**
	 * 获取当前工作的Session
	 */
	protected Session getSession() 
	{
		return this.sessionFactory.getCurrentSession();
	}
	@Override
	public Serializable save(T entity) 
	{
		return this.getSession().save(entity);
	}
	@Override
	public void update(T entity) 
	{
		this.getSession().update(entity);
	}
	
	@Override
	public void merge(T entity)
	{
		this.getSession().merge(entity);
	}
	
	@Override
	public void delete(Serializable id) 
	{
		this.getSession().delete(this.getById(id));
	}
	
	@Override
	public void delete(T entity) 
	{
		this.getSession().delete(entity);
	}
	
	
	
	@Override
	public T getById(Serializable id) 
	{
		return (T) this.getSession().get(this.clazz, id);
	}
	
	
	
	@Override
	public void saveOrUpdate(T o) {
		if (o != null) 
		{
			getSession().saveOrUpdate(o);
		}
	}
	
	@Override
	public List<T> find()
	{
		Query q = getSession().createQuery(" from " + clazz.getName());
		return q.list();
	}

	@Override
	public List<T> find(String hql) 
	{
		Query q = getSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params) 
	{
		Query q = getSession().createQuery(hql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return q.list();
	}
	
	@Override
	public List<T> find(String hql, Object... params) 
	{
		Query q = this.getSession().createQuery(hql);
		for (int i = 0; params != null && i < params.length; i++) 
		{
			q.setParameter(i, params[i]);
		}
		return q.list();
	}

	@Override
	public List<T> find(String hql, Map<String, Object> params, int page, int rows) {
		Query q = getSession().createQuery(hql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) {
				q.setParameter(key, params.get(key));
			}
		}
		return q.setFirstResult((page - 1) * rows).setMaxResults(rows).list();
	}
	
	@Override
	public List<T> find(int page, int rows, String hql, Object... params )
	{
		Query q = getSession().createQuery(hql);
		for (int i = 0; params != null && i < params.length; i++) 
		{
			q.setParameter(i, params[i]);
		}
		return q.setFirstResult((page - 1) * rows).setMaxResults(rows).list();
	}

	@Override
	public List<T> find(String hql, int page, int rows) 
	{
		Query q = getSession().createQuery(hql);
		return q.setFirstResult((page - 1) * rows).setMaxResults(rows).list();
	}
	
	@Override
	public Long count()
	{
		String queryString = String.format(" select count(*) from %1$s ", clazz.getName());
		Query q = getSession().createQuery(queryString);
		return (Long) q.uniqueResult();
	}
	
	@Override
	public Long count(String hql) {
		Query q = getSession().createQuery(hql);
		return (Long) q.uniqueResult();
	}

	@Override
	public Long count(String hql, Map<String, Object> params) {
		Query q = getSession().createQuery(hql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return (Long) q.uniqueResult();
	}
	
	@Override
	public Long count(String hql, Object... params)
	{
		Query q = getSession().createQuery(hql);
		for (int i = 0; params != null && i < params.length; i++) 
		{
			q.setParameter(i, params[i]);
		}
		return (Long) q.uniqueResult();
	}

	@Override
	public int executeHql(String hql) 
	{
		Query q = getSession().createQuery(hql);
		return q.executeUpdate();
	}

	@Override
	public int executeHql(String hql, Map<String, Object> params) {
		Query q = getSession().createQuery(hql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return q.executeUpdate();
	}
	@Override
	public int executeHql(String hql, Object... params)
	{
		Query q = getSession().createQuery(hql);
		for (int i = 0; params != null && i < params.length; i++) 
		{
			q.setParameter(i, params[i]);
		}
		return q.executeUpdate();
	}

	@Override
	public List<Map> findBySql(String sql) {
		SQLQuery q = getSession().createSQLQuery(sql);
		return q.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
	}

	@Override
	public List<Map> findBySql(String sql, int page, int rows) {
		SQLQuery q = getSession().createSQLQuery(sql);
		return q.setFirstResult((page - 1) * rows).setMaxResults(rows).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
	}

	@Override
	public List<Map> findBySql(String sql, Map<String, Object> params) {
		SQLQuery q = getSession().createSQLQuery(sql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return q.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
	}

	@Override
	public List<Map> findBySql(String sql, Map<String, Object> params, int page, int rows) {
		SQLQuery q = getSession().createSQLQuery(sql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return q.setFirstResult((page - 1) * rows).setMaxResults(rows).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
	}

	@Override
	public int executeSql(String sql) {
		SQLQuery q = getSession().createSQLQuery(sql);
		return q.executeUpdate();
	}

	@Override
	public int executeSql(String sql, Map<String, Object> params) 
	{
		SQLQuery q = getSession().createSQLQuery(sql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return q.executeUpdate();
	}
	
	@Override
	public int executeSql(String sql, Object... params)
	{
		SQLQuery q = getSession().createSQLQuery(sql);
		for(int i = 0; params != null && i < params.length; i++)
		{
			q.setParameter(i, params[i]);
		}
		return q.executeUpdate();
	}

	@Override
	public BigInteger countBySql(String sql) 
	{
		SQLQuery q = getSession().createSQLQuery(sql);
		return (BigInteger) q.uniqueResult();
	}

	@Override
	public BigInteger countBySql(String sql, Map<String, Object> params) 
	{
		SQLQuery q = getSession().createSQLQuery(sql);
		if (params != null && !params.isEmpty())
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return (BigInteger) q.uniqueResult();
	}
	
	@Override
	public BigInteger countBySql(String sql, Object... params) 
	{
		SQLQuery q = getSession().createSQLQuery(sql);
		for(int i = 0; params != null && i < params.length; i++)
		{
			q.setParameter(i, params[i]);
		}
		return (BigInteger) q.uniqueResult();
	}



	@Override
	public T getByHql(String hql) 
	{
		Query q = getSession().createQuery(hql);
		List<T> l = q.list();
		if (l != null && l.size() > 0)
		{
			return l.get(0);
		}
		return null;
	}


	@Override
	public T getByHql(String hql, Map<String, Object> params) 
	{
		Query q = getSession().createQuery(hql);
		if (params != null && !params.isEmpty())
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		List<T> l = q.list();
		if (l != null && l.size() > 0) 
		{
			return l.get(0);
		}
		return null;
	}
	
	@Override
	public T getByHql(String hql, Object... params) 
	{
		Query q = getSession().createQuery(hql);
		
		for(int i = 0; params != null && i < params.length; i++)
		{
			q.setParameter(i, params[i]);
		}
		List<T> l = q.list();
		if (l != null && l.size() > 0) 
		{
			return l.get(0);
		}
		return null;
	}


	@Override
	public Object UniqueResult(String hql) {
		Query q = getSession().createQuery(hql);
		return  q.uniqueResult();
	}

	@Override
	public Object UniqueResult(String hql, Map<String, Object> params) {
		Query q = getSession().createQuery(hql);
		if (params != null && !params.isEmpty()) 
		{
			for (String key : params.keySet()) 
			{
				q.setParameter(key, params.get(key));
			}
		}
		return  q.uniqueResult();
	}
	
	@Override
	public Object UniqueResult(String hql, Object... params)
	{
		Query q = getSession().createQuery(hql);
		for (int i = 0; params != null && i < params.length; i++) 
		{
			q.setParameter(i, params[i]);
		}
		return q.uniqueResult();
	}

	
}
