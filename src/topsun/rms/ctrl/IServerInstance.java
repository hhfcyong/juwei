package topsun.rms.ctrl;

public interface IServerInstance
{
	/**
	 * 初始化服务器实例
	 * @param strServerId
	 * @throws Exception
	 */
	void Init(String strServerId) throws Exception;
	
	
	/**
	 * 启动服务器
	 * @throws Exception
	 */
	void StartServer() throws Exception;
	
	
	
	/**
	 * 关闭服务器
	 * @throws Exception
	 */
	void ShutdownServer() throws Exception;
	
	
	
	
	/**
	 * 处理远程请求
	 * @param iRemoteActionContext
	 * @throws Exception
	 */
	MessagePackage ProcessRemoteAction(IRemoteAction iRemoteActionContext) throws Exception;
	
	
	/**
	 * 获取服务器标识
	 * @return
	 */
	String getServerId();
	
	
	/**
	 * 判断服务器是否正在关闭
	 * @return
	 */
	 boolean isServerShuttingDown();
	
	
	/**
	 * 判断服务器是否已经启动
	 * @return
	 */
	 boolean isServerStart();

}
