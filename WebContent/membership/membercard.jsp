<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>会员卡管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		function formatMembercardtype(val,row){if(val !=null) return val.name; else return ''; }
		function formatMember(val,row){if(val !=null) return val.code; else return ''; }
		function formatEnable(val,row){
			if(val !=null){
				 if(val == '0') return '停用';
				 else return '启用';
			}
			else return ''; 
		}
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/membership/membercardform.jsp');
		}
		
		$(function(){
			
		});
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					会员卡名:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch').form('clear');;reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="会员卡管理" class="easyui-datagrid" url="membercard/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				    <th field="code" width="250">会员卡卡号</th>
					<th field="name" width="200">会员卡名</th>
					<th field="membercardtype" width="200" formatter="formatMembercardtype" >会员卡类型</th>
				    <th field="member" width="200" formatter="formatMember" >会员编码</th>
					<th field="enable" width="200" formatter="formatEnable">状态</th>
					<th field="issuedate" width="200">发卡日期</th>
					<th field="opendate" width="200">有效开始时间</th>
					<th field="closedate" width="200">有效截止时间</th>
					<th field="point" width="200">积分</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("usermanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/membership/membercardform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usermanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/membership/membercardform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usermanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/membership/membercard/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usermanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/membership/membercardform.jsp')">查看</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
	
  </body>
</html>
