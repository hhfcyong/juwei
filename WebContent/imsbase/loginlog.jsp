<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>登录日志管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	
		
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		
		 //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		
		$(function(){
			
		/* 不重复定义，会造成重复加载
		$('#dg').datagrid({
				onDblClickRow:function(rowIndex, rowData){
					editFun();
				},
				url:topsun.contextPath+'/imsbase/user2/list.do'
			});
		*/
		});
		
		//formate 资源类型   资源类型为一个Object
		 function formatLogtype(val,row){ if(val=='1') return '登录';else return '注销'; }
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					登录日志名:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch')[0].reset();reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="登录日志管理" class="easyui-datagrid" url="loginlog/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="createdatetime" sortOrder="desc"
				toolbar="#toolbar" pagination="true" 
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				    <th field="id" width="250">登录日志标识</th>
					<th field="name" width="200">登录名</th>
					<th field="ip" width="200" >ip地址</th>
					<th field="createdatetime" width="200" >时间</th>
					<th field="type" width="200" formatter="formatLogtype">类型</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("usermanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/imsbase/loginlog/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usermanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/imsbase/loginlog/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
	
  </body>
</html>
