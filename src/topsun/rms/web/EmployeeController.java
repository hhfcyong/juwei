package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.Employee;
import topsun.rms.services.IBaseService;

/**
 * 员工 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/emp")
public class EmployeeController extends BaseController<Employee>
{
	@Resource(name = "employeeService")  
	public void setService(IBaseService<Employee> Service1) 
	{  
		super.setService(Service1);
	}
	
}
