package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Item;
import topsun.rms.entity.User;
import topsun.rms.entity.UserRole;



/**
 * 单品 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("itemService")
public class ItemService extends BaseService<Item>  implements IItemService
{
	@Resource(name = "itemDao")  
	public void setDao(IBaseDao<Item> dao) 
	{  
		super.setDao(dao);  
	}
	@Override
	public List<Item> findItemByFilter(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from Item t join t.promotions promotion";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}
	
}
