<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
	request.setCharacterEncoding("UTF-8");
	String contextPath = request.getContextPath();
	SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>资源管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=contextPath %>/js/zTree/jquery.ztree.core-3.5.min.js"></script>
    <link rel="stylesheet" href="<%=contextPath %>/style/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript">
	
		//排序字段，方向
		var sortName; 
		var sortOrder;
	
		var selectform = function($dialog, $grid, id) {
			if ($('form').form('validate')) {
				
				var arr =$('#dg').datagrid('getChecked');
				if(arr.length <=0){
					$.messager.show({
						title:'提示信息!',
						msg:'至少选择一行记录!'
					});
					return;
				}
				
				var ids = '';
				for(var i =0 ;i<arr.length;i++){
					ids += arr[i].id + ',' ;
				}
				var roleid = id;
			
				$.post(topsun.contextPath + '/imsbase/role/addres.do' ,
						{id:roleid,ids:ids} , function(result){
						if (result.success){
							 $dialog.dialog('destroy');		 
							 $grid.datagrid('reload');	 	
						}else {
							$.messager.show({	 
								title: 'Error',
								msg: 'error'
							});
						}
				},'json');
			}
		};
		

		//根据查询条件 重载datagird
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		//formate 资源类型   资源类型为一个Object
		 function formatResourcetype(val,row){if(val !=null) return val.name; else return ''; }
		
		 var zTreeObj;
		 var setting = {
					data: {
						simpleData: {
							enable: true
						}
					},
					callback: {
						onClick: onZTreeClick
					}
		 
		 
				};
		 function onZTreeClick(event, treeId, treeNode) {
				$('#dg').datagrid('load',{'QUERY_t#resource#id_S_EQ':treeNode.id});
			}
		 //刷新树图
   		function refreshZTree(){
   			 jQuery.ajax( {   
		          type : 'GET',   
		          contentType : 'application/json',   
		          url : topsun.contextPath + '/imsbase/res/getZTreeMainMenu.do',   
		          dataType : 'json',   
		          success : function(data) {   
						zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, data);
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        }); 
   		
   		}
		   $(document).ready(function(){
			   refreshZTree();
			   $("#expandAllBtn").bind("click", {type:"expandAll"}, expandNode);
			   $("#collapseAllBtn").bind("click", {type:"collapseAll"}, expandNode);
		   //   zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
		   });
		   
		   function expandNode(e) {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				type = e.data.type,
				nodes = zTree.getSelectedNodes();
				if (type.indexOf("All")<0 && nodes.length == 0) {
					alert("请先选择一个父节点");
				}

				if (type == "expandAll") {
					zTree.expandAll(true);
				} else if (type == "collapseAll") {
					zTree.expandAll(false);
				} else {
					var callbackFlag = $("#callbackTrigger").attr("checked");
					for (var i=0, l=nodes.length; i<l; i++) {
						zTree.setting.view.fontCss = {};
						if (type == "expand") {
							zTree.expandNode(nodes[i], true, null, null, callbackFlag);
						} else if (type == "collapse") {
							zTree.expandNode(nodes[i], false, null, null, callbackFlag);
						} else if (type == "toggle") {
							zTree.expandNode(nodes[i], null, null, null, callbackFlag);
						} else if (type == "expandSon") {
							zTree.expandNode(nodes[i], true, true, null, callbackFlag);
						} else if (type == "collapseSon") {
							zTree.expandNode(nodes[i], false, true, null, callbackFlag);
						}
					}
				}
			}
		   //双击表格触发
		   function onDblClickRow(rowIndex, rowData){
			   topsun.editFun($('#dg'), '/imsbase/resourceform.jsp');
			}
		   //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		   $(function(){
				/*
				$('#dg').datagrid({
					onDblClickRow:function(rowIndex, rowData){
						editFun();
					}
				});
				*/
			    sortName = $('#dg').attr("sortname");
				sortOrder = $('#dg').attr("sortorder");
			});
		   
			
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%" >
    <div region="center"  style="padding: 5px"   >
	    <div class="easyui-layout" fit="true" >
			<div region="north" style="padding: 5px" border="false" >
				<fieldset>
					<legend>信息查询</legend>
					<form id="mysearch" method="post">
							资源名称:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
							资源编码:<input name="QUERY_t#code_S_LK" class="easyui-validatebox"  value="" />
							<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
							<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch')[0].reset();reload();" >清空</a>
					</form>
				</fieldset>
			</div>
			<div region="center" border="false" onselectstart="return false;" style="-moz-user-select:none;" >
				<table id="dg" title="资源管理" class="easyui-datagrid"  
						iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
			        	CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
						url="res/list.do" fit="true" striped="true" sortname="sortno" sortorder="asc"
						toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow,onSortColumn:onSortColumn"
						rownumbers="true" fitColumns="true" 
						loadMsg="数据正在加载,请耐心的等待..." >
					<thead frozen="true">
						<tr>
							<th field="ck" checkbox="true"></th>
						</tr>
					</thead>
					<thead>
						<tr>
							<th field="name" width="40" sortable="true">资源名称</th>
							<th field="code" width="40" sortable="true">编码</th>
							<th field="iconCls" width="50" sortable="true">资源图标</th>
							<th field="url" width="50" sortable="true">资源路径</th>
							<th field="resourcetype" width="20"  formatter="formatResourcetype" sortable="true">资源类型</th>
							<th field="description" width="50" sortable="true">资源描述</th>
							<th field="sortno" width="50" sortable="true">排序值</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div region="west" hide="true" split="true" iconCls="ext-icon-book" title="资源菜单" style="width:210px;padding:2px" id="west" border="false" >
		<div class="easyui-layout" data-options="fit:true">
        	<div data-options="region:'north'" style="height:30px;padding:0px">
            	<a href="#" class="easyui-linkbutton" iconCls="ext-icon-arrow_refresh" plain="true" onclick="refreshZTree()">刷新</a>
				<a id="expandAllBtn" href="#" class="easyui-linkbutton" iconCls="yxb-icon ztree-expand" plain="true" >展开</a>
				<a id="collapseAllBtn" href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" >折叠</a>
          	</div>
	        <div data-options="region:'center'" style="padding:0px">
	           <ul id="treeDemo" class="ztree"></ul>
	        </div>
		</div>
	</div>
				
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("resourcemanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/imsbase/resourceform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/imsbase/resourceform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/imsbase/res/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("resourcemanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/imsbase/resourceform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("resourcemanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg'),'/imsbase/resourceform.jsp')">复制</a></td>
				<%}%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/imsbase/res/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="ext-icon-arrow_refresh" plain="true" onclick="reload()">所有</a></td>
			</tr>
		</table>
	</div>
	
	
  </body>
</html>
