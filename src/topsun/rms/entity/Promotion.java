package topsun.rms.entity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 员工 
 * 
 * @author zy
 *
 */
@SuppressWarnings("serial")
public class Promotion implements java.io.Serializable
{
	private int id;
	
	private String promotionNO;
	private String promotionName;
	private String description;
	private Date   formulateDate;

	public String getFormulateDate2() {
		return formulateDate2;
	}
	public void setFormulateDate2(String formulateDate2) {
		this.formulateDate2 = formulateDate2;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			setFormulateDate(formatter.parse(formulateDate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String formulateDate2;
	private User    user;
	private PromotionType    promotionType;
	private PromotionRange    promotionRange;
	private String value1;
	private String value2;
	private Set<Item> items=new HashSet<Item>(0);
	public Set<Item> getItems() {
		return items;
	}
	public void setItems(Set<Item> items) {
		this.items = items;
	}
	public int getId() {
		 return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPromotionNO() {
		return promotionNO;
	}
	public void setPromotionNO(String promotionNO) {
		this.promotionNO = promotionNO;
	}
	public String getPromotionName(){
		return promotionName;
	}
	public void setPromotionName(String promotionName){
		this.promotionName=promotionName;
	}
	public String getDescription(){
		return description;
	}
	public void setDescription(String description)
	{
        this.description=description;	
	}
	
	public Date getFormulateDate() {
		return formulateDate;
	}
	public void setFormulateDate(Date formulateDate) {
		this.formulateDate = formulateDate;
	}
	
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public PromotionType getPromotionType() {
		return promotionType;
	}
	public void setPromotionType(PromotionType promotionType) {
		this.promotionType = promotionType;
	}
	public PromotionRange getPromotionRange() {
		return promotionRange;
	}
	public void setPromotionRange(PromotionRange promotionRange) {
		this.promotionRange = promotionRange;
	}
}
