package demo;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import topsun.framework.web.SRFExHttpServlet;

/**
 * @author guohua
 */
public class HelloServlet extends SRFExHttpServlet 
{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		resp.setCharacterEncoding("utf-8");   
		resp.setContentType("text/html; charset=utf-8");   
		
	    String strParam = req.getParameter("SRFDEID") !=null ? req.getParameter("SRFDEID"):"";
	    
	    if("DE01".equals(strParam))
	    {
	    	resp.getWriter().println("Hello World! success");
	    }
	    else
	    {
	    	resp.getWriter().println("Hello World! faild");
	    }
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		super.doPost(req, resp);
	}

}
