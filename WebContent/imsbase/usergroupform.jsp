<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/imsbase/usergroup/update.do';
				
			} else {
				url = topsun.contextPath + '/imsbase/usergroup/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	 if (model == "add"){
		        		 getById(data.obj);
		        	 }
		        	 else getById(entityid );
					 model = 'edit';
					 $(':input[name="id"]').attr("readonly","readonly");
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	var addRoleFun = function() {
		if (entityid == "") {
			$.messager.show({
				title: '提示',
				msg: '请先保存用户'
			});
			return;
		}
	  var dialog = parent.topsun.modalDialog({
			title : '选择信息',
			url : topsun.contextPath + '/imsbase/userrole.jsp',
			buttons : [ {
				text : '选择',
				iconCls : 'icon-edit',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.selectform(dialog, $('#dguserrole'), entityid,'usergroup');
				}
			} ]
		});
	};
	
	var removeRoleFun = function() {
		var arr =$('#dguserrole').datagrid('getChecked');
		if(arr.length <=0){
			$.messager.show({
				title:'提示信息!',
				msg:'至少选择一行记录进行删除!'
			});
			return;
		} else {
			$.messager.confirm('提示信息' , '确认删除?' , function(r){
					if(r){
							var ids = '';
							for(var i =0 ;i<arr.length;i++){
								ids += arr[i].id + ',' ;
							}
							ids = ids.substring(0 , ids.length-1);
							$.post(topsun.contextPath + '/imsbase/user2/deleterole.do' ,
									{id:entityid,ids:ids} , function(result){
									if (result.success){
										$('#dguserrole').datagrid('reload');
										$('#dguserrole').datagrid('unselectAll');
									}else {
										$.messager.show({	 
											title: 'Error',
											msg: 'error'
										});
									}
							},'json');
							
					} 
			});
		}
	};
	
	var addUserFun = function() {
		if ($(':input[name="id"]').val().length <= 0) {
			$.messager.show({
				title: '提示',
				msg: '请先保存'
			});
			return;
		}
      var id =''+ $(':input[name="id"]').val();
	  var dialog1 = parent.topsun.modalDialog({
			title : '选择信息',
			url : topsun.contextPath + '/imsbase/usermanager.jsp',
			buttons : [ {
				text : '选择',
				iconCls : 'icon-edit',
				handler : function() {
					dialog1.find('iframe').get(0).contentWindow.selectform(dialog1, $('#dguserdetail'), id,'usergroup');
				}
			} ]
		});
	};
	
	var removeUserFun = function() {
		var id = $(':input[name="id"]').val();
		var arr =$('#dguserdetail').datagrid('getSelections');
		if(arr.length <=0){
			$.messager.show({
				title:'提示信息!',
				msg:'至少选择一行记录进行删除!'
			});
			return;
		} else {
			$.messager.confirm('提示信息' , '确认删除?' , function(r){
					if(r){
							var ids = '';
							for(var i =0 ;i<arr.length;i++){
								ids += arr[i].id + ',' ;
							}
							ids = ids.substring(0 , ids.length-1);
							$.post(topsun.contextPath + '/imsbase/usergroup/deleteuser.do' ,
									{id:id,ids:ids} , function(result){
									if (result.success){
										$('#dguserdetail').datagrid('reload');
										$('#dguserdetail').datagrid('unselectAll');
									}else {
										$.messager.show({	 
											title: 'Error',
											msg: 'error'
										});
									}
							},'json');
							
					} 
			});
		}
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/imsbase/usergroup/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name
					});
				}
				
				if(model =="edit"){
					$('#dguserrole').datagrid({
						url:topsun.contextPath + '/imsbase/role/getbyusergroupid.do?id='+obj.id
				   });
					$('#dguserdetail').datagrid({
						url:topsun.contextPath + '/imsbase/user2/getbyusergroupid.do?id='+obj.id
				   });
					
				}
				
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:70px;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>用户组标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>用户组名称</th>
					<td><input name="name" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<!-- 
				<tr>
					<th>角色</th>
					<td><select id="roles" multiple class="easyui-combotree" data-options="editable:false,idField:'id',textField:'name',parentField:'pid',url:'<%=contextPath%>/imsbase/role/listall.do'" style="width: 155px;"></select><img class="iconImg ext-icon-cross" onclick="$('#roles').combotree('clear');" title="清空" /></td>
				</tr>
				 -->
			</table>
		</form>
	</div>
	<div id="mainPanle" region="center" style="background: #eee; overflow-y:hidden"  >
      <div id="tabs" class="easyui-tabs"  fit="true" border="false" >
    	<div title="用户明细" style="padding:5px;overflow:hidden; " >
    	  <table id="dguserdetail"  class="easyui-datagrid"  
					 fit="true" striped="true"
					toolbar="#toolbarUser" pagination="true"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">用户名</th>
					</tr>
				</thead>
			</table>
        </div>
        <div title="用户角色" style="padding:5px;overflow:hidden; " >
    	  <table id="dguserrole"  class="easyui-datagrid"  
					 fit="true" striped="true"
					toolbar="#toolbar" pagination="true"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">角色名</th>
						<th field="description" width="50">描述</th>
						<th field="sortno" width="50">排序值</th>
					</tr>
				</thead>
			</table>
        </div>
   	  </div>
  	</div>
  	<div id="toolbar">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addRoleFun()">添加</a>
		 <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeRoleFun()">删除</a>
	</div>
	<div id="toolbarUser">
		<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addUserFun()">添加</a>
		 <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUserFun()">删除</a>
	</div>
</body>
</html>