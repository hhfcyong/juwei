package topsun.rms.services;

import topsun.rms.entity.StorageReceipt;

/**
 * 自定义仓库收货服务接口
 * 
 * @author Tang
 *
 */
public interface IStorageReceiptService extends IBaseService<StorageReceipt>
{

}
