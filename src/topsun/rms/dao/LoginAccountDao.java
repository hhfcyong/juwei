package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.LoginAccount;

/**
 * 登录账号Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("loginAccountDao")
@Transactional  
public class LoginAccountDao extends BaseDao<LoginAccount> {

}
