<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var membergrade;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/membership/member/update.do';
				
			} else {
				url = topsun.contextPath + '/membership/member/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			
			if(typeof(membergrade)!="undefined" && membergrade!='') {
				jsonuserinfo = jsonuserinfo.substring(0,jsonuserinfo.length-1);
				jsonuserinfo =  jsonuserinfo  +',' +membergrade+'}';
			}
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
		     		  $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		
		$('#selmembergrade').combobox({
			onSelect:function(row){
				membergrade = '"membergrade":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/membership/member/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'code' : obj.code,
						'sex' : obj.sex,
						'birthday' : obj.birthday,
						'mobile' : obj.mobile,
						'phone' : obj.phone,
						'qq' : obj.qq,
						'microletter' : obj.microletter,
						'othercontactway' : obj.othercontactway,
						'address' : obj.address,
						'email' : obj.email,
						'idnumber' : obj.idnumber,
						'createdate' : obj.createdate,
						'createora' : obj.createora,
						'createuser' : obj.createuser,
						'point' : obj.point,
						'pw' : obj.pw
						
					});
					if(obj.agegroup !=''){
						$('#agegroup').combobox('select', obj.agegroup );
					}
					if(obj.membergrade !=null ){
						$('#selmembergrade').combobox('select', obj.membergrade.id );
					}
				}
				if(model =="edit"){
					$('#dgmembercard').datagrid({
						url:topsun.contextPath + '/membership/membercard/getbymemberid.do?id='+obj.id
				   });
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	
	 var addMemberCardFun = function() {
			if (entityid == "") {
				$.messager.show({
					title: '提示',
					msg: '请先保存会员信息'
				});
				return;
			}
		  var dialog = parent.topsun.modalDialog({
				title : '选择信息',
				url : topsun.contextPath + '/membeship/membercard.jsp',
				buttons : [ {
					text : '选择',
					iconCls : 'icon-edit',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.selectform(dialog, $('#dgmembercard'), entityid,'member');
					}
				} ]
			});
		};
		
		function formatMembercardtype(val,row){if(val !=null) return val.name; else return ''; }
		function formatMember(val,row){if(val !=null) return val.code; else return ''; }
		function formatEnable(val,row){
			if(val !=null){
				 if(val == '0') return '停用';
				 else return '启用';
			}
			else return ''; 
		}
	 
	
</script>
</head>
<body class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false" style="padding: 5px;"  >
		<form method="post" class="form">
			<table class="table" style="width: 100%;font-size: 12px;"  >
				<tr>
					<th>会员标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>会员名称</th>
					<td><input name="name" class="easyui-validatebox"  /></td>
				</tr>
				<tr>
					<th>会员编号</th>
					<td><input name="code" class="easyui-validatebox" data-options="required:true" /></td>
					<th>性别</th>
					<td>
						<select id = "selsex" name="sex" class="easyui-combobox" data-options="required:true,editable:false,panelHeight:'auto'" style="width: 155px;">
							<option value="1">男</option>
	        				<option value="0">女</option>
        				</select>
        		   </td>
				
				</tr>
				<tr>
				    <th>年龄段</th>
					<td><select id="agegroup" name="agegroup" class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/dic/listbydictype.do?dictype=agegroup',panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>生日</th>
					<td><input name="birthday" class="easyui-datebox" data-options="editable:false"/></td>
				</tr>
				
				<tr>
				    <th>手机号</th>
					<td><input name="mobile" class="easyui-validatebox"  /></td>
					<th>固定电话</th>
					<td><input name="phone" class="easyui-validatebox"  /></td>
				</tr>
				
				<tr>
				    <th>QQ号</th>
					<td><input name="qq" class="easyui-validatebox"  /></td>
					<th>微信</th>
					<td><input name="microletter" class="easyui-validatebox"  /></td>
				</tr>
				
				<tr>
				    <th>其他联系方式</th>
					<td><input name="othercontactway" class="easyui-validatebox"  /></td>
					<th>地址</th>
					<td><input name="address" class="easyui-validatebox"    /></td>
				</tr>
				
				<tr>
				    <th>电子邮箱</th>
					<td><input name="email" class="easyui-validatebox"   data-options="validType:'email'"/></td>
					<th>身份证号码</th>
					<td><input name="idnumber" class="easyui-validatebox"  /></td>
				</tr>
				
				<tr>
				    <th>办理机构</th>
					<td><input name="createora" class="easyui-validatebox"  /></td>
					<th>办理人</th>
					<td><input name="createuser" class="easyui-validatebox"  /></td>
				</tr>
				
				<tr>
				    <th>办理日期</th>
					<td><input name=createdate class="easyui-datebox"   /></td>
					<th>等级</th>
					<td><select id="selmembergrade"  class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/membership/membergrade/listall.do',panelHeight:'auto'" style="width: 155px;"></select><img class="iconImg ext-icon-cross" onclick="$('#membergrade').combobox('clear');" title="清空" /></td>
				</tr>
				
				<tr>
				    <th>积分</th>
					<td><input name=point class="easyui-validatebox"  /></td>
					<th>密码</th>
					<td><input name="pw"  type="password" class="easyui-validatebox"  /></td>
				</tr>
				
			</table>
		</form>
	</div>
	<div id="mainPanle" region="center" style="background: #eee; overflow-y:hidden"  >
      <div id="tabs" class="easyui-tabs"  fit="true" border="false" >
     	 <div title="会员卡" style="padding:5px;overflow:hidden; " >
    	  <table id="dgmembercard"  class="easyui-datagrid"  
					 fit="true" striped="true" 
					pagination="true"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="code" width="250">会员卡卡号</th>
						<th field="name" width="200">会员卡名</th>
						<th field="membercardtype" width="200" formatter="formatMembercardtype" >会员卡类型</th>
					    <th field="member" width="200" formatter="formatMember" >会员编码</th>
						<th field="enable" width="200" formatter="formatEnable">状态</th>
						<th field="issuedate" width="200">发卡日期</th>
						<th field="opendate" width="200">有效开始时间</th>
						<th field="closedate" width="200">有效截止时间</th>
						<th field="point" width="200">积分</th>
					</tr>
				</thead>
			</table>
        </div>
   	  </div>
  	</div>
</body>
</html>