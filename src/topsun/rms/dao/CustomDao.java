package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.User;

/**
 * 自定义DAO的实现类 继承BaseDao 实现自定义DAO接口 
 * @author yuxiaobei
 *
 */
@Repository("customDao")
@Transactional  
public class CustomDao extends BaseDao<User> implements ICustomDao 
{

}
