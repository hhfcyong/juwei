package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.MemberCardType;

/**
 * 会员卡类型 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("memberCardTypeDao")
@Transactional  
public class MemberCardTypeDao extends BaseDao<MemberCardType> 
{
	
}
