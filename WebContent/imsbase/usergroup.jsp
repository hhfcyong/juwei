<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>用户管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/imsbase/usergroupform.jsp');
		}
		 //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		 function exportExcel()
			{
				$.messager.confirm('提示信息' , '确认导出当前分页，取消导出所有数据?' ,  function(r){
					if(r){
						//get方式
						//页码，一页数量，排序列，排序方向
						var options = $('#dg').datagrid('getPager').data("pagination").options;
						var params = 'page='+options.pageNumber+'&rows='+options.pageSize;
						if(typeof(sortName)!="undefined" && sortName!='' && typeof(sortOrder)!="undefined" && sortOrder!=''  ){
							params+='&sort='+sortName+'&order='+sortOrder;
						}
						//增加过滤条件
						var url =topsun.contextPath + '/imsbase/user2/exportexcel.do'+'?'+params+'&'+$('#mysearch').serialize();  
				 	 	window.location.href = url;
						
					} else{
						var params = 'exportmodel=all';
						if(typeof(sortName)!="undefined" && sortName!='' && typeof(sortOrder)!="undefined" && sortOrder!=''  ){
							params+='&sort='+sortName+'&order='+sortOrder;
						}
						//增加过滤条件
						var url =topsun.contextPath + '/imsbase/user2/exportexcel.do'+'?'+params+'&'+$('#mysearch').serialize();  
				 	 	window.location.href = url;
					}
				}
				);
				
				

			}
		
		$(function(){
			
		/* 不重复定义，会造成重复加载
		$('#dg').datagrid({
				onDblClickRow:function(rowIndex, rowData){
					editFun();
				},
				url:topsun.contextPath+'/imsbase/user2/list.do'
			});
		*/
		});
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					用户组名:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch')[0].reset();reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="用户管理" class="easyui-datagrid" url="usergroup/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				    <th field="id" width="250">用户组标识</th>
					<th field="name" width="200">用户组名</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("usergroupmanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/imsbase/usergroupform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usergroupmanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/imsbase/usergroupform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usergroupmanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/imsbase/usergroup/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usergroupmanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/imsbase/usergroupform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usergroupmanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg'),'/imsbase/usergroupform.jsp')">复制</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usergroupmanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/imsbase/usergroup/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 10px"
			closed="true" buttons="#dlg-buttons"  >
			 <div style="text-align:center">
		<form id="fm" method="post">
			<table cellpadding="5" style="font-size:12px;">
				 <tr>
					<td><label>编号:</label></td>
					<td><input class="easyui-validatebox" type="text" name="id" ></input></td>
				</tr> 
				<tr>
					<td><label>用户组名:</label></td>
					<td><input class="easyui-validatebox" type="text" name="name" data-options="required:true"></input></td>
				</tr>
				
			</table>
		</form>
		</div>
 	</div> 
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveUser()">确定</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">取消</a>
	</div>
  </body>
</html>
