package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Item;

/**
 * 单品  Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("itemDao")
@Transactional  
public class ItemDao extends BaseDao<Item> 
{
	
}
