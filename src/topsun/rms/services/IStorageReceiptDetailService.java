package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.StorageReceiptDetail;
import topsun.rms.entity.User;
/**
 * 自定义仓库要货单明细服务
 * 
 * @author Tang
 *
 */
public interface IStorageReceiptDetailService  extends IBaseService<StorageReceiptDetail>
{
	/**
	 *  查询角色所拥有的用户
	 * 
	 * @param hqlFilter
	 * 
	 * @return
	 */
	public List<StorageReceiptDetail> findStotageReceiptDetailByFilter(String id,String parentId); 
}
