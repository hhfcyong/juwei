package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.DataDictionary;

/**
 * 字典  Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("dataDictionaryDao")
@Transactional  
public class DataDictionaryDao extends BaseDao<DataDictionary> 
{
	
}
