package topsun.rms.entity;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 字典分类
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class DataDictionaryType implements java.io.Serializable
{
	private String id;
	private String name;
	private java.util.Set<DataDictionary> datadictionarys;
	
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public java.util.Set<DataDictionary> getDatadictionarys() {
		return datadictionarys;
	}
	public void setDatadictionarys(java.util.Set<DataDictionary> datadictionarys) {
		this.datadictionarys = datadictionarys;
	}
	
}
