package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.StoreOrder;
import topsun.rms.services.IBaseService;
/**
 * 要货单controller
 * @author fei
 *
 */
@Controller
@RequestMapping("datapage/storeorder")
public class StoreOrderController extends BaseController<StoreOrder>{
	@Resource(name = "storeOrderService")  
	public void setService(IBaseService<StoreOrder> service) 
	{  
		super.setService(service);
	}

}
