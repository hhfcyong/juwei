package topsun.rms.entity;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 员工 
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class PromotionRange implements java.io.Serializable
{
	private int id;
	private String name;
	public int getId() {
		 return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
