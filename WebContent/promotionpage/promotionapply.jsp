<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    //提交直接关闭
    var submitForm = function($dialog, $grid) {		
    	 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/promotionpage/pro/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'promotionNO' : obj.promotionNO,
						'promotionName':obj.promotionName
					});
				}
				if(model =="edit"){
					$('#dgpromotionapply').datagrid({
						
						url:topsun.contextPath + '/promotionpage/proapply/getbypromoationid.do?id='+obj.id+''
				   });
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
	 var editPromotionApplyFun= function() {
			
			var arr =$('#dgpromotionapply').datagrid('getSelections');
			if(arr.length != 1){
				$.messager.show({
					title:'提示信息!',
					msg:'只能选择一行记录进行修改!'
				});
			}else{
			  var row = $('#dgpromotionapply').datagrid('getSelected');
			  if (row){
				  var dialog = parent.topsun.modalDialog({
						title : '编辑',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/promotionpage/promotionapplyform.jsp?id='+row.id,
						buttons : [ {
							text : '确定',
							iconCls : 'icon-ok',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.submitaddForm(dialog, $('#dgpromotionapply'),entityid);
							}
						} ,
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.cancleform(dialog,$('#dgpromotionapply'));
							}
						} ]
					});
			   }
			}
		};
	
	
	
		var addPromotionApplyFun1 = function() {
		  var dialog = parent.topsun.modalDialog({
				title : '新增',
				iconCls : 'icon-add',
				url : topsun.contextPath + '/promotionpage/promotionapplyform.jsp',
				buttons : [ {
					text : '确定',
					iconCls : 'icon-ok',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.submitaddForm(dialog,$('#dgpromotionapply'),entityid);
					}
				} ,
				{
					text : '关闭',
					iconCls : 'icon-cancel',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $('#dgpromotionapply'));
					}
				} ]
			});
	};
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:70px;visible:false;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>方案序号</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>方案编号</th>
					<td><input name="promotionNO" class="easyui-validatebox" /></td>
				</tr>
				<tr>
				    <th>方案名称</th>
				    <td><input name="promotionName" class="easyui-validatebox"/></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="mainPanle" region="center" style="background: #eee; overflow-y:hidden"  >
      <div id="tabs" class="easyui-tabs"  fit="true" border="false" >
     	 <div title="包含单品" style="padding:5px;overflow:hidden; " >
    	  <table id="dgpromotionapply"  class="easyui-datagrid"  
					 fit="true" striped="true"
					pagination="true" toolbar="#toolbar"
					rownumbers="true" fitColumns="false" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
					    <th field="id" width="80">应用序号</th>
						<th field="startdate" width="150">开始日期</th>
						<th field="enddate" width="150">结束日期</th>
						<th field="starttime" width="120">开始时间</th>
						<th field="endtime" width="120">结束时间</th>
						<th field="state1" width="100">是否可用</th>
						<th field="monday" width="80">周一</th>
						<th field="tuesday" width="80">周二</th>
						<th field="wednesday" width="80">周三</th>
						<th field="thursday" width="80">周四</th>
						<th field="friday" width="80">周五</th>
						<th field="saturday" width="80">周六</th>
						<th field="sunday" width="80">周末</th>
					</tr>
				</thead>
			</table>
        </div>       
   	  </div>
  	</div>
  <div id="toolbar">
     	<table>
			<tr>
			<!--  	<%if (securityUtil.havePermission("PromotionApply.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addPromotionApplyFun($('#dgpromotionapply'),'/promotionpage/promotionapplyform.jsp','<%=id%>')">新增</a></td>
				<%}%>-->
				<%if (securityUtil.havePermission("PromotionApply.add")) {%>
				<td> <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addPromotionApplyFun1()">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("PromotionApply.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td> <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="editPromotionApplyFun()">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("PromotionApply.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dgpromotionapply'),'/promotionpage/proapply/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("PromotionApply.view1")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dgpromotionapply'),'/promotionpage/promotionapplyform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("PromotionApplyShop.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dgpromotionapply'),'/promotionpage/promotionShopform.jsp')">店铺明细</a></td>
				<%}%>
			</tr>
		</table>
	</div>
	
	
</body>
</html>