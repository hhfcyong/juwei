<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>用户管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
		var selectform = function($dialog, $grid, userid,type) {
			if ($('form').form('validate')) {
				
				var url ;
				
				if(type =='user'){
					 url = topsun.contextPath + '/imsbase/user2/grantrole.do';
				}
				if(type =='usergroup'){
					 url = topsun.contextPath + '/imsbase/usergroup/grantrole.do';
				}
				
				var arr =$('#dg').datagrid('getChecked');
				if(arr.length <=0){
					$.messager.show({
						title:'提示信息!',
						msg:'至少选择一行记录进行删除!'
					});
					return;
				}
				
				var ids = '';
				for(var i =0 ;i<arr.length;i++){
					ids += arr[i].id + ',' ;
				}
				var userid = userid;
			
				$.post(url ,
						{id:userid,ids:ids} , function(result){
						if (result.success){
							 $dialog.dialog('destroy');		 
							 $grid.datagrid('reload');	 	
						}else {
							$.messager.show({	 
								title: 'Error',
								msg: 'error'
							});
						}
				},'json');
			}
		};
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/imsbase/userroleform.jsp');
		}
		
		$(function(){
			/*$('#dg').datagrid({
				onDblClickRow:function(rowIndex, rowData){
					editFun();
				}
			});
			*/
		});
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%" >
	
		<div region="north" style="padding: 5px 5px 0px 5px;" border="false"  >
			<fieldset>
				<legend>信息查询</legend>
				<form id="mysearch" method="post">
						角色名:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
						<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
						<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch')[0].reset();reload();" >清空</a>
				</form>
			</fieldset>
		</div>
		<div region="center" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false" onselectstart="return false;"   >
			<table id="dg" title="角色管理" class="easyui-datagrid"  
					iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
			        CheckOnSelect = "false" singleSelect="true" pageSize="20"
					url="role/list.do" fit="true" striped="true"
					sortName="sortno" sortOrder="asc"
					toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">角色名</th>
						<th field="description" width="50">描述</th>
						<th field="sortno" width="50">排序值</th>
					</tr>
				</thead>
			</table>
		</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("rolemanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/imsbase/userroleform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("rolemanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/imsbase/userroleform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("rolemanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/imsbase/role/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("rolemanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/imsbase/userroleform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("rolemanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg'),'/imsbase/userroleform.jsp')">复制</a></td>
				<%}%>
				<%if (securityUtil.havePermission("rolemanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/imsbase/role/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%>
			</tr>
		</table>
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 10px"
			closed="true" buttons="#dlg-buttons"  >
			 <div style="text-align:center">
		<form id="fm" method="post">
			<table cellpadding="5" style="font-size:12px;">
				 <tr>
					<td><label>编号:</label></td>
					<td><input class="easyui-validatebox" type="text" name="id" ></input></td>
				</tr> 
				<tr>
					<td><label>角色名:</label></td>
					<td><input class="easyui-validatebox" type="text" name="name" data-options="required:true"></input></td>
				</tr>
				<tr>
					<td>排序值:</td>
					<td><input name="sortno" class="easyui-numberbox" data-options="required:true,min:0,max:100000" value="100"></input></td>
				</tr>
				<tr>
					<td>资源描述:</td>
					<td><textarea name="description"></textarea></td>
				</tr>
				
			</table>
		</form>
		</div>
 	</div> 
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveFun()">确定</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">取消</a>
	</div>
  </body>
</html>
