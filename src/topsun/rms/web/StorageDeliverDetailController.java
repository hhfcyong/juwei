package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.StorageDeliverDetail;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("storage/deliverDetail")
public class StorageDeliverDetailController extends BaseController<StorageDeliverDetail> 
{
	@Resource(name = "storageDeliverDetailService")  
	public void setService(IBaseService<StorageDeliverDetail> Service1) 
	{  
		super.setService(Service1);		
	}
		
}