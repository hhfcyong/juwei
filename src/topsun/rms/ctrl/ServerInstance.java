package topsun.rms.ctrl;

import topsun.rms.common.RmsErrors;
import topsun.rms.ctrl.data.RmsServer;

public class ServerInstance implements IServerInstance
{
	protected RmsServer rmsServer = null;
	/**
	 * 服务器启动
	 */
	private boolean bServerStart = false;
	
	
	private boolean bServerShuttingDown = false;
	
	@Override
	public void Init(String strServerId) throws Exception 
	{
		RmsServer rmsServer = new RmsServer();
		rmsServer.setServerId(strServerId);
		
		this.rmsServer = rmsServer;
		OnInit();
	}
	
	protected void OnInit() throws Exception
	{
		
	}

	@Override
	public void StartServer() throws Exception
	{
		if(isServerStart())
		{
			throw new Exception("服务器已经启动");
		}
		
		OnStartServer();
		this.bServerStart = true;
	}
	
	protected void OnStartServer() throws Exception
	{
		
	}

	@Override
	public void ShutdownServer() throws Exception
	{
		if(!isServerStart())
		{
			throw new Exception("服务器未启动");
		}
		this.bServerShuttingDown = true;
		OnBeforeShutdownServer();
		OnShutdownServer();
		this.bServerStart = false;
		
	}
	
	protected void OnBeforeShutdownServer() throws Exception
	{
	
	}
	
	protected void OnShutdownServer() throws Exception
	{
		
	}


	@Override
	public String getServerId() 
	{
		return rmsServer.getServerId();
	}

	@Override
	public boolean isServerShuttingDown()
	{
		return this.bServerShuttingDown;
	}

	@Override
	public boolean isServerStart()
	{
		return this.bServerStart;
	}
	
	@Override
	public MessagePackage ProcessRemoteAction(IRemoteAction iRemoteActionContext) throws Exception
	{
		if(this.isServerShuttingDown())
		{
			throw new RmsException(RmsErrors.Error);
		}
		
		return OnProcessRemoteAction(iRemoteActionContext);
	}
	
	protected MessagePackage OnProcessRemoteAction(IRemoteAction iRemoteActionContext) throws Exception
	{
		throw new Exception("没有处理的远程请求"+iRemoteActionContext.getAction());
	}

}
