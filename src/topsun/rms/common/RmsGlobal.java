package topsun.rms.common;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author yuxiaobei
 *
 */
public class RmsGlobal 
{
	/**
	 * 不需要sessoin的uri
	 */
	public static Set<String> doNotNeedSessionUri = new HashSet<String>();
	
	static 
	{
		doNotNeedSessionUri.add("/imsbase/user2/login.do");
		doNotNeedSessionUri.add("/imsbase/user2/logout.do");
		doNotNeedSessionUri.add("/imsbase/res/getMenu.do");
		
	}
}
