<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
	request.setCharacterEncoding("UTF-8");
	String contextPath = request.getContextPath();
	SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>资源管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
    <script type="text/javascript" src="<%=contextPath %>/js/zTree/jquery.ztree.core-3.5.min.js"></script>
    <link rel="stylesheet" href="<%=contextPath %>/style/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript">
	
		//排序字段，方向
		var sortName; 
		var sortOrder;
		var dicActionUrl;
	
		var selectform = function($dialog, $grid, id) {
			if ($('form').form('validate')) {
				
				var arr =$('#dg').datagrid('getChecked');
				if(arr.length <=0){
					$.messager.show({
						title:'提示信息!',
						msg:'至少选择一行记录!'
					});
					return;
				}
				
				var ids = '';
				for(var i =0 ;i<arr.length;i++){
					ids += arr[i].id + ',' ;
				}
				var roleid = id;
			
				$.post(topsun.contextPath + '/imsbase/role/addres.do' ,
						{id:roleid,ids:ids} , function(result){
						if (result.success){
							 $dialog.dialog('destroy');		 
							 $grid.datagrid('reload');	 	
						}else {
							$.messager.show({	 
								title: 'Error',
								msg: 'error'
							});
						}
				},'json');
			}
		};
		

		//根据查询条件 重载datagird
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		 function formatedictype(val,row){  if(val!=null) return val.name;else return '';}
		
		 var zTreeObj;
		 var setting = {
				    view: {
						dblClickExpand: false,
						showLine: true,
						selectedMulti: false
					},
					data: {
						simpleData: {
							enable: true
						}
					},
					callback: {
						onClick: onZTreeClick
					}
		 
		 
				};
		 function onZTreeClick(event, treeId, treeNode) {
			    if(treeNode.id =='head') {
			    	$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
			    }
			    else{
					$('#dg').datagrid('load',{'QUERY_t#datadictionarytype#id_S_EQ':treeNode.id});
			    }
			}
		 //刷新树图
   		function refreshZTree(){
   			 jQuery.ajax( {   
		          type : 'GET',   
		          contentType : 'application/json',   
		          url : topsun.contextPath + '/datapage/dictype/getZTreeMainMenu.do',   
		          dataType : 'json',   
		          success : function(data) {   
						zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, data);
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        }); 
   		
   		}
		   $(document).ready(function(){
			   refreshZTree();
		   });
		   
		   //双击表格触发
		   function onDblClickRow(rowIndex, rowData){
			   topsun.editFun($('#dg'), '/datapage/dictionaryform.jsp');
			}
		   //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		   $(function(){
			    sortName = $('#dg').attr("sortname");
				sortOrder = $('#dg').attr("sortorder");
			});
		   
		   
		   var addDicTypeFun = function() {
			   $('#dlg').dialog('open').dialog('setTitle','新增');
				$('#fm').form('clear');
				dicActionUrl = topsun.contextPath + '/datapage/dictype/add.do';
			};
			
			var editDicTypeFun = function(){
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				var nodes = zTree.getSelectedNodes();
				if (nodes.length > 0) {
					var tId = nodes[0].id;
					
					 $('#dlg').dialog('open').dialog('setTitle','编辑');
					 $('#fm').form('load',nodes[0]);
					 dicActionUrl = topsun.contextPath + '/datapage/dictype/update.do';
				}
			};
			
			 var removeDicTypeFun = function() {
				 var zTree = $.fn.zTree.getZTreeObj("treeDemo");
					var nodes = zTree.getSelectedNodes();
					if (nodes.length > 0) {
						var tId = nodes[0].id;
						if(tId == 'head') return;
						
						$.post(topsun.contextPath + '/datapage/dictype/remove.do' ,
								{ids:tId} , function(result){
								if (result.success){
									refreshZTree();
									$.messager.show({
										title:'提示信息!' , 
										msg:'操作成功!'
									});
								}else {
									$.messager.show({	// show error message
										title: 'Error',
										msg: 'error'
									});
								}
						},'json');
					}
			};
			
			function saveDicType()
			{
				var jsonuserinfo = $.toJSON(topsun.serializeObject($('#fm'))); 
		        jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : dicActionUrl,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $('#dlg').dialog('close');		
		        	  refreshZTree();			           
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
			      
			}
			
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%" >
    <div region="center"  style="padding: 5px"   >
	    <div class="easyui-layout" fit="true" >
			<div region="center" border="false" onselectstart="return false;" style="-moz-user-select:none;" >
				<table id="dg" title="字典明细信息" class="easyui-datagrid"  
						iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
			        	CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
						url="dic/list.do" fit="true" striped="true" 
						toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow,onSortColumn:onSortColumn"
						rownumbers="true" fitColumns="true" 
						loadMsg="数据正在加载,请耐心的等待..." >
					<thead frozen="true">
						<tr>
							<th field="ck" checkbox="true"></th>
						</tr>
					</thead>
					<thead>
						<tr>
							<th field="id" width="40" >字典值</th>
							<th field="name" width="40" >字典名称</th>
							<th field="datadictionarytype" width="40"  formatter="formatedictype">字典类型</th>
						    <th field="sortno" width="40" >排序值</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div region="west" hide="true" split="true" iconCls="ext-icon-book" title="字典类别" style="width:250px;padding:2px" id="west" border="false" >
		<div class="easyui-layout" data-options="fit:true">
        	<div data-options="region:'north'" style="height:30px;padding:0px">
            	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addDicTypeFun()">新增</a></td>
            	<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editDicTypeFun()">编辑</a></td>
            	<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeDicTypeFun()">删除</a></td>
            	<a href="#" class="easyui-linkbutton" iconCls="ext-icon-arrow_refresh" plain="true" onclick="refreshZTree()">刷新</a>
          	</div>
	        <div data-options="region:'center'" style="padding:0px">
	           <ul id="treeDemo" class="ztree"></ul>
	        </div>
		</div>
	</div>
				
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("resourcemanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/datapage/dictionaryform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/datapage/dictionaryform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("resourcemanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/datapage/dic/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("resourcemanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/datapage/dictionaryform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("resourcemanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg'),'/datapage/dictionaryform.jsp')">复制</a></td>
				<%}%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/datapage/dic/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<td><div class="datagrid-btn-separator"></div></td>
			</tr>
		</table>
	</div>
	
	
	<div id="dlg" class="easyui-dialog" style="width:400px;height:280px;padding:10px 10px" closed="true" buttons="#dlg-buttons" data-options="modal:true"  >
		<div style="text-align:center">
			<form id="fm" method="post">
				<table cellpadding="5" style="font-size:12px;">
					<tr>
						<td><label>字典类型标识:</label></td>
						<td><input class="easyui-validatebox" type="text" name="id" data-options="required:true"></input></td>
					</tr>
					<tr>
						<td><label>字典类型名称:</label></td>
						<td><input class="easyui-validatebox" type="text" name="name" data-options="required:true"></input></td>
					</tr>
					
				</table>
			</form>
		</div>
	</div>
	<div id="dlg-buttons">
		<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveDicType()">确定</a>
		<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">取消</a>
	</div>
	
  </body>
</html>
