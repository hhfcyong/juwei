package topsun.rms.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.naming.java.javaURLContextFactory;
import org.omg.CORBA.PRIVATE_MEMBER;

/**
 * 仓库收货 实体模型
 * @author Tang
 *
 */

@SuppressWarnings("serial")
public class StorageReceiptDetail {
	
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	private Item item;
	private String itemName;
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemName() {
		return itemName;
	}
	private BigDecimal quantity;
	private String unitId;

	private BigDecimal price;
	private BigDecimal amount;
	private StorageReceipt storageReceipt;

	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public StorageReceipt getStorageReceipt() {
		return storageReceipt;
	}
	public void setStorageReceipt(StorageReceipt storageReceipt) {
		this.storageReceipt = storageReceipt;
	}
	
}
