<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	String userid = request.getParameter("userid");
	if (userid == null) {
		userid = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    var model = "add";
    var userid;
	var addFromUserform = function($dialog, $grid, id) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/imsbase/loginaccount/update.do';
				
			} else {
				url = topsun.contextPath + '/imsbase/loginaccount/add.do';
			}
			var userinfo = '"user":{"id":"'+ $('#userid').val()+'"}' ;
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
		
			jsonString = jsonString +',' +userinfo+'}';
			
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	 if (model == "add"){
		        		 getById(data.obj);
		        	 }
		        	 else getById(entityid );
					 model = 'edit';
					 $(':input[name="id"]').attr("readonly","readonly");
					 $dialog.dialog('destroy');	
					 $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	
    var model = "add";
    var entityid;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/imsbase/user2/update.do';
				
			} else {
				url = topsun.contextPath + '/imsbase/user2/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	 if (model == "add"){
		        		 getById(data.obj);
		        	 }
		        	 else getById(entityid );
					 model = 'edit';
					 $(':input[name="id"]').attr("readonly","readonly");
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
	    userid = '<%=userid%>';
		if(entityid != ""){
			model = "edit";
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if(userid != ""){
			$('#userid').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.post(topsun.contextPath + '/imsbase/loginaccount/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name
					});
				}
			}, 'json');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:100px;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>用户标识</th>
					<td><input id="userid" value="<%=userid%>" class="easyui-validatebox" data-options="required:true"  /> <img class="iconImg ext-icon-zoom" onclick="showIcons();" title="选择用户" /></td>
					<th>登录账号</th>
					<td><input name="id" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				
				<tr>
					<th>登录密码</th>
					<td><input name="pwd" type="password" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				 
			</table>
		</form>
	</div>
	<div id="mainPanle" region="center" style="background: #eee; overflow-y:hidden"  >
      <div id="tabs" class="easyui-tabs"  fit="true" border="false" >
    	<div title="登录日志" style="padding:5px;overflow:hidden; " >
    	  <table id="dgloginlog"  class="easyui-datagrid"  
					 fit="true" striped="true"
					toolbar="#toolbar" pagination="true"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="name" width="50">登录账号</th>
						<th field="description" width="50">描述</th>
						<th field="sortno" width="50">排序值</th>
					</tr>
				</thead>
			</table>
        </div>
   	  </div>
  	</div>
  	<div id="toolbar">
		 <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeRoleFun()">删除</a>
	</div>
</body>
</html>