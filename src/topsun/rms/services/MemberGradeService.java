package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.MemberGrade;

/**
 * 会员等级 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("memberGradeService")
public class MemberGradeService extends BaseService<MemberGrade>
{
	@Resource(name = "memberGradeDao")  
	public void setDao(IBaseDao<MemberGrade> dao) 
	{  
		super.setDao(dao);  
	}
}
