package topsun.rms.entity;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 员工 
 * 
 * @author zy
 *
 */
@SuppressWarnings("serial")
public class PromotionApply implements java.io.Serializable
{
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPromotionid() {
		return promotionid;
	}
	public void setPromotionid(int promotionid) {
		this.promotionid = promotionid;
	}
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getState1() {
		return state1;
	}
	public void setState1(String state1) {
		this.state1 = state1;
	}
	public String getMonday() {
		return monday;
	}
	public void setMonday(String monday) {
		this.monday = monday;
	}
	public String getTuesday() {
		return tuesday;
	}
	public void setTuesday(String tuesday) {
		this.tuesday = tuesday;
	}
	public String getWednesday() {
		return wednesday;
	}
	public void setWednesday(String wednesday) {
		this.wednesday = wednesday;
	}
	public String getThursday() {
		return thursday;
	}
	public void setThursday(String thursday) {
		this.thursday = thursday;
	}
	public String getFriday() {
		return friday;
	}
	public void setFriday(String friday) {
		this.friday = friday;
	}
	public String getSaturday() {
		return saturday;
	}
	public void setSaturday(String saturday) {
		this.saturday = saturday;
	}
	public String getSunday() {
		return sunday;
	}
	public void setSunday(String sunday) {
		this.sunday = sunday;
	}
	private int id;
	private int promotionid;
	private Date startdate;
	public String getStartdate2() {
		return startdate2;
	}
	public void setStartdate2(String startdate2) {
		this.startdate2 = startdate2;
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		try {
			setStartdate(formatter.parse(startdate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getEnddate2() {
		return enddate2;
	}
	public void setEnddate2(String enddate2) {
		this.enddate2 = enddate2;
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
		try {
			setEnddate(formatter.parse(enddate2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private String startdate2;
	private String enddate2;
	private Date enddate;
	private String starttime;
	private String endtime;
	private String state1;
	private String monday;
	private String tuesday;
	private String wednesday;
	private String thursday;
	private String friday;
	private String saturday;
	private String sunday;


	public Set<Shop> getShops() {
		return shops;
	}
	public void setShops(Set<Shop> shops) {
		this.shops = shops;
	}
	private Set<Shop> shops=new HashSet<Shop>(0);
}
