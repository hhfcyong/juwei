package topsun.framework.utility;

public final class StringHelper
{
	  public static final int Length(String a)
	  {
	    if (a == null) return 0;
	    else return a.length();
	  }
	  
	  public static final boolean IsNullOrEmpty(String a)
	  {
		  return Length(a) == 0;
	  }
	  
	  public static final int Compare(String strA, String strB, boolean bIgnore) 
	  {
		  if(strA == null || strB == null) 
			  return -1; 
		  
		  if(bIgnore)
			  return strA.compareToIgnoreCase(strB);
		  else 
			  return strA.compareTo(strB);
	  }
	  

}
