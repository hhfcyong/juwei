package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.BArea;

/**
 * 大区 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("bAreaDao")
@Transactional  
public class BAreaDao extends BaseDao<BArea> 
{
	
}
