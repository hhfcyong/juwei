package topsun.rms.entity;

import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 字典
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class DataDictionary implements java.io.Serializable
{
	private String id;
	private String name;
	private int sortno;
	private DataDictionaryType datadictionarytype;
							   
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getSortno() {
		return sortno;
	}
	public void setSortno(int sortno) {
		this.sortno = sortno;
	}
	public DataDictionaryType getDatadictionarytype() {
		return datadictionarytype;
	}
	public void setDatadictionarytype(DataDictionaryType datadictionarytype) {
		this.datadictionarytype = datadictionarytype;
	}

	
}
