package topsun.rms.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.DataDictionary;
import topsun.rms.services.IBaseService;

/**
 * 数据字典 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/dic")
public class DataDictionaryController extends BaseController<DataDictionary>
{
	@Resource(name = "dataDictionaryService")  
	public void setService(IBaseService<DataDictionary> Service1) 
	{  
		super.setService(Service1);
	}
	
	/**
	 * 根据dictype查询
	 */
	@RequestMapping(value = "/listbydictype", method = RequestMethod.POST)
	protected void findAllEx(HttpServletRequest request, HttpServletResponse response) 
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addSort("sortno");
		if(paramList.containsKey("dictype"));
		{
			hqlFilter.addFilter("QUERY_t#datadictionarytype#id_S_EQ", paramList.get("dictype"));
		}
		
		writeJson(service.findByFilter(hqlFilter), request, response);
	}
}
