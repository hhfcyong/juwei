package topsun.rms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.Promotion;
import topsun.rms.entity.UserRole;
import topsun.rms.services.IBaseService;
import topsun.rms.services.ICustomService;
import topsun.rms.services.IPromotionService;

@Controller
@RequestMapping("promotionpage/pro")
public class PromotionController extends BaseController<Promotion>
{
	@Resource(name = "promotionService")  
	public void setService(IBaseService<Promotion> Service1) 
	{  
		super.setService(Service1);
	}
	@RequestMapping(value = "/addItem", method = RequestMethod.POST)
	public void addItem( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IPromotionService customService = (IPromotionService)service;
		if((getId()!=null) && StringUtils.isNotBlank(getIds()))
		{
			customService.addItem(Integer.parseInt(getId().toString()), getIds());
		}
		
		excuteSuccess(request,response);
	}
	@RequestMapping(value = "/deleteitem", method = RequestMethod.POST)
	public void deleteRole( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		IPromotionService customService = (IPromotionService)service;
		
		if(getId() !=null && StringUtils.isNotBlank(getIds()))
		{
			customService.deleteRole(Integer.parseInt(getId().toString()), getIds());
		}
		
		excuteSuccess(request,response);
	}
}
