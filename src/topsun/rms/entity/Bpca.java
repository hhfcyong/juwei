package topsun.rms.entity;

import java.util.HashSet;
import java.util.Set;


/**
 * 区域
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Bpca implements java.io.Serializable
{
	private Set<BArea> bareas  = new HashSet<BArea>(0);
	private Set<Province> provinces = new HashSet<Province>(0);
	private Set<City> citys = new HashSet<City>(0);
	private Set<County> countys = new HashSet<County>(0);
	
	
	public Set<BArea> getBareas() {
		return bareas;
	}
	public void setBareas(Set<BArea> bareas) {
		this.bareas = bareas;
	}
	public Set<Province> getProvinces() {
		return provinces;
	}
	public void setProvinces(Set<Province> provinces) {
		this.provinces = provinces;
	}
	public Set<City> getCitys() {
		return citys;
	}
	public void setCitys(Set<City> citys) {
		this.citys = citys;
	}
	public Set<County> getCountys() {
		return countys;
	}
	public void setCountys(Set<County> countys) {
		this.countys = countys;
	}
	
}
