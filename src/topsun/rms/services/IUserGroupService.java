package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.User;
import topsun.rms.entity.UserGroup;

public interface IUserGroupService extends IBaseService<UserGroup> 
{
	/**
	 * 修改用户组角色
	 * 
	 * @param id
	 *            用户ID
	 * @param roleIds
	 *            角色IDS
	 */
	public void grantRole(String id, String roleIds);
	
	/**
	 * 删除用户组角色
	 * @param id
	 * 
	 * @param roleIds
	 */
	public void deleteRole(String id, String roleIds);
	
	
	/**
	 * 修改用户角色
	 * 
	 * @param id
	 *            角色ID
	 * @param userIds
	 *            用户IDS
	 */
	public void addUser(String id, String userIds);
	
	
	/**
	 * 删除用户角色
	 * 
	 * @param id
	 * 			角色ID
	 * @param userIds
	 * 			用户IDs
	 */
	public void deleteUser(String id, String userIds);
	
	
	/**
	 *  根据用户查询 用户所属用户组
	 * 
	 * @param hqlFilter
	 * 
	 * @return
	 */
	public List<UserGroup> findUserGroupByFilter(HqlFilter hqlFilter); 
	
	/**
	 *  根据角色查询 角色拥有的用户组
	 * 
	 * @param hqlFilter
	 * 
	 * @return
	 */
	public List<UserGroup> findUserGroupByFilterRole(HqlFilter hqlFilter); 
}
