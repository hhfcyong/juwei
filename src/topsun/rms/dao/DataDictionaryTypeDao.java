package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.DataDictionaryType;

/**
 * 字典分类 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("dataDictionaryTypeDao")
@Transactional  
public class DataDictionaryTypeDao extends BaseDao<DataDictionaryType> 
{
	
}
