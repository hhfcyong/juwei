<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	String storageDeliverId =request.getParameter("storageDeliverId");
	String typeId =request.getParameter("typeId");
	request.setAttribute("storageDeliverId", storageDeliverId);
	request.setAttribute("typeId", typeId);
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	request.setAttribute("currentUser", currentUser);
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var itemId;
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/storage/deliverDetail/update.do';
				
			} else {
				url = topsun.contextPath + '/storage/deliverDetail/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			var storageDeliverId = $("#storageDeliverId").val();
			if(typeof(storageDeliverId) != 'undefined')
				jsonString =  jsonString  +',' +'"storageDeliver":{"id":"'+ storageDeliverId+'"}';
			if(typeof(itemId)!="undefined") 
				jsonString =  jsonString  +',' +itemId;
			
			jsonString = jsonString + "}";
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	$.messager.alert("提示", "保存成功！", "info", function () {
		        		$dialog.dialog('destroy');	
		        		$grid.datagrid('reload');
		        	});
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}		
		
		$("#price").blur(function(){
			var price  = Number(parseFloat($(this).val()));
			var quantity  = Number(parseInt($("#deliverQuantity").val()));
			if(!isNaN(price) && !isNaN(quantity)){
				$("#amount").val(price*quantity);
			}
			else{
				$("#amount").val(0);
			}
			
		});
		$("#deliverQuantity").blur(function(){
			var price  = Number(parseFloat($("#price").val()));
			var quantity  = Number(parseInt($(this).val()));
			if(!isNaN(price) && !isNaN(quantity)){
				$("#amount").val(price*quantity);
			}
			else{
				$("#amount").val(0);
			}
		});
		$('#itemId').combobox({
			onSelect:function(row){
				 $.post(topsun.contextPath + '/datapage/item/getbyid.do', {
						id : row.id
					}, function(result) {
						var obj = result.obj;
						if (obj.id != undefined) {
							$('form').form('load', {
								'itemName' : obj.name,
								'unitId' : obj.unit,
								'price' : "${typeId}"=="11"?obj.costprice:obj.wholesaleprice,
							});
						}
					}, 'json');
				 itemId = '"item":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
		 });
		 $.post(topsun.contextPath + '/storage/deliverDetail/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'itemName' : obj.itemName,
						'unitId' : obj.unit,
						'price' : obj.price,
						'orderQuantity':obj.orderQuantity,
						'deliverQuantity':obj.deliverQuantity,
						'amount':obj.amount
						 
					});
					 $("#itemId").combobox('select',obj.item.id);
					 $("#storageDeliverId").val(obj.storageDeliver.id);
					 $(":input[name='id']").val(obj.id);
					 
				}	
				
			}, 'json');
		  $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div data-options="border:false"  >
	<form method="post" class="form">
		<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="10" >
				<tr>
					<input type="hidden" name="id" />
					<th>单品编码</th>
					<td><select id="itemId" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'itemcode',url:'<%=contextPath%>/datapage/item/listall.do'"  style="width: 155px;"></select></td>
					<th>单品名称</th>
					<td><input id="itemName"  class="easyui-validatebox" value="" readonly="readonly" name="itemName"/></td>
				</tr>			
				<tr>	
					<th>应发数</th>
					<td><input class="easyui-numberbox" id="orderQuantity" name="orderQuantity"></input></td>						
					<th>本次发货数</th>
					<td><input id="deliverQuantity" class="easyui-numberbox" name="deliverQuantity" required="true"></input></td>																							
				</tr>			
				<tr>
					<th>单位</th>
					<td><input  id="unitId" class="easyui-validatebox" name="unitId" value="" readonly="readonly" /></td>
					<th>发货价</th>
					<td><input id="price" class="easyui-numberbox" value="" precision="2" name="price" required="true"/></td>
				</tr>
				<tr>
					<th>发货金额</th>
					<td><input id="amount" name="amount" class="easyui-validatebox"></td>
				</tr>	
				<input value="${storageDeliverId}" type="hidden" id="storageDeliverId">		
			</table>			
		</form>
	</div>
</body>
</html>