package topsun.rms.entity;

import java.math.BigDecimal;

/**
 * 门店收货明细 实体模型
 * @author Tang
 *
 */
@SuppressWarnings("serial")
public class ShopReceiptDetail {

	private int id;
	private Item item;
	private String itemName;
	private BigDecimal deliverQuantity;//发货数量
	private BigDecimal inStoreQuantity;//入库数
	private BigDecimal freezeQuantity;//冻结数
	private BigDecimal differenceQuantity;//差异数
	private String unitId;
	private BigDecimal price;
	private BigDecimal amount;
	private ShopReceipt storeReceipt;
	private String freezeReason;
	public String getFreezeReason() {
		return freezeReason;
	}
	public void setFreezeReason(String freezeReason) {
		this.freezeReason = freezeReason;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public BigDecimal getDeliverQuantity() {
		return deliverQuantity;
	}
	public void setDeliverQuantity(BigDecimal deliverQuantity) {
		this.deliverQuantity = deliverQuantity;
	}
	public BigDecimal getInStoreQuantity() {
		return inStoreQuantity;
	}
	public void setInStoreQuantity(BigDecimal inStoreQuantity) {
		this.inStoreQuantity = inStoreQuantity;
	}
	public BigDecimal getFreezeQuantity() {
		return freezeQuantity;
	}
	public void setFreezeQuantity(BigDecimal freezeQuantity) {
		this.freezeQuantity = freezeQuantity;
	}
	public BigDecimal getDifferenceQuantity() {
		return differenceQuantity;
	}
	public void setDifferenceQuantity(BigDecimal differenceQuantity) {
		this.differenceQuantity = differenceQuantity;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public ShopReceipt getStoreReceipt() {
		return storeReceipt;
	}
	public void setStoreReceipt(ShopReceipt storeReceipt) {
		this.storeReceipt = storeReceipt;
	}
}
