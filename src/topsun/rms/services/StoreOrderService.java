package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.StoreOrder;

@Service("storeOrderService")
@Transactional
public class StoreOrderService extends BaseService<StoreOrder>{

	@Resource(name = "storeOrderDao")  
	public void setDao(IBaseDao<StoreOrder> dao) 
	{  
		super.setDao(dao);  
	}
}
