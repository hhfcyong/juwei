package topsun.rms.ctrl;

import java.util.Hashtable;

import topsun.framework.web.ISRFExWebContext;

/**
 * 远程交互上下文对象
 * @author yuxiaobei
 *
 */
public interface IRemoteAction 
{

	/**
	 * 获取操作
	 * @return
	 */
	String getAction();
	
	
	
	/**
	 * 获取请求参数
	 * @param strParamName
	 * @param strDefault
	 * @return
	 */
	String getParam(String strParamName,String strDefault);
	
	/**
	 * 获取参数集合
	 * @return
	 */
	Hashtable<String,String> getParams();
	
	/**
	 * 获取远程的地址
	 * @return
	 */
	String getRemoteAddress();
	
	
	
	/**
	 * 从上下文中构造远程行为
	 */
	void FromWebContext(ISRFExWebContext webContext) throws Exception;
	
	
	/**
	 * 构造远程行为
	 */
	void FromRemoteAction(IRemoteAction iRemoteAction) throws Exception;
	
	
	
	/**
	 * 获取内容
	 * @return
	 */
	String getContent();

}
