<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	String storeReceiptId =request.getParameter("storeReceiptId");
	request.setAttribute("storeReceiptId", storeReceiptId);
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	String typeId =request.getParameter("typeId");
	request.setAttribute("typeId", typeId);
	request.setAttribute("currentUser", currentUser);
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var itemId;
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/shopreceiptdetail/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/shopreceiptdetail/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			var storeReceiptId = $("#storeReceiptId").val();
			if(storeReceiptId != '')
				jsonString =  jsonString  +',' +'"storeReceipt":{"id":"'+ storeReceiptId+'"}';
			if(typeof(itemId)!="undefined") 
				jsonString =  jsonString  +',' +itemId;
			
			var differenceQuantity = parseFloat($("#deliverQuantity").val())-parseFloat($("#inStoreQuantity").val())
									 -parseFloat($("#freezeQuantity").val());
			jsonString = jsonString +',' +'"differenceQuantity":"'+ differenceQuantity+'"';
			jsonString = jsonString + "}";
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	$.messager.alert("提示", "保存成功！", "info", function () {
		        		$dialog.dialog('destroy');	
		        		$grid.datagrid('reload');
		        	});
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			//$('.easyui-numberbox').attr("disabled","disabled");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		$("#price").blur(function(){
			var price  = Number(parseFloat($(this).val()));
			var quantity  = Number(parseInt($("#deliverQuantity").val()));
			if(!isNaN(price) && !isNaN(quantity)){
				$("#amount").val(price*quantity);
			}
			else{
				$("#amount").val(0);
			}
			
		});
		$("#deliverQuantity").blur(function(){
			var price  = Number(parseFloat($("#price").val()));
			var quantity  = Number(parseInt($(this).val()));
			if(!isNaN(price) && !isNaN(quantity)){
				$("#amount").val(price*quantity);
			}
			else{
				$("#amount").val(0);
			}
		});
		$('#itemId').combobox({
			onSelect:function(row){
				 $.post(topsun.contextPath + '/datapage/item/getbyid.do', {
						id : row.id
					}, function(result) {
						var obj = result.obj;
						if (obj.id != undefined) {
							$('form').form('load', {
								'itemName' : obj.name,
								'unitId' : obj.unit,
								'price' : "${typeId}"=="11"?obj.costprice:obj.wholesaleprice,
							});

						}
					}, 'json');
				 itemId = '"item":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
		 });
		 $.post(topsun.contextPath + '/datapage/shopreceiptdetail/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'itemName' : obj.itemName,
						'unitName' : obj.unitName,
						'price' : obj.price,
						'inStoreQuantity':obj.inStoreQuantity,
						'freezeQuantity':obj.freezeQuantity,
						'deliverQuantity':obj.deliverQuantity,
						'freezeReason':obj.freezeReason,
						'amount':obj.amount
					});
					 $("#itemId").combobox('select',obj.item.id);
					 $("#storeReceiptId").val(obj.storeReceipt.id);
					 $(":input[name='id']").val(obj.id);
				}
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div  data-options="border:false"  >
			<form class="form" method="post">
				<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
				    <input type="hidden" name="id" />
					<th>单据编码：</th>
					<td><select id="itemId" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'itemcode',url:'<%=contextPath%>/datapage/item/listall.do'"  style="width: 155px;"></select></td>
					<th>单据名称：</th>
					<td><input name="itemName" class="easyui-validatebox"  value="" readonly="readonly" /></td>
				</tr>
				<tr>
					<th>发货数量：</th>
					<td><input id="deliverQuantity" precision="0" name="deliverQuantity" class="easyui-numberbox"  required="true" /></td>
					<th>单位：</th>
					<td><input  name="unitId" class="easyui-validatebox"  value=""  readonly="readonly" /></td>
				</tr>				
				<tr>
					<th>单价：</th>
					<td><input id="price" name="price" class="easyui-numberbox"  value="" precision="2" required="true"></td>
					<th>入库数：</th>
					<td><input id="inStoreQuantity" precision="0" name="inStoreQuantity" class="easyui-numberbox"  value="1"  required="true"/></td>
					
				</tr>
				<tr>
					<th>冻结数：</th>
					<td><input id="freezeQuantity" precision="0" name="freezeQuantity" class="easyui-numberbox"  value="0" required="true"  /></td>
					<th>冻结原因：</th>
					<td><input id="freezeReason" name="freezeReason" class="easyui-validatebox"  value=""  /></td>
				</tr>	
				<tr>				
					<th>收货金额：</th>
					<td><input class="easyui-validatebox" id="amount" name="amount"  value="" />
				</tr>
				<input value="${storeReceiptId}" type="hidden" id="storeReceiptId">
				
			</table>
			</form>
	</div>
	
</body>
</html>