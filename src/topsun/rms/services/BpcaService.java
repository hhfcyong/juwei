package topsun.rms.services;


import java.util.HashSet;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.BArea;
import topsun.rms.entity.Bpca;
import topsun.rms.entity.City;
import topsun.rms.entity.County;
import topsun.rms.entity.Province;

/**
 *  区域 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("bpcaService")
public class BpcaService extends BaseService<BArea> implements IBpcaService
{
	@Resource(name = "bAreaDao")  
	public void setDao(IBaseDao<BArea> dao) 
	{  
		super.setDao(dao);  
	}
	
	@Resource(name = "provinceDao")  
	private IBaseDao<Province> provinceDao;
	
	@Resource(name = "cityDao")  
	private IBaseDao<City> cityDao;
	
	@Resource(name = "countyDao")  
	private IBaseDao<County> countyDao;

	@Override
	public Bpca getAllBpca() 
	{
		Bpca bpca = new Bpca();
		bpca.setBareas(new HashSet<BArea>(find()));
		bpca.setProvinces(new HashSet<Province>(provinceDao.find()));
		bpca.setCitys(new HashSet<City>(cityDao.find()));
		return bpca;
	}
}
