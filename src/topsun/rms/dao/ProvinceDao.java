package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Province;

/**
 * 省 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("provinceDao")
@Transactional  
public class ProvinceDao extends BaseDao<Province> 
{
	
}
