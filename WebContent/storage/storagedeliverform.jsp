<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%@ page import="java.util.*"%> 
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	request.setAttribute("date", formatter.format(new Date()));
	request.setAttribute("currentUser", currentUser);
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var deliver;
    var shop;
    var type;
    var documentState;
    var storage;
    var audit;
    var creater;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/storage/deliver/update.do';
				
			} else {
				url = topsun.contextPath + '/storage/deliver/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			var createUserId = '"newUser":{"id":"'+ $("#newUser").val()+'"}' ;
			jsonString=jsonString+','+deliver+','+shop+','+type;	
			if(typeof(createUserId)!="undefined")
			    jsonString = jsonString +',' +createUserId;
			if(typeof(audit)!="undefined")
				jsonString=jsonString+','+audit;
			if(typeof(storage)!="undefined")
				jsonString=jsonString+','+storage;
	
			jsonString = jsonString +',' +documentState+'}';

			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
			     		 $grid.datagrid('reload');
		          },   
		          error: function(XMLHttpRequest, textStatus, errorThrown) {
                      alert(XMLHttpRequest.status);
                      alert(XMLHttpRequest.readyState);
                      alert(textStatus);
                  }   
		        });   
		}
	};

	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};		
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		$('#deliverUser').combobox({
			onSelect:function(row){
				deliver = '"deliverUser":{"id":"'+ row.id+'"}' ;
			}
		});

		$('#shop').combobox({
			onSelect:function(row){
				shop = '"shop":{"id":"'+ row.id+'"}' ;
			}
		});

		$('#type').combobox({
			onSelect:function(row){
				type = '"type":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#auditUser').combobox({
			onSelect:function(row){
				audit = '"auditUser":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#documentState').combobox({
			onSelect:function(row){
				documentState = '"documentState":{"documentStateId":"'+ row.documentStateId+'"}' ;
			}
		});
		$('#warehouse').combobox({
			onSelect:function(row){
				storage = '"warehouse":{"id":"'+ row.id+'"}' ;
			}
		});
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="storageReceiptNo"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/storage/deliver/getbyid.do', {
			 id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'storageDeliverNo' : obj.storageDeliverNo,
						'deliverDateString' : obj.deliverDate,
						'newDateString' : obj.newDate,
						'remark' : obj.remark,
						'auditDateString' : obj.auditDate
					});
					$("#documentState").combobox('select',obj.documentState.documentStateId);
					$("#deliverUser").combobox('select',obj.deliverUser.id);				
					$("#warehouse").combobox('select',obj.warehouse.id);
					$("#type").combobox('select',obj.type.id);
					$("#shop").combobox('select',obj.shop.id);
					$("#auditUser").combobox('select',obj.auditUser.id);
				}
				
			}, 'json');
		 $.messager.progress('close');
     }	
	
		//formate date
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}

</script>
</head>

<body class="easyui-layout" style="width: 100%;height:100%" >	
	<div data-options="border:false">
		<form method="post" class="form">
		<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="10" >
				<tr>
					<input type="hidden" name="id" />
					<th>单据号</th>
					<td><input name="storageDeliverNo"  class="easyui-validatebox" value="" required="true"/></td>
					<th>单据状态</th>
					<td><select id="documentState" style="width: 155px;" class="easyui-combobox" data-options="required:true,
																					editable:false,
																					valueField:'documentStateId',
																					textField:'documentStateName',
																					url:'<%=contextPath%>/document/state/listall.do',
																					panelHeight:'auto'" style="width: 155px;"></select></td>
					
				</tr>			
				<tr>
					<th>发货人</th>
					<td><select  id="deliverUser"  class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/user2/listall.do',panelHeight:'auto'" style="width: 155px;"></select>
					</td>	
					<th>发货日期</th>
					<td><input class="easyui-datetimebox" name="deliverDateString" required="true"></input></td>
																									
				</tr>	
				<tr>
					<th>发货方</th>
					<td><select id="warehouse" class="easyui-combobox" data-options="required:true,
																					editable:false,
																					valueField:'id',
																					textField:'name',
																					url:'<%=contextPath%>/datapage/warehouse/listall.do',
																					panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>发货类型</th>
					<td><select id="type" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/type/listall.do',panelHeight:'auto'" style="width: 155px;"></select>
					</td>
				</tr>		
				<tr>
					<th>新建人</th>
					<td><input name="newUser" class="easyui-validatebox" value="${currentUser.name}" disabled="disabled"/>
					<input type="hidden" value="${currentUser.id}" id="newUser"/></td>
					<th>新建日期</th>
					<td><input class="easyui-datetimebox" name="newDateString" required="true" value="${date}"></td>		
				</tr>			
				<tr>
					<th>接收门店</th>
					<td><select id="shop" class="easyui-combobox"  data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/shop/listall.do'" style="width: 155px;"></select></td>
					<th>备注</th>
					<td><input name="remark"  class="easyui-validatebox" value=""></input></td>									
				</tr>
				<tr>
					<th>审核人</th>
					<td><select id="auditUser" class="easyui-combobox"  data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/imsbase/user2/listall.do'" style="width: 155px;"></select></td>
					<th>审核日期</th>
					<td><input class="easyui-datetimebox" name="auditDateString"></td>
				</tr>
			</table>			
		</form>
	</div>
	
</body>
</html>