package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.UserRole;

/**
 * 用户角色 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("userRoleDao")
@Transactional  
public class UserRoleDao extends BaseDao<UserRole>
{
	
}
