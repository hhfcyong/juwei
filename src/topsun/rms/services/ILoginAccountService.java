package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.LoginAccount;

/**
 * 登录账号 服务
 * 
 * @author yuxiaobei
 *
 */
public interface ILoginAccountService extends IBaseService<LoginAccount>
{
	/**
	 * 通过 添加 查询用户所拥有的登录账号
	 * 
	 * @param hqlFilter
	 */
	public List<LoginAccount> findAccountByFilter(HqlFilter hqlFilter);
	
	
}
