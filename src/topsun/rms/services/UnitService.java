package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Unit;

/**
 * 单位 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("unitService")
public class UnitService extends BaseService<Unit>
{
	@Resource(name = "unitDao")  
	public void setDao(IBaseDao<Unit> dao) 
	{  
		super.setDao(dao);  
	}
}
