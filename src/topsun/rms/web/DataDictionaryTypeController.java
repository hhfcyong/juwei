package topsun.rms.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.DataDictionaryType;
import topsun.rms.model.ZTree;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IResourceService;
import topsun.rms.utility.BeanUtils;

/**
 * 
 * 字典类型 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/dictype")
public class DataDictionaryTypeController extends BaseController<DataDictionaryType>
{
	@Resource(name = "dataDictionaryTypeService")  
	public void setService(IBaseService<DataDictionaryType> Service1) 
	{  
		super.setService(Service1);
	}
	
	/**
	 * Ztree menu
	 * 
	 * @param request
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/getZTreeMainMenu", method = RequestMethod.GET)
	protected void getZTreeMainMenu(HttpServletRequest request, HttpServletResponse response)
	{
		
		
		List<ZTree> tree = new ArrayList<ZTree>();
		
		ZTree nodeHead = new ZTree();
		nodeHead.setId("head");
		nodeHead.setName("数据字典管理");
		nodeHead.setOpen(true);
		tree.add(nodeHead);
		
		List<DataDictionaryType> dictypes = service.find();
		for (DataDictionaryType dictype : dictypes) 
		{
			ZTree node = new ZTree();
			node.setId(dictype.getId());
			node.setName(dictype.getName());
			node.setPId("head");
			tree.add(node);
		}
		
		writeJson(tree, request, response);
	}
}
