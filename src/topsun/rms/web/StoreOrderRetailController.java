package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.StoreOrderRetail;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("datapage/storeorderretail")
public class StoreOrderRetailController extends BaseController<StoreOrderRetail>{
	@Resource(name = "storeOrderRetailService")  
	public void setService(IBaseService<StoreOrderRetail> service) 
	{  
		super.setService(service);
	}

}
