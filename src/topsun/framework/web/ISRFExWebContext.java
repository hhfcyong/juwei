package topsun.framework.web;

import java.util.Hashtable;

public  interface ISRFExWebContext
{

  public abstract Object GetSessionValue(String paramString);

  public abstract String getCurPagePath();

  public abstract void RemoveParam(String paramString);

  public abstract Object GetGlobalValue(String paramString);

  public abstract void SetGlobalValue(String paramString, Object paramObject);

  public abstract void SetSessionValue(String paramString, Object paramObject);

  public abstract String GetPostValue(String paramString);

  public abstract Hashtable<String, String> GetParams();

  public abstract void SetParamValue(String paramString1, String paramString2);

  public abstract String GetParamValue(String paramString);
}
