package topsun.rms.entity;


@SuppressWarnings("serial")
public class DocumentState {

	private int documentStateId;
	private String documentStateName;
	
	public int getDocumentStateId() {
		return documentStateId;
	}
	public void setDocumentStateId(int documentStateId) {
		this.documentStateId = documentStateId;
	}
	
	public String getDocumentStateName() {
		return documentStateName;
	}
	public void setDocumentStateName(String documentStateName) {
		this.documentStateName = documentStateName;
	}
}
