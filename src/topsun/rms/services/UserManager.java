package topsun.rms.services;

import java.util.List;

import topsun.rms.dao.UserDAO;
import topsun.rms.entity.User;

/**
 * 用户管理 
 * 
 * @author yuxiaobei
 *
 */
public class UserManager implements IUserManager
{
	private UserDAO userDao;
	
	public void setUserDao(UserDAO userDao)
	{
		this.userDao = userDao;
	}
    @Override
	public User getUser(String id)
	{
	    return userDao.findById(id);
	}
    @Override
	public List<User> getAllUser() 
	{
		return userDao.findAll();
	}
    @Override
	public void addUser(User user) 
	{
		userDao.save(user);
	}
    @Override
	public boolean delUser(String id) 
	{
		
		User user = userDao.findById(id);
		if (user == null) return false;
		return userDao.delete(user);
	}
    @Override
	public void updateUser(User user) 
	{
		userDao.merge(user);
	}
    
	@Override
	public int count() 
	{
		return userDao.count();
	}
	
	@Override
	public List<User> findPageByQuery(int pageNo, int pageSize)
	{
		return userDao.findPageByQuery(pageNo, pageSize);
	}
}
