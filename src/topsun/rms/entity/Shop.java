package topsun.rms.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 店铺
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Shop implements java.io.Serializable
{
	private String id;
	private String name;
	
	private String code;
	private String sname;
	private String shoptype;
	private String managemode;
	private Oranization oranization;
	private String address;
	private String phone;
	private String contacts;
	private Date beginmanagedate;
	private String bank;
	private String bankaccount;
	
	private String barea;
	private String province;
	private String city;
	private String county;
	public Set<PromotionApply> getPromotionapplys() {
		return promotionapplys;
	}
	public void setPromotionapplys(Set<PromotionApply> promotionapplys) {
		this.promotionapplys = promotionapplys;
	}
	private Set<PromotionApply> promotionapplys=new HashSet<PromotionApply>(0);
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBarea() {
		return barea;
	}
	public void setBarea(String barea) {
		this.barea = barea;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getShoptype() {
		return shoptype;
	}
	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}
	public String getManagemode() {
		return managemode;
	}
	public void setManagemode(String managemode) {
		this.managemode = managemode;
	}
	public Oranization getOranization() {
		return oranization;
	}
	public void setOranization(Oranization oranization) {
		this.oranization = oranization;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContacts() {
		return contacts;
	}
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	public Date getBeginmanagedate() {
		return beginmanagedate;
	}
	public void setBeginmanagedate(Date beginmanagedate) {
		this.beginmanagedate = beginmanagedate;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBankaccount() {
		return bankaccount;
	}
	public void setBankaccount(String bankaccount) {
		this.bankaccount = bankaccount;
	}
	
}
