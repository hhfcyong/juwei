package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.ShopReceiptDetail;

/**
 * 门店收货明细服务 实现
 * 
 * @author Tang
 *
 */
@Service("shopReceiptDetailService")
public class ShopReceiptDetailService extends BaseService<ShopReceiptDetail>{
	@Resource(name = "shopReceiptDetailDao")  
	public void setDao(IBaseDao<ShopReceiptDetail> dao) 
	{  
		super.setDao(dao);  
	}
}
