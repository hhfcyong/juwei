package topsun.rms.entity;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;


/**
 * 用户 实体模型
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class User implements java.io.Serializable
{
	private String id;
	private String name;
	private Set<UserRole> userroles = new HashSet<UserRole>(0);
	private Set<LoginAccount> loginaccounts = new HashSet<LoginAccount>(0);
	private Set<UserGroup> usergroups = new HashSet<UserGroup>(0);
	
	private String ip;
	private String loginAccountName;
	
	public String getLoginAccountName() {
		return loginAccountName;
	}
	public void setLoginAccountName(String loginAccountName) {
		this.loginAccountName = loginAccountName;
	}
	public User()
	{
		
	}
	public User(String id)
	{
		this.id = id;
	}
	
	public User(String id,String name)
	{
		this.id = id;
		this.name = name;
	}
	public String getId() {
//		return id;
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Set<UserRole> getUserroles() {
		return userroles;
	}
	public void setUserroles(Set<UserRole> userroles) {
		this.userroles = userroles;
	}
	public Set<LoginAccount> getLoginaccounts() {
		return loginaccounts;
	}
	public void setLoginaccounts(Set<LoginAccount> loginaccounts) {
		this.loginaccounts = loginaccounts;
	}
	public Set<UserGroup> getUsergroups() {
		return usergroups;
	}
	public void setUsergroups(Set<UserGroup> usergroups) {
		this.usergroups = usergroups;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
}
