package topsun.rms.dao;

import org.hibernate.Session;


/**
 * Data access object (DAO) for domain model
 * @author MyEclipse Persistence Tools
 */
public class BaseHibernateDAO implements IBaseHibernateDAO {
	
	public Session getCurrentSession() {
		return HibernateSessionFactory.getSession();
	}
	
}