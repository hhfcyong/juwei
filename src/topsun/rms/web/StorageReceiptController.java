package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.StorageReceipt;
import topsun.rms.entity.UserRole;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("storage/receipt")
public class StorageReceiptController extends BaseController<StorageReceipt> 
{
	@Resource(name = "storageReceiptService")  
	public void setService(IBaseService<StorageReceipt> Service1) 
	{  
		super.setService(Service1);		
	}
	
	
}
