package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.StorageReceipt;;

/**
 * 仓库收货  Dao
 * 
 * @author Tang
 *
 */

@Repository("storageReceiptDao")  
@Transactional
public class StorageReceiptDao extends BaseDao<StorageReceipt> 
{
	
}
