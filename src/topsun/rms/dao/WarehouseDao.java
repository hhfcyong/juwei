package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Warehouse;

/**
 * 仓库 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("warehouseDao")
@Transactional  
public class WarehouseDao extends BaseDao<Warehouse> 
{
	
}
