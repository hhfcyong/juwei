package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Unit;

/**
 * 单位 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("unitDao")
@Transactional  
public class UnitDao extends BaseDao<Unit> 
{
	
}
