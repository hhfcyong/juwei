<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();%>
<%String contextPath = request.getContextPath();%>
<%-- 解决js缓存问题 --%>
<%String version = "20140807"; %>

<%
Map<String, Cookie> cookieMap = new HashMap<String, Cookie>();
Cookie[] cookies = request.getCookies();
if (null != cookies) {
	for (Cookie cookie : cookies) {
		cookieMap.put(cookie.getName(), cookie);
	}
}
String easyuiTheme = "default";//指定如果用户未选择样式，那么初始化一个默认样式
if (cookieMap.containsKey("easyuiTheme")) {
	Cookie cookie = (Cookie) cookieMap.get("easyuiTheme");
	easyuiTheme = cookie.getValue();
}
%>

<script type="text/javascript">
var topsun = topsun || {};
topsun.contextPath = '<%=contextPath%>';
topsun.basePath = '<%=basePath%>';
topsun.version = '<%=version%>';
topsun.pixel_0 = '<%=contextPath%>/style/images/pixel_0.gif';//0像素的背景，一般用于占位333
</script>
<script type="text/javascript" src="<%=contextPath%>/js/jquery-1.11.1.min.js"></script> 
<%-- 引入EasyUI --%>
<link id="easyuiTheme" rel="stylesheet" href="<%=contextPath%>/js/jquery-easyui-1.3.6/themes/<%=easyuiTheme%>/easyui.css" type="text/css">
<link rel="stylesheet" href="<%=contextPath%>/js/jquery-easyui-1.3.6/themes/icon.css" type="text/css"> 
<script type="text/javascript" src="<%=contextPath%>/js/jquery-easyui-1.3.6/jquery.easyui.min.js" charset="utf-8"></script>
<script type="text/javascript" src="<%=contextPath%>/js/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>

<script type="text/javascript" src="<%=contextPath%>/js/jquery.json-2.4.min.js"></script>   
<%-- 引入扩展图标 --%>
<link rel="stylesheet" href="<%=contextPath%>/style/syExtIcon.css" type="text/css">
<%-- 引入自定义样式 --%>
<link rel="stylesheet" href="<%=contextPath%>/style/syExtCss.css" type="text/css">

<link rel="stylesheet" href="<%=contextPath%>/style/yxbExtcon.css?version=<%=version%>" type="text/css">


<script type="text/javascript" src="<%=contextPath%>/js/topsun.js?version=<%=version%>"></script>  

