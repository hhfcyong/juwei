package topsun.rms.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单模型
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Menu  implements java.io.Serializable 
{
	private String id;
	private String name;
	private String icon;
	private String url;
	private List<Menu> menus = new ArrayList<Menu>();
	private String pid;
	private Long sortno;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<Menu> getMenus() {
		return menus;
	}
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public Long getSortno() {
		return sortno;
	}
	public void setSortno(Long sortno) {
		this.sortno = sortno;
	}
	
	
}
