package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.DocumentState;
import topsun.rms.entity.Employee;
import topsun.rms.entity.StorageReceipt;

/**
 * 状态服务类
 * 
 * @author Tang
 *
 */
@Service("documentStateService")
public class DocumentStateService extends BaseService<DocumentState>{
	
	@Resource(name = "documentStateDao")  
	public void setDao(IBaseDao<DocumentState> dao) 
	{  
		super.setDao(dao);  
	}
}
