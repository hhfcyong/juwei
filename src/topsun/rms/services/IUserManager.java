package topsun.rms.services;

import java.util.List;
import topsun.rms.entity.User;

/**
 * 用户管理
 * 
 * @author yuxiaobei
 *
 */
public interface IUserManager 
{
	/**
	 * 根据id获取User
	 * @param id
	 * @return
	 */
    public User getUser(String id);
	
    /**
     * 获取所有user
     * @return
     */
	public List<User> getAllUser();
	
	/**
	 * 新增User
	 * @param user
	 */
	public void addUser(User user);
	
	/**
	 * 根据用户id删除User
	 * @param id
	 */
	public boolean delUser(String id);
	
	/**
	 * 更新用户
	 * @param user
	 */
	public void updateUser(User user);
	
	/**
	 * 用户数
	 * @return
	 */
	public int count();
	
	/**
	 * 分页查询
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public  List<User> findPageByQuery(int pageNo, int pageSize);
}
