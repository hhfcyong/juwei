package topsun.rms.entity;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;


/**
 * 用户角色 模型
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class UserRole implements java.io.Serializable
{
	private String id;
	private String name;
	private String description;
	private String iconCls;
	private Long sortno;
	private Set<User> users = new HashSet<User>(0);
	private Set<Resource> resources = new HashSet<Resource>(0);
	private Set<UserGroup> usergroups = new HashSet<UserGroup>(0);
	
	
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public Long getSortno() {
		return sortno;
	}
	public void setSortno(Long sortno) {
		this.sortno = sortno;
	}
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	public Set<UserGroup> getUsergroups() {
		return usergroups;
	}
	public void setUsergroups(Set<UserGroup> usergroups) {
		this.usergroups = usergroups;
	}
	
	
}
