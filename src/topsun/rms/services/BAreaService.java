package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.BArea;

/**
 * 大区 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("bAreaService")
public class BAreaService extends BaseService<BArea>
{
	@Resource(name = "bAreaDao")  
	public void setDao(IBaseDao<BArea> dao) 
	{  
		super.setDao(dao);  
	}
}
