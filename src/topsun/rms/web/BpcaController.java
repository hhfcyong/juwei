package topsun.rms.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.rms.entity.BArea;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IBpcaService;

/**
 * 
 * 区域 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/bpca")
public class BpcaController extends BaseController<BArea>
{
	@Resource(name = "bpcaService")  
	public void setService(IBaseService<BArea> Service1) 
	{  
		super.setService(Service1);
	}
	
	@RequestMapping(value = "/getAllBpca", method = RequestMethod.GET)
	public void getAllBpca( HttpServletRequest request, HttpServletResponse response)
	{
		IBpcaService bpcaService = (IBpcaService)service;
		 
		writeJson(bpcaService.getAllBpca(),request,response);
	}
}
