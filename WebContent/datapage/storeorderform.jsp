<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	request.setAttribute("currentUser", currentUser);
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var typeId;
    var documentStateId;
    var orderUserId;
    var storeId;
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/storeorder/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/storeorder/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			var createUserId = '"createUser":{"id":"'+ $("#createUserId").val()+'"}' ;
			var stateId = $("#documentStateId").val();
			if(stateId!="") {
				documentStateId = '"documentState":{"documentStateId":"'+ stateId+'"}' ;
				jsonString =  jsonString  +',' +documentStateId;
			}
			var typeId = $('#typeId').combobox('getValue');	
			if(typeId!=""){
				typeId = '"type":{"id":"'+ typeId+'"}'
				jsonString = jsonString +',' +typeId;
			}
			var orderUserId = $('#orderUserId').combobox('getValue');	    
			if(orderUserId!=""){
				orderUserId = '"orderUser":{"id":"'+ orderUserId+'"}' ;
				jsonString = jsonString +',' +orderUserId;
			}
			    
			if(typeof(createUserId)!="undefined")
			    jsonString = jsonString +',' +createUserId;
			var storeId = $('#storeId').combobox('getValue');	    
			if(storeId!=""){
				 storeId = '"shop":{"id":"'+ storeId+'"}' ;
				 jsonString = jsonString +',' +storeId;
			}
			   
			
			jsonString = jsonString + "}";
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	$.messager.alert("提示", "保存成功！", "info", function () {
		        		$dialog.dialog('destroy');	
		        		$grid.datagrid('reload');
		        	});
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }  
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$('.easyui-numberbox').attr("disabled","disabled");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
		 });
		 $.post(topsun.contextPath + '/datapage/storeorder/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'storeId' : obj.storeId,
						'orderDate2' : obj.orderDate,
						'remark' : obj.remark,
						'newDate2':obj.newDate,
						'storeOrderNo' : obj.storeOrderNo
					});
					 $("#documentStateId").val(obj.documentState.documentStateId);
					 $("#typeId ").combobox('select',obj.type.id);
					 $("#orderUserId ").combobox('select',obj.orderUser.id)
					 $("#storeId ").combobox('select',obj.shop.id);
					 $("#createUserName").val(obj.createUser.name);
					 $("#createUserId").val(obj.createUser.id);
				     $.messager.progress('close');
				}
				if(model =="copy"){
					$('.easyui-numberbox').val('');
					entityid = "";
				}
				
			}, 'json');
		
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div  data-options="border:false"  >
			<form class="form" method="post">
				<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
				    <input type="hidden" name="id" />
					<th>单据号：</th>
					<td><input name="storeOrderNo" class="easyui-validatebox"  value="" /></td>
					<input id="documentStateId" value="1" type="hidden"> 
					<th>订单类型：</th>
					<td><select id="typeId" class="easyui-combobox"  data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/type/listall.do'" style="width: 155px;"></select></td>
					
				</tr>
				<tr>
					<th>订货日期：</th>
					<td><input name="orderDate2" class="easyui-datetimebox"  value="" required="true"/></td>
					<th>订货门店：</th>
					<td><select id="storeId" class="easyui-combobox"  data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/shop/listall.do'" style="width: 155px;"></select></td>
				</tr>
				<tr>
					<th>新建日期：</th>
					<td><input name="newDate2" class="easyui-datetimebox"  value="" required="true"/></td>
					<th>订货人：</th>
					<td><select id="orderUserId" class="easyui-combobox"  data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>//imsbase/user2/listall.do'" style="width: 155px;"></select>
				</tr>
				<tr>
				<th>新建人：</th>
					<td><input class="easyui-validatebox" id="createUserName"  value="${currentUser.name}" disabled="disabled"/>
						<input type="hidden" value="${currentUser.id}" id="createUserId"/>
					</td>
				</tr>
				<tr>
				  <th>备注:</th> 
				  <td colspan="3"><textarea name="remark" style="overflow:auto;width:100%;height:100%;"></textarea>  </td>
				</tr>
				
			</table>
			</form>
	</div>
	
</body>
</html>