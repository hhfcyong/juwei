<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.model.SessionInfo"  %>
<%@ page import="topsun.rms.entity.*"  %>
<%@ page import="java.util.*"%> 
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	SessionInfo sessionInfo =(SessionInfo)request.getSession().getAttribute("sessionInfo");
	User currentUser = sessionInfo.getUser();
	String userId= currentUser.getId();
	request.setAttribute("currentUser", currentUser);
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	request.setAttribute("date", formatter.format(new Date()));
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var documentStateId;
    var formulateUser;
    var areaId;
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/itemscheme/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/itemscheme/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);

			if(typeof(documentStateId)!="undefined") 
				jsonString =  jsonString  +',' +documentStateId;
			if(typeof(formulateUser)=="undefined"){
				var user = '<%=userId%>';
				formulateUser = '"formulateUser":{"id":"'+ user +'"}' ;			    
			}
			jsonString = jsonString +',' +formulateUser;
			if(typeof(areaId)!="undefined")
			    jsonString = jsonString +',' +areaId;
			
			jsonString = jsonString + "}";
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	$.messager.alert("提示", "保存成功！", "info", function () {
		        		$dialog.dialog('destroy');	
		        		$grid.datagrid('reload');
		        	});
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$('#itemSchemeNo').attr("readonly","readonly");
			$('#itemSchemeName').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		$('#documentState').combobox({
			onSelect:function(row){
				documentStateId = '"documentState":{"documentStateId":"'+ row.documentStateId+'"}' ;
			}
		});
		$('#formulateUser').combobox({
			onSelect:function(row){
				formulateUser = '"formulateUser":{"id":"'+ row.id+'"}' ;
			}
		});
		$('#areaId').combobox({
			onSelect:function(row){
				areaId = '"areaId":{"id":"'+ row.id+'"}' ;
			}
		});
		
		$('#selbarea').combobox({
			onSelect:function(row){
				 $('#selprovince').combobox('clear');
				 $('#selcity').combobox('clear');
				 $('#areaId').combobox('clear');
				 $('#selcity').combobox('loadData', '');
				 $('#areaId').combobox('loadData', '');
				 
				 var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+row.code;
				 $('#selprovince').combobox('reload', url);
				
			}
		});
		$('#selprovince').combobox({
			onSelect:function(row){
				 
				 $('#selcity').combobox('clear');
				 $('#areaId').combobox('clear');
				 $('#areaId').combobox('loadData', '');
				 
				 var url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+row.code;
				 $('#selcity').combobox('reload', url);
			}
		});
		
		$('#selcity').combobox({
			onSelect:function(row){
				 $('#areaId').combobox('clear');
				 
				 var url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+row.code;
				 $('#areaId').combobox('reload', url);
			}
		});		
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
		 });
		 $.post(topsun.contextPath + '/datapage/itemscheme/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'itemSchemeNo' : obj.itemSchemeNo,
						'itemSchemeName' : obj.itemSchemeName,
						'remark' : obj.remark,
						'formulateDateString':obj.formulateDate,
						'startDateString' : obj.startDate,
						'endDateString' : obj.endDate,
						'executionDateString' : obj.executionDate,
						'automatic' : obj.automatic,
						'frequency' : obj.frequency,
					});
					 $("#documentState").combobox('select',obj.documentState.documentStateId);		
					 $("#formulateUser").combobox('select',obj.formulateUser.id);	
					 //alert(obj.automatic);
					 $("#areaId").combobox('select',obj.areaId.id);
				}		
				
			}, 'json');
			$.messager.progress('close');
     }
	 	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div  data-options="border:false"  >
			<form class="form" method="post">
				<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
				    <input type="hidden" name="id" />
					<th>方案号：</th>
					<td><input id="itemSchemeNo" name="itemSchemeNo" class="easyui-validatebox"  value="" required="true"/></td>					
					<th>方案名：</th>
					<td><input id="itemSchemeName" name="itemSchemeName" class="easyui-validatebox"  value="" required="true"/></td>
				</tr>
				<tr>				
					<th>单据状态：</th>
					<td><select id="documentState" class="easyui-combobox" data-options="required:true,editable:false,valueField:'documentStateId',
																					textField:'documentStateName',
																					url:'<%=contextPath%>/document/state/listall.do'"  style="width: 155px;"></select></td>
					<th>制定人：</th>
					<td><select id="formulateUser" class="easyui-combobox"  data-options="required:true,editable:false,valueField:'id',textField:'name',
											url:'<%=contextPath%>//imsbase/user2/listall.do'" style="width: 155px;">
							<option value="${currentUser.id}">${currentUser.name}</option>	
						</select>
				</tr>
				<tr>
					<th>制定日期：</th>
					<td><input name="formulateDateString" class="easyui-datetimebox"  value="${date}" required="true"/></td>
					<th>起始日期：</th>
					<td><input id="startDateString" class="easyui-datetimebox"  value="" required="true" name="startDateString"/>
				</tr>
				<tr>
					<th>结束日期：</th>
					<td><input id="endDateString" class="easyui-datetimebox"  value="" required="true" name="endDateString"/>
					<th>自动执行：</th>
					<td><select id="automatic" class="easyui-combobox" name="automatic" style="width: 155px;" required="true">
							<option value=""></option>
							<option value="1">否</option>
							<option value="2">是</option>
						</select>
				</tr>
				<tr>
					<th>执行频率：</th>
					<td><input class="easyui-numberbox" id="frequency"  value="" name="frequency"/>						
					</td>					
					<th>
						执行日期：
					</th>
					<td><input id="executionDateString" class="easyui-datetimebox"  value="" required="true" name="executionDateString"/>					
				</tr>
				<tr>
					<th>大区：</th>					
					<td><select id="selbarea" class="easyui-combobox"
						value="" class="easyui-combobox" data-options="editable:false,valueField:'code',
																					textField:'name',
																					url:'<%=contextPath%>/datapage/barea/listall.do'"  style="width: 155px;"></select></td>
					<th>省份：</th>
					<td>
					<select id="selprovince" class="easyui-combobox"
						value="" class="easyui-combobox"  data-options="editable:false,valueField:'code',textField:'name'"  style="width: 155px;"></select>
					</td>				
				</tr>
				<tr>
					<th>地市：</th>
					<td>
					<select id="selcity" data-options="editable:false,valueField:'code',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 155px;"></select>
					</td>
					<th>使用城区：</th>
					<td>
						<select id="areaId" class="easyui-combobox"  data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/county/listall.do'" style="width: 155px;">
						</select>
					</td>
				</tr>
				<tr>
				  <th>备注:</th> 
				  <td colspan="3"><textarea name="remark" style="overflow:auto;width:100%;height:100%;"></textarea></td>
				</tr>
				
			</table>
			</form>
	</div>
	
</body>
</html>