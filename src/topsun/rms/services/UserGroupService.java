package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.User;
import topsun.rms.entity.UserGroup;
import topsun.rms.entity.UserRole;

/**
 * 用户组 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("userGroupService")
public class UserGroupService extends BaseService<UserGroup> implements IUserGroupService
{
	@Resource(name = "userGroupDao")  
	public void setDao(IBaseDao<UserGroup> dao) 
	{  
		super.setDao(dao);  
	}
	
	@Resource(name = "userRoleDao")  
	private IBaseDao<UserRole> roleDao;
	
	@Resource(name = "customDao")  
	private IBaseDao<User> customDao;

	@Override
	public void grantRole(String id, String roleIds)
	{
		UserGroup userGroup = getById(id);
		if (userGroup != null) 
		{
			for (String roleId : roleIds.split(",")) 
			{
				if (StringUtils.isBlank(roleId))  continue;
				
				UserRole role = roleDao.getById(roleId);
				if (role != null)
				{
					userGroup.getUserroles().add(role);
				}
			}
			super.save(userGroup);
		}
	}

	@Override
	public void deleteRole(String id, String roleIds) 
	{
		UserGroup userGroup = getById(id);
		if(userGroup != null)
		{
			for(String roleId :roleIds.split(",") )
			{
				if (StringUtils.isBlank(roleId))  continue;
				
				UserRole role = roleDao.getById(roleId);
				if (role != null)
				{
					userGroup.getUserroles().remove(role);
				}
			}
			super.save(userGroup);
		}
		
	}

	@Override
	public void addUser(String id, String userIds) 
	{
		UserGroup userGroup = getById(id);
		if (userGroup != null) 
		{
			for (String userId : userIds.split(",")) 
			{
				if (StringUtils.isBlank(userId))  continue;
				
				User user = customDao.getById(userId);
				if (user != null)
				{
					userGroup.getUsers().add(user);
				}
			}
			super.save(userGroup);
		}
	}

	@Override
	public void deleteUser(String id, String userIds) 
	{
		UserGroup userGroup = getById(id);
		if (userGroup != null) 
		{
			for (String userId : userIds.split(",")) 
			{
				if (StringUtils.isBlank(userId))  continue;
				
				User user = customDao.getById(userId);
				if (user != null)
				{
					userGroup.getUsers().remove(user);
				}
			}
			super.save(userGroup);
		}		
	}
	
	/**
	 * 解决更新删除关联问题
	 */
	@Override
	public void update(UserGroup useGroup)
	{
		UserGroup tuserGroup = getById(useGroup.getId());
		Hibernate.initialize(tuserGroup.getUserroles());
		Hibernate.initialize(tuserGroup.getUsers());
		useGroup.setUserroles(tuserGroup.getUserroles());
		useGroup.setUsers(tuserGroup.getUsers());
		
		super.merge(useGroup);
	}

	@Override
	public List<UserGroup> findUserGroupByFilter(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from UserGroup t join t.users user";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}

	@Override
	public List<UserGroup> findUserGroupByFilterRole(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from UserGroup t join t.userroles userrole";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}

}
