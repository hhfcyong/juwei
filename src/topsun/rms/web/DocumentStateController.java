package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.DocumentState;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("document/state")
public class DocumentStateController extends BaseController<DocumentState> {
	
	@Resource(name = "documentStateService")  
	public void setService(IBaseService<DocumentState> Service1) 
	{  
		super.setService(Service1);		
	}
}
