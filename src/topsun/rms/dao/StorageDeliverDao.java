package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.StorageDeliver;

/**
 * 仓库发货  Dao
 * 
 * @author Tang
 *
 */

@Repository("storageDeliverDao")  
@Transactional
public class StorageDeliverDao extends BaseDao<StorageDeliver>  {

}
