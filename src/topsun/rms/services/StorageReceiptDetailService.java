package topsun.rms.services;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.StorageReceiptDetail;
import topsun.rms.entity.User;

/**
 * 自定义仓库收货服务类
 * 
 * @author Tang
 *
 */
@Service("storageReceiptDetailService")
public class StorageReceiptDetailService extends BaseService<StorageReceiptDetail> implements IStorageReceiptDetailService
{
	@Resource(name = "storageReceiptDetailDao")  
	public void setDao(IBaseDao<StorageReceiptDetail> dao) 
	{  
		super.setDao(dao);  
	}

	@Override
	public List<StorageReceiptDetail> findStotageReceiptDetailByFilter(String id, String parentId) {
		return null;
	}
}
