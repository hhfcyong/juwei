package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.LoginLog;

/**
 * 登录日志服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("loginLogService")
public class LoginLogService extends BaseService<LoginLog>
{
	@Resource(name = "loginLogDao")  
	public void setDao(IBaseDao<LoginLog> dao) 
	{  
		super.setDao(dao);  
	}

}
