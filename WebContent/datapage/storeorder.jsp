<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
	String contextPath = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ contextPath + "/";
	SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>要货单管理</title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
	function formatDocumentState(val, row) {
		return val.documentStateName;
	}
	function formatObject(val, row) {
		return val.name;
	}
	var Common = {

		//EasyUI用DataGrid用日期格式化
		DateFormatter : function(val, row) {
			if (val == undefined) {
				return "";
			}

			val = val.substr(0, val.length - 9);
			return val;
		}
	};

	function reload() {
		$('#dg').datagrid('load', topsun.serializeObject($('#mysearch')));
	}

	function onDblClickRow(rowIndex, rowData) {
		topsun.editFun($('#dg'), '/datapage/storeorderform.jsp');
	}
	//排序时触发
	function onSortColumn(sort, order) {
		sortName = sort;
		sortOrder = order;
	}

	topsun.viewDetailFun = function($grid, uri) {
		var arr =$grid.datagrid('getSelections');
		if(arr.length != 1){
				$.messager.show({
					title:'提示信息!',
					msg:'只能选择一行记录!'
				});
		}else if(arr.length == 1){
			
		  var row = $grid.datagrid('getSelected');
		  if (row){
			  var dialog = parent.topsun.modalDialog({
					title : '要货单明细',
					iconCls : 'icon-edit',
					url : topsun.contextPath + uri+'?id='+row.id,
					buttons : [ 
					{
						text : '关闭',
						iconCls : 'icon-cancel',
						handler : function() {
							dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $grid);
						}
					} ]
				});
		   }
		}
		
	};
	$(function(){
		$('#selbarea').combobox({
			onSelect:function(row){
				
				 $('#selprovince').combobox('clear');
				 $('#selcity').combobox('clear');
				 $('#selcounty').combobox('clear');
				 $('#selshop').combobox('clear');
				 $('#selcity').combobox('loadData', '');
				 $('#selcounty').combobox('loadData', '');
				 
				 var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+row.code;
				 $('#selprovince').combobox('reload', url);
				
			}
		});
		$('#selprovince').combobox({
			onSelect:function(row){
				 
				 $('#selcity').combobox('clear');
				 $('#selcounty').combobox('clear');
				 $('#selshop').combobox('clear');
				 $('#selcounty').combobox('loadData', '');
				 
				 var url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+row.code;
				 $('#selcity').combobox('reload', url);
			}
		});
		
		$('#selcity').combobox({
			onSelect:function(row){
				 $('#selcounty').combobox('clear');
				 $('#selshop').combobox('clear');
				 
				 var url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+row.code;
				 $('#selcounty').combobox('reload', url);
			}
		});
		$('#selcounty').combobox({
			onSelect:function(row){
				 $('#selshop').combobox('clear');
				 var url = topsun.contextPath + '/datapage/shop/listbypcode.do?pcode='+row.code;
				 $('#selshop').combobox('reload', url);
			}
		});
	})

</script>
</head>

<body class="easyui-layout" style="width: 100%; height: 100%">

	<div region="north" style="padding: 5px 5px 0px 5px;" border="false">
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
				<div style="margin-top: 1%;">
					<lable style="width: 4%;">单据号:</lable>
					<input name="QUERY_t#storeOrderNo_S_LK" class="easyui-validatebox" value="" style="width: 100px;"/>
					单据状态:<select style="width: 100px;" name="QUERY_t#documentState.documentStateId_I_EQ" class="easyui-combobox"
						value="" class="easyui-combobox" data-options="editable:false,valueField:'documentStateId',
																					textField:'documentStateName',
																					url:'<%=contextPath%>/document/state/listall.do'"  style="width: 100px;"></select> 新建日:<input name="QUERY_t#newDate_D_GE"
						class="easyui-datebox" value="" style="width: 100px;" />
					&nbsp;至&nbsp;&nbsp;<input name="QUERY_t#newDate_D_LE" class="easyui-datebox"
						value="" style="width: 100px;" /> 审核日:<input
						name="QUERY_t#auditDate_D_GE" class="easyui-datebox" value=""
						style="width: 100px;" /> &nbsp;至&nbsp;<input
						name="QUERY_t#auditDate_D_LE" class="easyui-datebox" value=""
						style="width: 100px;" />
				</div>
				<div style="margin-top: 1%; margin-left: 1%;">
					<span margin-left: 1%;">&nbsp;大区:<select id="selbarea" class="easyui-combobox"
						value="" class="easyui-combobox" data-options="editable:false,valueField:'code',
																					textField:'name',
																					url:'<%=contextPath%>/datapage/barea/listall.do'"  style="width: 100px;"></select>
					</span> <span style="margin-left: 3.3%;"> <label
						style="width: 100px;">省:</label><select id="selprovince" class="easyui-combobox"
						value="" class="easyui-combobox"  data-options="editable:false,valueField:'code',textField:'name'"  style="width: 100px;"></select>
					</span> <span style="margin-left: 2%;"> 市:<select id="selcity" data-options="editable:false,valueField:'code',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 100px;"></select>
					</span> <span> 城区:<select id="selcounty" data-options="editable:false,valueField:'code',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"   style="width: 100px;"></select>
					</span> <span>&nbsp; 门店:<select id="selshop" name="QUERY_t#shop.id_S_EQ" data-options="editable:false,valueField:'id',textField:'name'" class="easyui-combobox"
						value="" class="easyui-combobox"  style="width: 100px;"></select>
					</span>
					<span>
					&nbsp; &nbsp; <a id="searchbtn" class="easyui-linkbutton"
						data-options="iconCls:'icon-search'" onclick="reload()">查询</a> <a
						id="clearbtn" class="easyui-linkbutton"
						data-options="iconCls:'yxib-icon-clear'"
						onclick="javascript:$('#mysearch')[0].reset();reload();">清空</a></span>
				</div>
			</form>
		</fieldset>
	</div>
	<div region="center" onselectstart="return false;"
		style="-moz-user-select: none; padding: 0px 5px 5px 5px;"
		border="false">
		<table id="dg" title="要货单管理" class="easyui-datagrid"
			url="storeorder/list.do" iconCls="yxb-icon datagrid-head"
			SelectOnCheck="false" CheckOnSelect="false" singleSelect="true"
			pageSize="20" pageNumber="1" fit="true" striped="true" sortName="id"
			sortOrder="asc" toolbar="#toolbar" pagination="true"
			data-options="onDblClickRow:onDblClickRow" rownumbers="true"
			fitColumns="true" loadMsg="数据正在加载,请耐心的等待...">
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
					<th field="id" hidden="true"></th>
					<th field="storeOrderNo" width="150">单据号</th>
					<th field="documentState" width="100"
						formatter="formatDocumentState">单据状态</th>
					<th field="type" width="100" formatter="formatObject">订单类型</th>
					<th field="shop" width="150" formatter="formatObject">订货门店</th>
					<th field="orderUser" width="100" formatter="formatObject">订货人</th>
					<th field="orderDate" width="170" >订货日期</th>
					<th field="createUser" width="100" formatter="formatObject">新建人</th>
					<th field="newDate" width="170" >新建日期</th>
					<th field="auditDate" width="170" >审核日期</th>
				</tr>
			</thead>
		</table>
	</div>

	<div id="toolbar">
		<table>
			<tr>
				<%
					if (securityUtil.havePermission("typemanager.add")) {
				%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add"
					plain="true"
					onclick="topsun.addFun($('#dg'),'/datapage/storeorderform.jsp')">新增</a></td>
				<%
					}
				%>

				<%
					if (securityUtil.havePermission("typemanager.edit")) {
				%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit"
					plain="true"
					onclick="topsun.editFun($('#dg'),'/datapage/storeorderform.jsp')">编辑</a></td>
				<%
					}
				%>

				<%
					if (securityUtil.havePermission("typemanager.delete")) {
				%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove"
					plain="true"
					onclick="topsun.removeNFun($('#dg'),'/datapage/type/remove.do')">删除</a></td>
				<%
					}
				%>
				<%
					if (securityUtil.havePermission("typemanager.view")) {
				%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton"
					iconCls="yxb-icon view" plain="true"
					onclick="topsun.viewFun($('#dg'),'/datapage/storeorderform.jsp')">查看</a></td>
				<%
					}
				%>
				<%
					if (securityUtil.havePermission("typemanager.add")) {
				%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton"
					iconCls="yxb-icon copy" plain="true"
					onclick="topsun.copyFun($('#dg'),'/datapage/storeorderform.jsp')">复制</a></td>
				<%
					}
				%>
				<%
					if (securityUtil.havePermission("typemanager.view")) {
				%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton"
					iconCls="yxb-icon view" plain="true"
					onclick="topsun.viewDetailFun($('#dg'),'/datapage/storeorderretail.jsp')">查看明细</a></td>
				<%
					}
				%>
				<%-- 	<%if (securityUtil.havePermission("typemanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/datapage/type/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%> --%>
			</tr>
		</table>

	</div>

</body>
</html>
