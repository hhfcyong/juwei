package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.ResourceType;
import topsun.rms.services.IBaseService;

/**
 * 
 * 资源类型
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/restype")
public class ResourceTypeController extends BaseController<ResourceType>
{
	@Resource(name = "resourceTypeService")  
	public void setService(IBaseService<ResourceType> Service1) 
	{  
		super.setService(Service1);
	}
	
}
