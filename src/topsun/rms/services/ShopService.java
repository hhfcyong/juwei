package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Item;
import topsun.rms.entity.Shop;

/**
 * 店铺 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("shopService")
public class ShopService extends BaseService<Shop>  implements IShopService
{
	@Resource(name = "shopDao")  
	public void setDao(IBaseDao<Shop> dao) 
	{  
		super.setDao(dao);  
	}
	@Override
	public List<Shop> findShopByFilter(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from Shop t join t.promotionapplys promotionapply";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}
	
}
