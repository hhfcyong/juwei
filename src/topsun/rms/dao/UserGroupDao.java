package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.UserGroup;

/**
 * 用户组  Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("userGroupDao")
@Transactional  
public class UserGroupDao extends BaseDao<UserGroup> 
{
	
}
