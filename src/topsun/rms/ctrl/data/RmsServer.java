package topsun.rms.ctrl.data;

public class RmsServer 
{
	/**
	 *定义服务器标识字段
	 */
	private String ServerId ;
	
	/**
	 *定义服务器名称字段
	 */
	private String ServerName;

	public String getServerId() {
		return ServerId;
	}

	public void setServerId(String serverId) {
		ServerId = serverId;
	}

	public String getServerName() {
		return ServerName;
	}

	public void setServerName(String serverName) {
		ServerName = serverName;
	}

}
