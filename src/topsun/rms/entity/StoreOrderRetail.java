package topsun.rms.entity;

import java.math.BigDecimal;

public class StoreOrderRetail implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Item item;
	private String itemName;
	private BigDecimal orderQuantity;
	private BigDecimal deliverQuantity;
	private BigDecimal price;
	private BigDecimal amount;
	private String unitName;
	private StoreOrder storeOrder;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public BigDecimal getDeliverQuantity() {
		return deliverQuantity;
	}
	public void setDeliverQuantity(BigDecimal deliverQuantity) {
		this.deliverQuantity = deliverQuantity;
	}

	public BigDecimal getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public StoreOrder getStoreOrder() {
		return storeOrder;
	}
	public void setStoreOrder(StoreOrder storeOrder) {
		this.storeOrder = storeOrder;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
	
	
	
}
