package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Oranization;

/**
 * 组织机构服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("oranizationService")
public class OranizationService extends BaseService<Oranization>
{
	@Resource(name = "oranizationDao")  
	public void setDao(IBaseDao<Oranization> dao) 
	{  
		super.setDao(dao);  
	}

}
