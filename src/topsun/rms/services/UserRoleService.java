package topsun.rms.services;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.User;
import topsun.rms.entity.UserRole;

/**
 * 用户角色  服务
 * 
 * @author yuxiaobei
 *
 */
@Service("userRoleService")
public class UserRoleService extends BaseService<UserRole> implements IUserRoleService
{
	@Resource(name = "userRoleDao")  
	public void setDao(IBaseDao<UserRole> dao) 
	{  
		super.setDao(dao);  
	}
	
	@Resource(name = "customDao")  
	private IBaseDao<User> customDao;
	
	@Resource(name = "resourceDao")  
	private IBaseDao<topsun.rms.entity.Resource> resourceDao;

	@Override
	public List<UserRole> findRoleByFilter(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from UserRole t join t.users user";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}
	
	@Override
	public List<UserRole> findRoleByFilterGroup(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from UserRole t join t.usergroups usergroup";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}
	
	/**
	 * 解决更新删除关联问题
	 */
	@Override
	public void update(UserRole role)
	{
		UserRole trole = getById(role.getId());
		Hibernate.initialize(trole.getUsers());
		Hibernate.initialize(trole.getResources());
		Hibernate.initialize(trole.getUsergroups());
		role.setUsers(trole.getUsers());
		role.setResources(trole.getResources());
		role.setUsergroups(trole.getUsergroups());
		
		super.merge(role);
	}

	@Override
	public void addUser(String id, String userIds) 
	{
		UserRole role = getById(id);
		if (role != null) 
		{
			for (String userId : userIds.split(",")) 
			{
				if (StringUtils.isBlank(userId))  continue;
				
				User user = customDao.getById(userId);
				if (user != null)
				{
					role.getUsers().add(user);
				}
			}
			super.save(role);
		}
	}

	@Override
	public void deleteUser(String id, String userIds) 
	{
		UserRole role = getById(id);
		if (role != null) 
		{
			for (String userId : userIds.split(",")) 
			{
				if (StringUtils.isBlank(userId))  continue;
				
				User user = customDao.getById(userId);
				if (user != null)
				{
					role.getUsers().remove(user);
				}
			}
			super.save(role);
		}
	}

	@Override
	public void addRes(String id, String resIds) 
	{
		UserRole role = getById(id);
		role.getResources().clear();
		if (role != null) 
		{
			for (String resId : resIds.split(",")) 
			{
				if (StringUtils.isBlank(resId))  continue;
				
				topsun.rms.entity.Resource resource = resourceDao.getById(resId);
				if (resource != null)
				{
					role.getResources().add(resource);
				}
			}
			super.save(role);
		}
		
	}

	@Override
	public void deleteRes(String id, String resIds) 
	{
		UserRole role = getById(id);
		if (role != null) 
		{
			for (String resId : resIds.split(",")) 
			{
				if (StringUtils.isBlank(resId))  continue;
				
				topsun.rms.entity.Resource resource = resourceDao.getById(resId);
				if (resource != null)
				{
					role.getResources().remove(resource);
				}
			}
			super.save(role);
		}
		
	}
}
