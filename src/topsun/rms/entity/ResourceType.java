package topsun.rms.entity;

/**
 * 资源类型 实体模型
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class ResourceType implements java.io.Serializable
{
	private String id;
	private String name;
	private java.util.Set<Resource> resources;
	
	public ResourceType()
	{
		
	}
	
	public String getId() 
	{
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public java.util.Set<Resource> getResources() 
	{
		return resources;
	}

	public void setResources(java.util.Set<Resource> resources) 
	{
		this.resources = resources;
	}

	
	
}
