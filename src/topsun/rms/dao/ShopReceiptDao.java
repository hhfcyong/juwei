package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.ShopReceipt;

/**
 * 门店收货  Dao
 * 
 * @author Tang
 *
 */

@Repository("shopReceiptDao")  
@Transactional
public class ShopReceiptDao extends BaseDao<ShopReceipt>{

}
