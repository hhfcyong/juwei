package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.MemberCard;

/**
 * 会员卡服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("memberCardService")
public class MemberCardService extends BaseService<MemberCard>
{
	@Resource(name = "memberCardDao")  
	public void setDao(IBaseDao<MemberCard> dao) 
	{  
		super.setDao(dao);  
	}

}
