package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.ItemSchemeDetail;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("datapage/itemschemedetail")
public class ItemSchemeDetailController extends BaseController<ItemSchemeDetail> 
{
	@Resource(name = "itemSchemeDetailService")  
	public void setService(IBaseService<ItemSchemeDetail> Service1) 
	{  
		super.setService(Service1);		
	}	
	
}