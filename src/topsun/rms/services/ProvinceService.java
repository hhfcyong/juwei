package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Province;

/**
 * 省 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("provinceService")
public class ProvinceService extends BaseService<Province>
{
	@Resource(name = "provinceDao")  
	public void setDao(IBaseDao<Province> dao) 
	{  
		super.setDao(dao);  
	}
}
