package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.MemberCardType;
import topsun.rms.services.IBaseService;

/**
 * 会员卡类型 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("membership/membercardtype")
public class MemberCardTypeController extends BaseController<MemberCardType>
{
	@Resource(name = "memberCardTypeService")  
	public void setService(IBaseService<MemberCardType> Service1) 
	{  
		super.setService(Service1);
	}
}
