package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Oranization;

/**
 * 组织机构管理 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("oranizationDao")
@Transactional  
public class OranizationDao extends BaseDao<Oranization> 
{
	
}
