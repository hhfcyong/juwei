package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.MemberCard;

/**
 * 会员卡 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("memberCardDao")
@Transactional  
public class MemberCardDao extends BaseDao<MemberCard> 
{
	
}
