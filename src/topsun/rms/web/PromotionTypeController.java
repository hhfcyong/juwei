package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.PromotionType;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("promotionpage/protype")
public class PromotionTypeController extends BaseController<PromotionType>
{
	@Resource(name = "promotionTypeService")  
	public void setService(IBaseService<PromotionType> Service1) 
	{  
		super.setService(Service1);
	}
	
}
