package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.County;

/**
 * 区县 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("countyService")
public class CountyService extends BaseService<County>
{
	@Resource(name = "countyDao")  
	public void setDao(IBaseDao<County> dao) 
	{  
		super.setDao(dao);  
	}
}
