package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.MemberCardType;

/**
 * 会员卡类型 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("memberCardTypeService")
public class MemberCardTypeService extends BaseService<MemberCardType>
{
	@Resource(name = "memberCardTypeDao")  
	public void setDao(IBaseDao<MemberCardType> dao) 
	{  
		super.setDao(dao);  
	}
}
