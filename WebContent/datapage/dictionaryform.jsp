<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var dictypeid;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/dic/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/dic/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			
			if(typeof(dictypeid)!="undefined" && dictypeid!='') {
				jsonuserinfo = jsonuserinfo.substring(0,jsonuserinfo.length-1);
				jsonuserinfo =  jsonuserinfo  +',' +dictypeid+'}';
			}
		
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  cancleform($dialog, $grid);
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		$('#dictype').combobox({
			onSelect:function(row){
				dictypeid = '"datadictionarytype":{"id":"'+ row.id+'"}' ;
			}
		});
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		
		
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/datapage/dic/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'sortno' : obj.sortno
					});
				}

				if(obj.datadictionarytype !=null){
					$('#dictype').combobox('setValue', obj.datadictionarytype.id );
					dictypeid = '"datadictionarytype":{"id":"'+ obj.datadictionarytype.id+'"}' ;
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>数据字典值</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>数据字典名称</th>
					<td><input name="name" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
					<th>数据字典类型</th>
					<td><select  id="dictype" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/dictype/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>排序值</th>
					<td><input name="sortno" class="easyui-numberbox" data-options="required:true,min:0,max:100000" style="width: 155px;" value="100" /></td>
				</tr>
			</table>
		</form>
	
</body>
</html>