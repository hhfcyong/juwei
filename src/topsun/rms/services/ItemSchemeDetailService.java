package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.ItemSchemeDetail;

/**
 * 预定义方案服务类
 * 
 * @author Tang
 *
 */

@Service("itemSchemeDetailService")
public class ItemSchemeDetailService extends BaseService<ItemSchemeDetail>
{
	@Resource(name = "itemSchemeDetailDao")  
	public void setDao(IBaseDao<ItemSchemeDetail> dao) 
	{  
		super.setDao(dao);  
	}
}