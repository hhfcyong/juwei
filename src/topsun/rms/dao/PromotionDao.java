package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Promotion;;

/**
 * 员工 Dao
 * 
 * @author zy
 *
 */
@Repository("promotionDao")
@Transactional  
public class PromotionDao extends BaseDao<Promotion> 
{
	
}
