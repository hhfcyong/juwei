<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>店铺管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
var selectform = function($dialog, $grid, id, type) {		
		
		if ($('form').form('validate')) {
			

			var arr =$('#dg').datagrid('getChecked');
			if(arr.length <=0){
				$.messager.show({
					title:'提示信息!',
					msg:'至少选择一行记录!'
				});
				return;
			}
			
			var ids = '';
			for(var i =0 ;i<arr.length;i++){
				ids += arr[i].id + ',' ;
			}
			var roleid = id;
			var url;
			if(type=='shop'){
			
				url = topsun.contextPath + '/promotionpage/proapply/addShop.do';
			
			}else{
			//	url = topsun.contextPath + '/imsbase/usergroup/adduser.do';
			}

			$.post(url ,
					{id:roleid,ids:ids} , function(result){
					if (result.success){
						 $dialog.dialog('destroy');		 
						 $grid.datagrid('reload');	 	
					}else {
						$.messager.show({	 
							title: 'Error',
							msg: 'error'
						});
					}
			},'json');
		}
	};
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/datapage/shopform.jsp');
		}
		
		function formatdepartment(val,row){ if(val!=null) return val.name;else return ''; }
		 //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		
		$(function(){
		
			$('#selbarea').combobox({
				onSelect:function(row){
					
					 $('#selprovince').combobox('clear');
					 $('#selcity').combobox('clear');
					 $('#selcounty').combobox('clear');
					 
					 $('#selcity').combobox('loadData', '');
					 $('#selcounty').combobox('loadData', '');
					 
					 var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+row.code;
					 $('#selprovince').combobox('reload', url);
					
				}
			});
			$('#selprovince').combobox({
				onSelect:function(row){
					
					 $('#selcity').combobox('clear');
					 $('#selcounty').combobox('clear');
					 
					 $('#selcounty').combobox('loadData', '');
					 
					 var url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+row.code;
					 $('#selcity').combobox('reload', url);
				}
			});
			
			$('#selcity').combobox({
				onSelect:function(row){
					 $('#selcounty').combobox('clear');
					 
					 var url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+row.code;
					 $('#selcounty').combobox('reload', url);
				}
			});
			
			
		});
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					<label style="margin: 0px 0px 0px 10px;">店铺名:</label> <input name="QUERY_t#name_S_LK" class="easyui-validatebox" style="width: 100px; "  />
					<label style="margin: 0px 0px 0px 10px;">简称:</label> <input name="QUERY_t#sname_S_LK" class="easyui-validatebox"  style="width: 100px; " />
					<label style="margin: 0px 0px 0px 10px;">门店类型:</label> <select name="QUERY_t#shoptype_S_EQ"  class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/dic/listbydictype.do?dictype=1',panelHeight:'auto'" style="width: 100px;"></select>
					<label style="margin: 0px 0px 0px 10px;">经营方式:</label> <select name="QUERY_t#managemode_S_EQ"  class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/dic/listbydictype.do?dictype=2',panelHeight:'auto'" style="width: 100px;"></select>
					<label style="margin: 0px 0px 0px 10px;">大区:</label>  <select name="QUERY_t#barea_S_EQ"  id="selbarea" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name',url:'<%=contextPath%>/datapage/barea/listall.do',panelHeight:'auto'" style="width: 100px;"></select>
					<label style="margin: 0px 0px 0px 10px;">省:</label> <select name="QUERY_t#province_S_EQ" id="selprovince" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name',panelHeight:'auto'" style="width: 100px;  "></select>
					<label style="margin: 0px 0px 0px 10px;">市:</label> <select name="QUERY_t#city_S_EQ" id="selcity" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name',panelHeight:'auto'" style="width: 100px;  "></select>
					<label style="margin: 0px 0px 0px 10px;">区县:</label><select name="QUERY_t#county_S_EQ" id="selcounty" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name',panelHeight:'auto'" style="width: 100px;  "></select>
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch').form('clear');reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="店铺管理" class="easyui-datagrid" url="shop/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				    <th field="code" width="250">门店编码</th>
					<th field="name" width="200">名称</th>
					<th field="sname" width="200">简称</th>
					<th field="shoptype" width="200">类型</th>
					<th field="managemode" width="200">经营方式</th>
					<!-- <th field="oranization" width="200" formatter="formatdepartment">所属部门</th> -->
					<th field="address" width="200">联系地址</th>
					<th field="phone" width="200">电话号码</th>
					<th field="contacts" width="200">联系人</th>
					<th field="beginmanagedate" width="200">开始经营日</th>
					<th field="bank" width="200">往来银行</th>
					<th field="bankaccount" width="200">银行账户</th>
					<th field="cityname" width="200">市</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("shopmanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/datapage/shopform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("shopmanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/datapage/shopform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("shopmanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/datapage/shop/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("shopmanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/datapage/shopform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("shopmanager.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="topsun.copyFun($('#dg'),'/datapage/shopform.jsp')">复制</a></td>
				<%}%>
				<%if (securityUtil.havePermission("shopmanager.export")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon importexcle" plain="true" onclick="topsun.exportExcel($('#dg'),'/datapage/shop/exportexcel.do',$('#mysearch'))" >导出</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
	
  </body>
</html>
