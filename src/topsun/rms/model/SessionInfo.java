package topsun.rms.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import topsun.rms.entity.User;
import topsun.rms.entity.UserGroup;
import topsun.rms.entity.UserRole;

/**
 * sessionInfo模型，登录成功，设置到session里面，便于系统使用
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class SessionInfo  implements java.io.Serializable 
{

	private User user;
	
	private Map<String, topsun.rms.entity.Resource> resources = new HashMap<String, topsun.rms.entity.Resource>();
	
	private Set<String> urlResources = new HashSet<String>();
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
		
		List<topsun.rms.entity.Resource> l = new ArrayList<topsun.rms.entity.Resource>();
		for (UserRole role : user.getUserroles())
		{
			l.addAll(role.getResources());
		}
		
		for (UserGroup ug : user.getUsergroups())
		{
			for (UserRole role : ug.getUserroles())
			{
				l.addAll(role.getResources());
			}
		}
		
		l = new ArrayList<topsun.rms.entity.Resource>(new HashSet<topsun.rms.entity.Resource>(l));// 去重(这里包含了当前用户可访问的所有资源)
		resources.clear();
		for(topsun.rms.entity.Resource resource : l)
		{
			if(StringUtils.isNotBlank(resource.getCode()) && !resources.containsKey(resource.getCode()))
			{
				resources.put(resource.getCode(), resource);
			}
			
			urlResources.add(resource.getUrl());
		}
	}

	public Map<String, topsun.rms.entity.Resource> getResources() {
		return resources;
	}

	public void setResources(Map<String, topsun.rms.entity.Resource> resources) {
		this.resources = resources;
	}
	
	public Set<String> getUrlResources() {
		return urlResources;
	}

	public void setUrlResources(Set<String> urlResources) {
		this.urlResources = urlResources;
	}

}
