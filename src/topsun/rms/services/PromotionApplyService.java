package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Item;
import topsun.rms.entity.Shop;
import topsun.rms.entity.Promotion;
import topsun.rms.entity.PromotionApply;
import topsun.rms.entity.User;
import topsun.rms.entity.UserRole;



/**
 * 单品 服务实现
 * 
 * @author zy
 *
 */
@Service("promotionapplyService")
public class PromotionApplyService extends BaseService<PromotionApply>  implements IPromotionApplyService
{  
	@Resource(name = "shopDao")  
	private IBaseDao<Shop> customDao;
	@Resource(name = "promotionapplyDao")  
	public void setDao(IBaseDao<PromotionApply> dao) 
	{  
		super.setDao(dao);  
	}
	@Override
	public List<PromotionApply> findpromotionapplyByFilter(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from PromotionApply t";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}
	@Override
	public void addShop(int id, String userIds) 
	{
		PromotionApply promotionapply = getById(id);
		if (promotionapply != null) 
		{
			for (String userId : userIds.split(",")) 
			{
				if (StringUtils.isBlank(userId))  continue;
				
				Shop shop = customDao.getById(userId);
				if (shop != null)
				{
					promotionapply.getShops().add(shop);
				}
			}
			super.save(promotionapply);
		}
	}
	@Override
	public void deleteShop(int id,String roleIds)
	{
		PromotionApply promotionapply = getById(id);
		if(promotionapply != null)
		{
			for(String roleId :roleIds.split(",") )
			{
				if (StringUtils.isBlank(roleId))  continue;
				
				Shop shop = customDao.getById(roleId);
				if (shop != null)
				{
					promotionapply.getShops().remove(shop);
				}
			}
			super.save(promotionapply);
		}
	}
}
