package topsun.rms.entity;

import java.math.BigDecimal;

@SuppressWarnings("serial")
public class ItemSchemeDetail {

	private int id;
	private Item item;
	private String itemName;
	private BigDecimal quantity;
	private String unitId;
	private BigDecimal tradePrice;
	private BigDecimal costPrice;
	private ItemScheme itemScheme;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public BigDecimal getTradePrice() {
		return tradePrice;
	}
	public void setTradePrice(BigDecimal tradePrice) {
		this.tradePrice = tradePrice;
	}
	public BigDecimal getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(BigDecimal costPrice) {
		this.costPrice = costPrice;
	}
	public ItemScheme getItemScheme() {
		return itemScheme;
	}
	public void setItemScheme(ItemScheme itemScheme) {
		this.itemScheme = itemScheme;
	}
	
}
