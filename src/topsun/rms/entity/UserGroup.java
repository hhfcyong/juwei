package topsun.rms.entity;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;


/**
 * 用户组 实体模型
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class UserGroup implements java.io.Serializable
{
	private String id;
	private String name;
	private Set<UserRole> userroles = new HashSet<UserRole>(0);
	private Set<User> users = new HashSet<User>(0);
	
	public UserGroup()
	{
		
	}
	public UserGroup(String id)
	{
		this.id = id;
	}
	
	public UserGroup(String id,String name)
	{
		this.id = id;
		this.name = name;
	}
	public String getId() {
//		return id;
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Set<UserRole> getUserroles() {
		return userroles;
	}
	public void setUserroles(Set<UserRole> userroles) {
		this.userroles = userroles;
	}
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
}
