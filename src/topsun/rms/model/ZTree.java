package topsun.rms.model;


/**
 * ZTree模型
 * 
 * @author yuxiaobei
 * 
 */
@SuppressWarnings("serial")
public class ZTree implements java.io.Serializable 
{

	private String id;
	private String name;
	private boolean open = false;
	private String pId ;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public String getPId() {
		return pId;
	}
	public void setPId(String pId) {
		this.pId = pId;
	}

}
