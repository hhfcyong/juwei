<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var departmentid;
    var bLoad = false;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/shop/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/shop/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			
			if(typeof(departmentid)!="undefined" && departmentid!='') {
				jsonuserinfo = jsonuserinfo.substring(0,jsonuserinfo.length-1);
				jsonuserinfo =  jsonuserinfo  +',' +departmentid+'}';
			}
		
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
					  $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		
		$('#selbarea').combobox({
			onSelect:function(row){
				
				 if(bLoad) return;
				 $('#selprovince').combobox('clear');
				 $('#selcity').combobox('clear');
				 $('#selcounty').combobox('clear');
				 
				 $('#selcity').combobox('loadData', '');
				 $('#selcounty').combobox('loadData', '');
				 
				 var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+row.code;
				 $('#selprovince').combobox('reload', url);
				
			}
		});
		$('#selprovince').combobox({
			onSelect:function(row){
				 if(bLoad) return;
				 
				 $('#selcity').combobox('clear');
				 $('#selcounty').combobox('clear');
				 
				 $('#selcounty').combobox('loadData', '');
				 
				 var url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+row.code;
				 $('#selcity').combobox('reload', url);
			}
		});
		
		$('#selcity').combobox({
			onSelect:function(row){
				 if(bLoad) return;
				 $('#selcounty').combobox('clear');
				 
				 var url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+row.code;
				 $('#selcounty').combobox('reload', url);
			}
		});
		
		$('#oranization').combotree({
			onSelect:function(row){
				departmentid = '"oranization":{"id":"'+ row.id+'"}' ;
			}
		});
		
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/datapage/shop/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'code' : obj.code,
						'sname' : obj.sname,
						'phone' : obj.phone,
						'oranization':obj.oranization.id,
						'contacts' : obj.contacts,
						'beginmanagedate':obj.beginmanagedate,
						'bank' : obj.bank,
						'bankaccount' : obj.bankaccount,
						'address': obj.address
					});
				}
				bLoad = true;
				 
				$('#shoptype').combobox('select', obj.shoptype );
				$('#managemode').combobox('select', obj.managemode );
				
				$('#selbarea').combobox('select', obj.barea );
				$('#selprovince').combobox('select', obj.province);
				
				var url = topsun.contextPath + '/datapage/province/listbypcode.do?pcode='+obj.barea;
				 $('#selprovince').combobox('reload', url);
				
				 url = topsun.contextPath + '/datapage/city/listbypcode.do?pcode='+obj.province;
				 $('#selcity').combobox('reload', url);
				 
				 url = topsun.contextPath + '/datapage/county/listbypcode.do?pcode='+obj.city;
				 $('#selcounty').combobox('reload', url);
				
				if(obj.city !=null && obj.city!=''){
					$('#selcity').combobox('select', obj.city );
				}
				if(obj.county !=null && obj.county!=''){
					$('#selcounty').combobox('select', obj.county );
				}
				
				if(obj.oranization !=null){
					$('#oranization').combotree('setValue', obj.oranization.id );
				}
				bLoad = false;
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>店铺标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>店铺名称</th>
					<td><input name="name" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
					<th>门店编码</th>
					<td><input name="code"  class="easyui-validatebox" /></td>
					<th>简称</th>
					<td><input name="sname" class="easyui-validatebox"  /></td>
				</tr>
				<tr>
					<th>门店类型</th>
					<td><select id="shoptype" name="shoptype" class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/dic/listbydictype.do?dictype=1',panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>经营方式</th>
					<td><select id="managemode" name="managemode" class="easyui-combobox" data-options="editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/dic/listbydictype.do?dictype=2',panelHeight:'auto'" style="width: 155px;"></select></td>
				</tr>
				<tr>
					<th>所属部门</th>
					<td><select id="oranization"  class="easyui-combotree" data-options="editable:false,idField:'id',textField:'text',parentField:'pid',url:'<%=contextPath%>/imsbase/ora/getTreeOranization.do'" style="width: 155px;"></select><img class="iconImg ext-icon-cross" onclick="$('#oranization').combotree('clear');" title="清空" /></td>
					<th>电话号码</th>
					<td><input name="phone" class="easyui-validatebox"  /></td>
				</tr>
				
				<tr>
					<th>大区</th>
					<td><select id="selbarea" name="barea" class="easyui-combotree" data-options="editable:false,valueField:'code',textField:'name',url:'<%=contextPath%>/datapage/barea/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>省</th>
					<td><select id="selprovince" name="province" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name',url:'<%=contextPath%>/datapage/barea/listall.do',panelHeight:'auto'" style="width: 155px;" ></select></td>
				</tr>
				
				<tr>
					<th>市</th>
					<td><select id="selcity" name="city" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name',panelHeight:'auto'" style="width: 155px;" ></select></td>
					<th>区</th>
					<td><select id="selcounty" name="county" class="easyui-combobox" data-options="editable:false,valueField:'code',textField:'name'" style="width: 155px;" ></select></td>
				</tr>
				<tr>
					<th>联系人</th>
					<td><input name="contacts"  class="easyui-validatebox" /></td>
					<th>开始经营日</th>
					<td><input name="beginmanagedate"  class="easyui-datebox"  /></td>
				</tr>
				<tr>
					<th>往来银行</th>
					<td><input name="bank"  class="easyui-validatebox" /></td>
					<th>银行账户</th>
					<td><input name="bankaccount" class="easyui-numberbox" /></td>
				</tr>
				<tr>
					<th>地址</th>
					<td><input name="address" class="easyui-validatebox" /></td>
				</tr>
			</table>
		</form>
</body>
</html>