package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.ItemScheme;

/**
 * 预定义方案服务类
 * 
 * @author Tang
 *
 */
@Service("itemSchemeService")
public class ItemSchemeService extends BaseService<ItemScheme>
{
	@Resource(name = "itemSchemeDao")  
	public void setDao(IBaseDao<ItemScheme> dao) 
	{  
		super.setDao(dao);  
	}
}
