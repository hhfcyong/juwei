package topsun.rms.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.Oranization;
import topsun.rms.model.Tree;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IResourceService;
import topsun.rms.utility.BeanUtils;

/**
 * 组织机构 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/ora")
public class OranizationController extends BaseController<Oranization>
{
	@Resource(name = "oranizationService")  
	public void setService(IBaseService<Oranization> Service1) 
	{  
		super.setService(Service1);
	}
	
	/**
	 * Easyui tree menu
	 * 
	 * @param request
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/getTreeOranization", method = RequestMethod.POST)
	protected void getTreeOranization(HttpServletRequest request, HttpServletResponse response)
	{
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addSort("sortno");
		hqlFilter.addOrder("asc");
		
		List<Oranization> oranizations = service.find();
		List<Tree> tree = new ArrayList<Tree>();
		for (Oranization oranization : oranizations) 
		{
			Tree node = new Tree();
			BeanUtils.copyNotNullProperties(oranization, node);
			node.setText(oranization.getName());
			Map<String, String> attributes = new HashMap<String, String>();
			node.setAttributes(attributes);
			tree.add(node);
		}
		
		writeJson(tree, request, response);
	}
	
}
