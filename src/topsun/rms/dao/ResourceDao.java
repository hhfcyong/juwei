package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Resource;

/**
 * 资源管理 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("resourceDao")
@Transactional  
public class ResourceDao extends BaseDao<Resource> 
{
	
}
