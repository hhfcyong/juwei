package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.UserRole;

/**
 * 用户角色 服务
 * 
 * @author yuxiaobei
 *
 */
public interface IUserRoleService extends IBaseService<UserRole>
{
	/**
	 * 通过用户 查询用户所拥有的角色
	 * 
	 * @param hqlFilter
	 */
	public List<UserRole> findRoleByFilter(HqlFilter hqlFilter);
	
	
	/**
	 * 通过用户组 查询用户组所拥有的角色
	 * 
	 * @param hqlFilter
	 */
	public List<UserRole> findRoleByFilterGroup(HqlFilter hqlFilter);
	
	
	/**
	 * 修改用户角色
	 * 
	 * @param id
	 *            角色ID
	 * @param userIds
	 *            用户IDS
	 */
	public void addUser(String id, String userIds);
	
	/**
	 * 删除用户角色
	 * 
	 * @param id
	 * 			角色ID
	 * @param userIds
	 * 			用户IDs
	 */
	public void deleteUser(String id, String userIds);
	
	/**
	 * 修改用户角色
	 * 
	 * @param id
	 *            角色ID
	 * @param resIds
	 *            资源IDS
	 */
	public void addRes(String id, String resIds);
	
	/**
	 * 删除用户角色
	 * 
	 * @param id
	 * 			角色ID
	 * @param resIds
	 * 			资源IDs
	 */
	public void deleteRes(String id, String resIds);
}
