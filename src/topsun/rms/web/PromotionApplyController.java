package topsun.rms.web;

import java.util.Hashtable;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.DataDictionary;
import topsun.rms.entity.PromotionApply;
import topsun.rms.entity.ItemType;
import topsun.rms.entity.Unit;
import topsun.rms.entity.UserRole;
import topsun.rms.model.Pager;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IPromotionService;
import topsun.rms.services.IUserRoleService;
import topsun.rms.services.IPromotionApplyService;

@Controller
@RequestMapping("promotionpage/proapply")
public class PromotionApplyController extends BaseController<PromotionApply>
{
		
	@Resource(name = "promotionapplyService")  
	public void setService(IBaseService<PromotionApply> Service1) 
	{  
		super.setService(Service1);
	}	
	@Resource(name = "promotionapplyService")   
	private IBaseService<PromotionApply> service;
	
	@RequestMapping(value = "/getbypromoationid", method = RequestMethod.POST)
	public void getItemByPromotionId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);   
		hqlFilter.addFilter("QUERY_t#promotionid_I_EQ",id.toString());
		List<PromotionApply> items =((IPromotionApplyService)service).findpromotionapplyByFilter(hqlFilter);
		writeJson(items,request,response);
	}
	@RequestMapping(value = "/addShop", method = RequestMethod.POST)
	public void addItem( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IPromotionApplyService customService = (IPromotionApplyService)service;
		if((getId()!=null) && StringUtils.isNotBlank(getIds()))
		{
			customService.addShop(Integer.parseInt(getId().toString()), getIds());
		}
		
		excuteSuccess(request,response);
	}
	@RequestMapping(value = "/deleteshop", method = RequestMethod.POST)
	public void deleteRole( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		IPromotionApplyService customService = (IPromotionApplyService)service;
		
		if(getId() !=null && StringUtils.isNotBlank(getIds()))
		{
			customService.deleteShop(Integer.parseInt(getId().toString()), getIds());
		}
		
		excuteSuccess(request,response);
	}
}
