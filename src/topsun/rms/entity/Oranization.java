package topsun.rms.entity;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
/**
 * 组织机构 实体模型
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Oranization implements java.io.Serializable
{
	private String pid;// 虚拟属性，用于获得当前资源的父资源ID
	
	private String id;
	private String name;
	private String description;
	private Long sortno;
	private String code;
	private Oranization oranization;
	private Set<Oranization> oranizations = new HashSet<Oranization>(0);
	private Set<Warehouse> warehouses = new HashSet<Warehouse>(0);
	
	private Set<Shop> shops = new HashSet<Shop>(0);
	
	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
//		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getSortno() {
		return sortno;
	}
	public void setSortno(Long sortno) {
		this.sortno = sortno;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	
	public String getPid() 
	{
		if (oranization != null && !StringUtils.isBlank(oranization.getId())) 
		{
			return oranization.getId();
		}
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}
	public Oranization getOranization() {
		return oranization;
	}
	public void setOranization(Oranization oranization) {
		this.oranization = oranization;
	}
	public Set<Oranization> getOranizations() {
		return oranizations;
	}
	public void setOranizations(Set<Oranization> oranizations) {
		this.oranizations = oranizations;
	}
	public Set<Warehouse> getWarehouses() {
		return warehouses;
	}
	public void setWarehouses(Set<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}
	public Set<Shop> getShops() {
		return shops;
	}
	public void setShops(Set<Shop> shops) {
		this.shops = shops;
	}
	
	
}
