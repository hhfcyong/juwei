package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.BArea;
import topsun.rms.services.IBaseService;

/**
 * 大区 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/barea")
public class BAreaController extends BaseController<BArea>
{
	@Resource(name = "bAreaService")  
	public void setService(IBaseService<BArea> Service1) 
	{  
		super.setService(Service1);
	}
}
