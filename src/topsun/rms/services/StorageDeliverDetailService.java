package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.StorageDeliverDetail;
import topsun.rms.entity.StorageReceiptDetail;

/**
 * 自定义仓库发货服务类
 * 
 * @author Tang
 *
 */
@Service("storageDeliverDetailService")
public class StorageDeliverDetailService extends BaseService<StorageDeliverDetail> {
	@Resource(name = "storageDeliverDetailDao")  
	public void setDao(IBaseDao<StorageDeliverDetail> dao) 
	{  
		super.setDao(dao);  
	}

}
