package topsun.rms.entity;


/**
 * 省
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class Province implements java.io.Serializable
{
	private int id;
	private String name;
	private String code;
	private String bareacode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getBareacode() {
		return bareacode;
	}
	public void setBareacode(String bareacode) {
		this.bareacode = bareacode;
	}
	
}
