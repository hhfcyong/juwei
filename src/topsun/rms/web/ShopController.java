package topsun.rms.web;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.DataDictionary;
import topsun.rms.entity.Shop;
import topsun.rms.entity.VShop;
import topsun.rms.model.Pager;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IShopService;
import topsun.rms.utility.MapJavaBeanUtil;

/**
 * 店铺 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/shop")
public class ShopController extends BaseController<Shop>
{
	protected Hashtable<String, String> shopTypeList = new Hashtable<>();
	protected Hashtable<String, String> manageModeList = new Hashtable<>();
	
	
	
	@Resource(name = "shopService")  
	public void setService(IBaseService<Shop> Service1) 
	{  
		super.setService(Service1);
	}
	
	@Resource(name = "dataDictionaryService")   
	private IBaseService<DataDictionary> dataDictionaryService;
	
	/**
	 * get字典信息
	 */
	private void getDictionarys()
	{
		HqlFilter hqlFilter = new HqlFilter();
		List<DataDictionary> dictionarys = dataDictionaryService.findByFilter(hqlFilter);
		for (DataDictionary dataDictionary : dictionarys) 
		{
			if(dataDictionary.getDatadictionarytype() == null) continue;
			switch(dataDictionary.getDatadictionarytype().getId())
			{
				case "1":
					if(!shopTypeList.containsKey(dataDictionary.getId()))
					{
						shopTypeList.put(dataDictionary.getId(), dataDictionary.getName());
					}
					break;
				case "2":
					if(!manageModeList.containsKey(dataDictionary.getId()))
					{
						manageModeList.put(dataDictionary.getId(), dataDictionary.getName());
					}
					break;
			}
		}
	}
	
	@Override
	protected void grid(HttpServletRequest request, HttpServletResponse response) 
	{
		getDictionarys();  //temp
		parseRequest(request);
		
		Pager pager = new Pager();
		HqlFilter hqlFilter = new HqlFilter(request);
		pager.setTotal(service.countByFilter(hqlFilter));
		
//		List<Shop> shops = service.findByFilter(hqlFilter, page, rows);
		String strSql = "select * from v_shop t" +hqlFilter.getWhereAndOrderHql();
		List<Map> shops1 = service.findBySql(strSql,hqlFilter.getParams(), page, rows);
		List<VShop> vshops = new ArrayList<VShop>();
		//字典转换
		for (Map map : shops1)
		{
			try {
				vshops.add((VShop)MapJavaBeanUtil.convertMap(VShop.class, map));
			} catch (Exception e) {
			} 
		}
		//字典转换
		for (VShop shop : vshops)
		{
			String shopType  = shop.getShoptype();
			if(StringUtils.isBlank(shopType)) continue;
			if(shopTypeList.containsKey(shopType)) 
			{
				shop.setShoptype(shopTypeList.get(shopType));
			}
			
			String manageMode  = shop.getManagemode();
			if(StringUtils.isBlank(manageMode)) continue;
			
			if(manageModeList.containsKey(manageMode)) 
			{
				shop.setManagemode(manageModeList.get(manageMode));
			}
		}
		
		pager.setRows(vshops);
		writeJson(pager, request, response);
	}
	/**
	 * 根据countycode获取
	 */
	@RequestMapping(value = "/listbypcode", method = RequestMethod.POST)
	protected void findAllEx(HttpServletRequest request, HttpServletResponse response) 
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter(request);
		
		if(paramList.containsKey("pcode"));
		{
			hqlFilter.addFilter("QUERY_t#county_S_EQ", paramList.get("pcode"));
		}
		
		writeJson(service.findByFilter(hqlFilter), request, response);
	}
	@RequestMapping(value = "/getbyshopid", method = RequestMethod.POST)
	public void getItemByPromotionId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);   
		hqlFilter.addFilter("QUERY_promotionapply#id_I_EQ",id.toString());
		List<Shop> shops =((IShopService)service).findShopByFilter(hqlFilter);
		writeJson(shops,request,response);
	}
}
