package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.MemberGrade;

/**
 * 会员等级Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("memberGradeDao")
@Transactional  
public class MemberGradeDao extends BaseDao<MemberGrade> 
{
	
}
