package topsun.rms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.MemberCard;
import topsun.rms.services.IBaseService;

/**
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("membership/membercard")
public class MemberCardController extends BaseController<MemberCard>
{
	@Resource(name = "memberCardService")  
	public void setService(IBaseService<MemberCard> Service1) 
	{  
		super.setService(Service1);
	}
	
	@RequestMapping(value = "/getbymemberid", method = RequestMethod.POST)
	public void getRoleByUserId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_t#member#id_S_EQ", id.toString());
		List<MemberCard> membercards = service.findByFilter(hqlFilter);
		writeJson(membercards,request,response);
	}
}
