<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    //提交直接关闭
    var submitForm = function($dialog, $grid) {		
    	 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	var addItemFun = function() {
		if (entityid == "") {
			$.messager.show({
				title: '提示',
				msg: '请先保存用户'
			});
			return;
		}
	  var dialog = parent.topsun.modalDialog({
			title : '选择信息',
			url : topsun.contextPath + '/datapage/item.jsp',
			buttons : [ {
				text : '选择',
				iconCls : 'icon-edit',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.selectform(dialog, $('#dgitem'), entityid,'item');
				}
			} ]
		});
	};
	
	var removeItemFun = function() {
		var arr =$('#dgitem').datagrid('getChecked');
		if(arr.length <=0){
			$.messager.show({
				title:'提示信息!',
				msg:'至少选择一行记录进行删除!'
			});
			return;
		} else {
			$.messager.confirm('提示信息' , '确认删除?' , function(r){
					if(r){
							var ids = '';
							for(var i =0 ;i<arr.length;i++){
								ids += arr[i].id + ',' ;
							}
							ids = ids.substring(0 , ids.length-1);
							$.post(topsun.contextPath + '/promotionpage/pro/deleteitem.do' ,
									{id:entityid,ids:ids} , function(result){
									if (result.success){
										$('#dgitem').datagrid('reload');
										$('#dgitem').datagrid('unselectAll');
									}else {
										$.messager.show({	 
											title: 'Error',
											msg: 'error'
										});
									}
							},'json');
							
					} 
			});
		}
	};
	
	
	
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/promotionpage/pro/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name
					});
				}
				if(model =="edit"){
					$('#dgitem').datagrid({
						
						url:topsun.contextPath + '/datapage/item/getbyitemid.do?id='+obj.id+''
				   });
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:70px;visible:false;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>促销方案标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="mainPanle" region="center" style="background: #eee; overflow-y:hidden"  >
      <div id="tabs" class="easyui-tabs"  fit="true" border="false" >
     	 <div title="包含单品" style="padding:5px;overflow:hidden; " >
    	  <table id="dgitem"  class="easyui-datagrid"  
					 fit="true" striped="true"
					pagination="true" toolbar="#toolbar"
					rownumbers="true" fitColumns="true" 
					loadMsg="数据正在加载,请耐心的等待..." >
				<thead frozen="true">
					<tr>
						<th field="ck" checkbox="true"></th>
					</tr>
				</thead>
				<thead>
					<tr>
						<th field="id" width="100">单品序号</th>
						<th field="itemcode" width="180">单品编码</th>
						<th field="name" width="180">单品名字</th>
						<th field="description" width="180">单品描述</th>
						<th field="unit" width="80">单位</th>
						<th field="skucode" width="180">SKU</th>
					</tr>
				</thead>
			</table>
        </div>       
   	  </div>
  	</div>
   <div id="toolbar">
     	<table>
			<tr>
			 <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addItemFun()">添加</a>
	     	 <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeItemFun()">删除</a>
				
			</tr>
		</table>
	</div>
	
	
</body>
</html>