package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.ItemType;

/**
 * 单品分类 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("itemTypeDao")
@Transactional  
public class ItemTypeDao extends BaseDao<ItemType> 
{
	
}
