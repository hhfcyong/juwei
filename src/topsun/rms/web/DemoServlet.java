package topsun.rms.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import topsun.framework.Common.Errors;
import topsun.framework.utility.StringHelper;
import topsun.framework.web.SRFExHttpServlet;
import topsun.framework.web.SRFExHttpServletContext;
import topsun.rms.ctrl.DemoServiceInstance;
import topsun.rms.ctrl.MessagePackage;
import topsun.rms.ctrl.RemoteAction;
import topsun.rms.ctrl.RmsException;

public class DemoServlet extends SRFExHttpServlet 
{
	private static final Log log = LogFactory.getLog(DemoServlet.class);
	
	protected DemoServiceInstance demoServiceInstance = null;
	
	protected DemoServiceInstance getDemoServiceInstance() throws ServletException
	{
		return demoServiceInstance;
	}
	@Override
	public void init() throws ServletException
	{
		super.init();
		
		demoServiceInstance = new DemoServiceInstance();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		this.addTimeOutHeaders(resp);
		resp.setCharacterEncoding("utf-8");   
		resp.setContentType("text/html; charset=utf-8");  
		
		SRFExHttpServletContext servletContext = new SRFExHttpServletContext(req, resp, this.getServletContext());
		
		MessagePackage messagePackage = null;
		try
		{
			RemoteAction remoteAction = new RemoteAction();
			remoteAction.FromWebContext(servletContext);
			messagePackage = this.getDemoServiceInstance().ProcessRemoteAction(remoteAction);
		}
		catch(Exception ex)
		{
			messagePackage = new MessagePackage();
			
			if(ex instanceof RmsException)
			{
				RmsException e = (RmsException)ex;
				messagePackage.setRetCode(e.getErrorCode());
				messagePackage.setRetInfo(e.getMessage());
			}
			else
			{
				messagePackage.setRetCode(Errors.INTERNALERROR);
				messagePackage.setRetInfo(ex.getMessage());
			}
			log.error("demo服务器处理远端请求发生异常，"+ex.getMessage(),ex);
		}
		
		try
		{
			resp.getWriter().print(messagePackage.toJSONString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		resp.setCharacterEncoding("utf-8");   
		resp.setContentType("text/html; charset=utf-8");   
		    
		this.addTimeOutHeaders(resp);
		SRFExHttpServletContext servletContext = new SRFExHttpServletContext(req, resp, this.getServletContext());
		MessagePackage messagePackage = null;
		try
		{
			InputStream in =req.getInputStream();
			
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] tmpbuf = new byte[1024*1024];
			int count = 0;
			
			while ((count = in.read(tmpbuf)) != -1)
			{
				bout.write(tmpbuf, 0, count);
				tmpbuf = new byte[1024];
				
			}
			in.close();
			byte[] orgData = bout.toByteArray();
		
			String strContent = new String(orgData,"UTF-8");
			
			RemoteAction remoteAction = new RemoteAction();
			
			remoteAction.FromWebContext(servletContext);
			if(!StringHelper.IsNullOrEmpty(strContent))
			{
				log.debug(strContent);
				remoteAction.setContent(strContent);
			}
			messagePackage = this.getDemoServiceInstance().ProcessRemoteAction(remoteAction);
		}
		catch(Exception ex)
		{
			messagePackage = new MessagePackage();
			if(ex instanceof RmsException)
			{
				RmsException e = (RmsException)ex;
				messagePackage.setRetCode(e.getErrorCode());
				messagePackage.setRetInfo(e.getMessage());
			}
			else
			{
				messagePackage.setRetCode(Errors.INTERNALERROR);
				messagePackage.setRetInfo(ex.getMessage());
			}
			log.error("demo服务器处理远端请求发生异常，"+ex.getMessage(),ex);
		}
		
		try
		{
			resp.getWriter().print(messagePackage.toJSONString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


}
