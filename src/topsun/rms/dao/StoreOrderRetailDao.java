package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.StoreOrder;
import topsun.rms.entity.StoreOrderRetail;
import topsun.rms.entity.Type;
@Repository("storeOrderRetailDao")
public class StoreOrderRetailDao extends BaseDao<StoreOrderRetail> {

}
