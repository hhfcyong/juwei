package topsun.rms.entity;

/**
 * 登录账号
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class LoginAccount implements java.io.Serializable 
{
	private String id;
	private String name;
	private String pwd;
	
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	

}
