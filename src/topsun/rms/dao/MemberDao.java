package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Member;

/**
 * 会员 Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("memberDao")
@Transactional  
public class MemberDao extends BaseDao<Member> 
{
	
}
