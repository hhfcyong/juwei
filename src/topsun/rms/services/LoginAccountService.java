package topsun.rms.services;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.LoginAccount;

@Service("loginAccountService")
public class LoginAccountService extends BaseService<LoginAccount> implements ILoginAccountService
{
	@Resource(name = "loginAccountDao")  
	public void setDao(IBaseDao<LoginAccount> dao) 
	{  	
		super.setDao(dao);  
	}

	@Override
	public List<LoginAccount> findAccountByFilter(HqlFilter hqlFilter) 
	{
		String hql = "select distinct t from LoginAccount t join t.user user";
		return find(hql + hqlFilter.getWhereAndOrderHql(), hqlFilter.getParams());
	}

}
