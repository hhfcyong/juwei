package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Member;

/**
 * 会员 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("memberService")
public class MemberService extends BaseService<Member>
{
	@Resource(name = "memberDao")  
	public void setDao(IBaseDao<Member> dao) 
	{  
		super.setDao(dao);  
	}
}
