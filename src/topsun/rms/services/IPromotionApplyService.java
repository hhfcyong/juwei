package topsun.rms.services;

import java.util.List;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.PromotionApply;


/**
 * 自定义服务
 * 
 * @author zy
 *
 */
public interface IPromotionApplyService extends IBaseService<PromotionApply>
{
   public List<PromotionApply> findpromotionapplyByFilter(HqlFilter hqlFilter);
   public void addShop(int id, String userIds);
   public void deleteShop(int id,String roleIds);
}
