package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.City;

/**
 * 市 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("cityService")
public class CityService extends BaseService<City>
{
	@Resource(name = "cityDao")  
	public void setDao(IBaseDao<City> dao) 
	{  
		super.setDao(dao);  
	}
}
