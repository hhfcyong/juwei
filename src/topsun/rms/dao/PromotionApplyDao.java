package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.PromotionApply;;

/**
 * 单品  Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("promotionapplyDao")
@Transactional  
public class PromotionApplyDao extends BaseDao<PromotionApply> 
{
	
}
