package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.ShopReceiptDetail;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("datapage/shopreceiptdetail")
public class ShopReceiptDetailController extends BaseController<ShopReceiptDetail> 
{
	@Resource(name = "shopReceiptDetailService")  
	public void setService(IBaseService<ShopReceiptDetail> Service1) 
	{  
		super.setService(Service1);		
	}
}
