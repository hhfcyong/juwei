package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.ItemType;
import topsun.rms.services.IBaseService;

/**
 * 单品类型 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/itemtype")
public class ItemTypeController extends BaseController<ItemType>
{
	@Resource(name = "itemTypeService")  
	public void setService(IBaseService<ItemType> Service1) 
	{  
		super.setService(Service1);
	}
}
