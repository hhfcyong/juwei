package topsun.rms.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.City;
import topsun.rms.services.IBaseService;

/**
 * 城市 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("datapage/city")
public class CityController extends BaseController<City>
{
	@Resource(name = "cityService")  
	public void setService(IBaseService<City> Service1) 
	{  
		super.setService(Service1);
	}
	
	/**
	 * 根据provincea查询
	 */
	@RequestMapping(value = "/listbypcode", method = RequestMethod.POST)
	protected void findAllEx(HttpServletRequest request, HttpServletResponse response) 
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter(request);
		
		if(paramList.containsKey("pcode"));
		{
			hqlFilter.addFilter("QUERY_t#provincecode_S_EQ", paramList.get("pcode"));
		}
		
		writeJson(service.findByFilter(hqlFilter), request, response);
	}
}
