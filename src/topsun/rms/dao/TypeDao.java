package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.Type;
@Repository("typeDao")
public class TypeDao extends BaseDao<Type> {

}
