package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.LoginLog;

/**
 * 登录日志Dao
 * 
 * @author yuxiaobei
 *
 */
@Repository("loginLogDao")
@Transactional  
public class LoginLogDao extends BaseDao<LoginLog> 
{
	
}
