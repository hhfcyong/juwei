package topsun.rms.utility;

import javax.servlet.http.HttpSession;

import topsun.rms.model.SessionInfo;

/**
 * 用于前台页面判断是否有权限的工具类
 * 
 * @author yuxiaobei
 * 
 */
public class SecurityUtil 
{
	private HttpSession session;

	public SecurityUtil(HttpSession session) 
	{
		this.session = session;
	}

	/**
	 * 判断当前用户是否可以访问某资源
	 * 
	 * @param resourcecode
	 *            资源编码
	 * @return
	 */
	public boolean havePermission(String resourcecode) {
		SessionInfo sessionInfo = (SessionInfo) session.getAttribute("sessionInfo");
		
		if(sessionInfo.getUser().getId().equals("system")) return true;
		
	    return sessionInfo.getResources().containsKey(resourcecode);
		
	}
}
