package topsun.rms.ctrl;

import java.util.Hashtable;
import java.util.Vector;
import net.sf.json.JSONObject;
import topsun.framework.Common.Errors;
import topsun.framework.utility.StringHelper;

/**
 * 消息包
 * @author yuxiaobei
 *
 */
public class MessagePackage 
{
	private int nRetCode = Errors.OK;
	private String nRetInfo = "";
	private Hashtable<String,String> extInfoMap = new Hashtable<String,String>();
	
	/**
	 * @return the nRetCode
	 */
	public int getRetCode()
	{
		return nRetCode;
	}

	/**
	 * @param nRetCode the nRetCode to set
	 */
	public void setRetCode(int nRetCode)
	{
		this.nRetCode = nRetCode;
	}

	/**
	 * @return the nRetInfo
	 */
	public String getRetInfo()
	{
		return nRetInfo;
	}

	/**
	 * @param nRetInfo the nRetInfo to set
	 */
	public void setRetInfo(String nRetInfo)
	{
		this.nRetInfo = nRetInfo;
	}

	
	
	
	public String toJSONString() throws Exception
	{
		JSONObject jo = new JSONObject();
		OnFillJSONObject(jo);		
		
		return jo.toString();
	}
	
	
	protected void OnFillJSONObject(JSONObject jo)  throws Exception
	{
		Vector<JSONObject> messages = new Vector<JSONObject>();

		jo.put("messages", messages.toArray());
		jo.put("retcode", nRetCode);
		jo.put("retinfo", nRetInfo);
		
		JSONObject extInfo = new JSONObject();
		for(String strKey:extInfoMap.keySet())
		{
			extInfo.put(strKey,extInfoMap.get(strKey));
		}
		
		jo.put("extinfo", extInfo);
	}

	
	public void setExtInfo(String strParam,String strValue)
	{
		strParam = strParam.toUpperCase();
		if(StringHelper.IsNullOrEmpty(strValue))
		{
			extInfoMap.remove(strParam);
		}
		else
		{
			extInfoMap.put(strParam,strValue);
		}
	}
	
	
	public String getExtInfo(String strParam,String strDefault)
	{
		strParam = strParam.toUpperCase();
		if(extInfoMap.containsKey(strParam))
		{
			return extInfoMap.get(strParam);
		}
		return strDefault;
	}

}
