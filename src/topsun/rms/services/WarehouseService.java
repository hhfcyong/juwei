package topsun.rms.services;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Warehouse;

/**
 * 仓库 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("warehouseService")
public class WarehouseService extends BaseService<Warehouse> implements IWarehouseService
{
	@Resource(name = "warehouseDao")  
	public void setDao(IBaseDao<Warehouse> dao) 
	{  
		super.setDao(dao);  
	}

	@Override
	public int getMaxCode()
	{
		String hql =  "select max(w.code) from Warehouse w";
		
		Object objcode = UniqueResult(hql);
		if(objcode == null ) return 0;
		
		return (int) objcode;
	}
}
