<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>员工管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	
		
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		
		
		function onDblClickRow(rowIndex, rowData){
			topsun.editFun($('#dg'), '/promotionpage/promotionform.jsp');
		}
		 //排序时触发
		   function onSortColumn(sort, order){
			   sortName = sort;
			   sortOrder = order;
			}
		
		$(function(){
			
		/* 不重复定义，会造成重复加载
		$('#dg').datagrid({
				onDblClickRow:function(rowIndex, rowData){
					editFun();
				},
				url:topsun.contextPath+'/imsbase/user2/list.do'
			});
		*/
		});
		function formatUser(val,row){if(val!=null) return val.name; else return '';}
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					方案编号:<input name="QUERY_t#promotionNO_S_LK" class="easyui-validatebox"  value="" />
					方案名称:<input name="QUERY_t#promotionName_S_LK" calss="easyui-validatebox" value=""/>
					方案描述:<input name="QUERY_t#description_S_LK" class="easyui-validatebox" value=""/>
					促销类型:<select  name="QUERY_t#promotionType#id_I_EQ"  class="easyui-combobox" data-options="required:false,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/promotionpage/protype/listall.do',panelHeight:'auto'" style="width: 155px;"></select>
					促销范围:<select  name="QUERY_t#promotionRange#id_I_EQ"  class="easyui-combobox" data-options="required:false,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/promotionpage/prorange/listall.do',panelHeight:'auto'" style="width: 155px;"></select>
			
					
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'"  onclick="javascript:$('#mysearch').form('clear');;reload();" >清空</a>
			</form>
		</fieldset>
		
		
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="促销管理" class="easyui-datagrid" url="pro/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="true"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				    <th field="id" width="40">方案序号</th>
					<th field="promotionNO" width="180">方案编号</th>
					<th field="promotionName" width="180">方案名称</th>
					<th field="description" width="200">方案描述</th>
					<th field="user" width="80" formatter="formatUser">制定人</th>
					<th field="formulateDate" width="100">制定日期</th>
					<th field="promotionType" width="50" formatter="formatUser">类型</th>
					<th field="promotionRange" width="50" formatter="formatUser">范围</th>
					<th field="value1" width="50">值1</th>
					<th field="value2" width="50">值2</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
     	<table>
			<tr>
				<%if (securityUtil.havePermission("Promotion.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="topsun.addFun($('#dg'),'/promotionpage/promotionform.jsp')">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("Promotion.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="topsun.editFun($('#dg'),'/promotionpage/promotionform.jsp')">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("Promotion.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeNFun($('#dg'),'/promotionpage/pro/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("Promotion.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/promotionpage/promotionform.jsp')">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("PromotionItem.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/promotionpage/promotionItemform.jsp')">促销明细</a></td>
				<%}%>
				<%if (securityUtil.havePermission("PromotionApply.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="topsun.viewFun($('#dg'),'/promotionpage/promotionapply.jsp')">促销应用</a></td>
				<%}%>
			</tr>
		</table>
	</div>
	
  </body>
</html>
