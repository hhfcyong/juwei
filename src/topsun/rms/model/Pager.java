package topsun.rms.model;

import java.util.ArrayList;
import java.util.List;

/**
 * EasyUI DataGrid 分页模型
 * 
 * @author yuxiaobei
 * 
 */
@SuppressWarnings({"serial","rawtypes"})
public class Pager implements java.io.Serializable 
{

	private Long total = 0L;
	private List rows = new ArrayList();

	public Long getTotal() 
	{
		return total;
	}

	public void setTotal(Long total) 
	{
		this.total = total;
	}

	public List getRows() 
	{
		return rows;
	}

	public void setRows(List rows) 
	{
		this.rows = rows;
	}

}
