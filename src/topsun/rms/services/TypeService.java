package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.Type;

@Service("typeService")
@Transactional
public class TypeService extends BaseService<Type>{

	@Resource(name = "typeDao")  
	public void setDao(IBaseDao<Type> dao) 
	{  
		super.setDao(dao);  
	}
}
