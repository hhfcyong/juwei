package topsun.rms.entity;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

/**
 * 店铺视图 
 * 
 * @author yuxiaobei
 *
 */
@SuppressWarnings("serial")
public class VShop implements java.io.Serializable
{
	private String id;
	private String name;
	
	private String code;
	private String sname;
	private String shoptype;
	private String managemode;
	private String oranizationid;
	private String address;
	private String phone;
	private String contacts;
	private Date beginmanagedate;
	private String bank;
	private String bankaccount;
	
	private String barea;
	private String province;
	private String city;
	private String county;
	
	private String bareaname;
	private String provincename;
	private String cityname;
	private String countyname;
	private String oranizationname;

	public String getId() {
		if (!StringUtils.isBlank(this.id)) {
			return this.id;
		}
		return UUID.randomUUID().toString();
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBarea() {
		return barea;
	}
	public void setBarea(String barea) {
		this.barea = barea;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getShoptype() {
		return shoptype;
	}
	public void setShoptype(String shoptype) {
		this.shoptype = shoptype;
	}
	public String getManagemode() {
		return managemode;
	}
	public void setManagemode(String managemode) {
		this.managemode = managemode;
	}
	
	public String getOranizationid() {
		return oranizationid;
	}
	public void setOranizationid(String oranizationid) {
		this.oranizationid = oranizationid;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContacts() {
		return contacts;
	}
	public void setContacts(String contacts) {
		this.contacts = contacts;
	}
	public Date getBeginmanagedate() {
		return beginmanagedate;
	}
	public void setBeginmanagedate(Date beginmanagedate) {
		this.beginmanagedate = beginmanagedate;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBankaccount() {
		return bankaccount;
	}
	public void setBankaccount(String bankaccount) {
		this.bankaccount = bankaccount;
	}
	public String getBareaname() {
		return bareaname;
	}
	public void setBareaname(String bareaname) {
		this.bareaname = bareaname;
	}
	public String getProvincename() {
		return provincename;
	}
	public void setProvincename(String provincename) {
		this.provincename = provincename;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	public String getCountyname() {
		return countyname;
	}
	public void setCountyname(String countyname) {
		this.countyname = countyname;
	}
	public String getOranizationname() {
		return oranizationname;
	}
	public void setOranizationname(String oranizationname) {
		this.oranizationname = oranizationname;
	}
	
}
