<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var ckstring='';
    var promotionType;
    var promotionRange;
    var submitaddForm = function($dialog, $grid,id) {		
		if ($('form').form('validate')) {
			var url;
			var promotionid=id;
			if (model == "edit") {
				url = topsun.contextPath + '/promotionpage/proapply/update.do';
				
			} else {
				url = topsun.contextPath + '/promotionpage/proapply/add.do';  
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			 
			var jsonString = jsonuserinfo.substring(0,jsonuserinfo.length-1);
			
			jsonString=jsonString+','+'"promotionid":'+promotionid+'}';
		  //  alert(jsonString);
		//	jsonString = jsonString +',' +promotionRange+'}';
			
		  // alert(jsonString);
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonString,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
		     		 $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		//alert($(':input[name="state1"]').attr("checked"));
	 
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/promotionpage/proapply/getbyid.do', {
				id : id,idtype:'1'
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'promotionid':obj.promotionid,
						'startdate2' : obj.startdate,
						'enddate2':obj.enddate,
						'starttime':obj.starttime,
						'endtime':obj.endtime,
						'state':obj.state
					});
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}	
			
				if(obj.state1=="on"){					
					   $(':input[name="state1"]').attr("checked", 'true');
		     	}
				if(obj.monday=="on"){
					 $(':input[name="monday"]').attr("checked", 'true');
				}
				if(obj.tuesday=="on"){					
					   $(':input[name="tuesday"]').attr("checked", 'true');
		     	}
				if(obj.wednesday=="on"){
					 $(':input[name="wednesday"]').attr("checked", 'true');
				}
				if(obj.thursday=="on"){					
					   $(':input[name="thursday"]').attr("checked", 'true');
		    	}
				if(obj.friday=="on"){
					 $(':input[name="friday"]').attr("checked", 'true');
				}
			if(obj.saturday=="on"){					
					   $(':input[name="saturday"]').attr("checked", 'true');
		     	}
				if(obj.sunday=="on"){
					 $(':input[name="sunday"]').attr("checked", 'true');
				}
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
	<div region="north" data-options="border:false"  style="height:500px;">
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>应用序号</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>开始日期</th>
					<td><input name="startdate2" class="easyui-datetimebox" data-options="required:true" /></td>
				</tr>
				<tr>
				    <th>结束日期</th>
				    <td><input name="enddate2" class="easyui-datetimebox" data-options="required:true" /></td>
				    <th>开始时间</th>
				    <td><!-- <input name="starttime" class="easyui-validatebox" data-options="required:true" /> --><input  name="starttime" class="easyui-timespinner"/></td>
				</tr>
				<tr>
				    <th>结束时间</th>
				 <td><!-- <input name="endtime" class="easyui-validatebox" data-options="required:true" /> --><input  name="endtime" class="easyui-timespinner"/></td>
				    <th>是否可用</th>
				    <td><input name="state1" type="checkbox"/></td>
				</tr>
				<tr>
				    <th>周一</th>
                   <td><input name="monday" type="checkbox"  /></td>      
                    <th>周二</th>
                    <td><input name="tuesday" type="checkbox" /></td>      
    				</tr>
				<tr>
				   <th>周三</th>
				   <td><input name="wednesday" type="checkbox"  /></td>
				   <th>周四</th>
				   <td><input name="thursday" type="checkbox" /></td>
				</tr>
				<tr>
				   <th>周五</th>
				   <td><input name="friday" type="checkbox" /></td>
				   <th>周六</th>
				   <td><input name="saturday" type="checkbox" /></td>
				</tr>
				<tr>
				   <th>周末</th>
				   <td><input name="sunday" type="checkbox" /></td>
				  
				</tr>
			</table>
		</form>
	</div>
	
</body>
</html>