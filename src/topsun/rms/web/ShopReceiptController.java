package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.ShopReceipt;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("datapage/shopreceipt")
public class ShopReceiptController extends BaseController<ShopReceipt> 
{
	@Resource(name = "shopReceiptService")  
	public void setService(IBaseService<ShopReceipt> Service1) 
	{  
		super.setService(Service1);		
	}
}