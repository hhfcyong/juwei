package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.StorageReceiptDetail;;

/**
 * 仓库收货明细  Dao
 * 
 * @author Tang
 *
 */

@Repository("storageReceiptDetailDao")  
@Transactional
public class StorageReceiptDetailDao extends BaseDao<StorageReceiptDetail> 
{

}
