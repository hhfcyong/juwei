package topsun.rms.web;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import topsun.rms.entity.ItemScheme;
import topsun.rms.services.IBaseService;

@Controller
@RequestMapping("datapage/itemscheme")
public class ItemSchemeController extends BaseController<ItemScheme> 
{
	@Resource(name = "itemSchemeService")  
	public void setService(IBaseService<ItemScheme> Service1) 
	{  
		super.setService(Service1);		
	}	
	
}