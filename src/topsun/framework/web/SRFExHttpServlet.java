package topsun.framework.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author yuxiaobei
 *
 */
public class SRFExHttpServlet extends HttpServlet 
{
	protected void addTimeOutHeaders(HttpServletResponse a)
	{
	    a.setDateHeader("Expires", System.currentTimeMillis());
	}
}
