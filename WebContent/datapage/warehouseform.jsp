<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var departmentid;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/warehouse/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/warehouse/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			
			if(typeof(departmentid)!="undefined" && departmentid!='') {
				jsonuserinfo = jsonuserinfo.substring(0,jsonuserinfo.length-1);
				jsonuserinfo =  jsonuserinfo  +',' +departmentid+'}';
			}
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
		     		  $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		
		$('#department').combotree({
			onSelect:function(row){
				departmentid = '"oranization":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/datapage/warehouse/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'code':obj.code,
						'phone':obj.phone,
						'address':obj.address
						
					});
				}
				if(obj.oranization !=null){
					$('#department').combotree('setValue', obj.oranization.id );
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
		<form method="post" class="form">
			<table class="table" style="width: 100%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>仓库标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>仓库名称</th>
					<td><input name="name" class="easyui-validatebox" data-options="required:true" /></td>
				</tr>
				<tr>
					<th>仓库编码</th>
					<td><input name="code" readonly="readonly" /></td>
					<th>所属部门</th>
					<td><select id="department" class="easyui-combotree" data-options="editable:false,idField:'id',textField:'text',parentField:'pid',url:'<%=contextPath%>/imsbase/ora/getTreeOranization.do'" style="width: 155px;"></select><img class="iconImg ext-icon-cross" onclick="$('#department').combotree('clear');departmentid='';" title="清空" /></td>
				</tr>
				<tr>
					<th>电话</th>
					<td><input name="phone"   /></td>
					<th>地址</th>
					<td><input name="address" class="easyui-validatebox"/></td>
				</tr>
			</table>
		</form>
	
</body>
</html>