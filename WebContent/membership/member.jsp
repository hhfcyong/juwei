<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="topsun.rms.utility.SecurityUtil"%>
<%
String contextPath = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
SecurityUtil securityUtil = new SecurityUtil(session);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <title>会员管理</title>
   <jsp:include page="../include.jsp"></jsp:include>
	<script type="text/javascript">
	
		var addFun = function() {
			var dialog = topsun.modalDialog({
				width : 800,
				height : 680,
				title : '添加',
				iconCls : 'icon-add',
				url : topsun.contextPath + '/membership/memberform.jsp',
				buttons : [ {
					text : '添加',
					iconCls : 'icon-ok',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.submitForm(dialog, $('#dg'));
					}
				} ,
				{
					text : '关闭',
					iconCls : 'icon-cancel',
					handler : function() {
						dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $('#dg'));
					}
				}  ]
			});
		};
		var editFun = function() {
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length != 1){
				$.messager.show({
					title:'提示信息!',
					msg:'只能选择一行记录进行修改!'
				});
			}else{
			  var row = $('#dg').datagrid('getSelected');
			  if (row){
				  var dialog = topsun.modalDialog({
					    width : 800,
						height : 680,
						title : '编辑',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/membership/memberform.jsp?id=' + row.id,
						buttons : [ {
							text : '编辑',
							iconCls : 'icon-ok',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.submitForm(dialog, $('#dg'));
							}
						} ,
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $('#dg'));
							}
						}  ]
					});
			   }
			}
			
		};
		
		var viewFun = function() {
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length == 1){
			  var row = $('#dg').datagrid('getSelected');
			  if (row){
				  var dialog = topsun.modalDialog({
					    width : 800,
						height : 680,
						title : '查看',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/membership/memberform.jsp?id=' + row.id,
						buttons : [ 
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $('#dg'));
							}
						}  ]
					});
			   }
			}
			
		};
		var copyFun = function() {
			var arr =$('#dg').datagrid('getSelections');
			if(arr.length == 1){
			  var row = $('#dg').datagrid('getSelected');
			  if (row){
				  var dialog = topsun.modalDialog({
					    width : 800,
						height : 680,
						title : '复制资源信息',
						iconCls : 'icon-edit',
						url : topsun.contextPath + '/membership/memberform.jsp?showmodel=copy&id=' + row.id,
						buttons : [ {
							text : '添加',
							iconCls : 'icon-ok',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.submitForm(dialog, $('#dg'));
							}
						} ,
						{
							text : '关闭',
							iconCls : 'icon-cancel',
							handler : function() {
								dialog.find('iframe').get(0).contentWindow.cancleform(dialog, $('#dg'));
							}
						}  ]
					});
			   }
			}
			
		};
		
		function reload()
		{
			$('#dg').datagrid('load',topsun.serializeObject($('#mysearch')));
		}
		function formatGrade(val,row){if(val !=null) return val.name; else return ''; }
		function formatSex(val,row){
			if(val !=null){
				 if(val == '0') return '女';
				 else return '男';
			}
			else return ''; 
		}
		
		function onDblClickRow(rowIndex, rowData){
			//topsun.editFun($('#dg'), '/membership/memberform.jsp');
			editFun();
		}
		
		$(function(){
			
		});
		
	</script>
  </head>
  
  <body class="easyui-layout" style="width: 100%;height:100%"  >
	
	<div region="north" style="padding: 5px 5px 0px 5px;" border="false" >
		<fieldset>
			<legend>信息查询</legend>
			<form id="mysearch" method="post">
					会员名:<input name="QUERY_t#name_S_LK" class="easyui-validatebox"  value="" />
					<a id="searchbtn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" onclick="reload()">查询</a> 
					<a id="clearbtn" class="easyui-linkbutton" data-options="iconCls:'yxib-icon-clear'" onclick="javascript:$('#mysearch').form('clear');;reload();" >清空</a>
			</form>
		</fieldset>
	</div>
	<div region="center"  onselectstart="return false;" style="-moz-user-select:none;padding: 0px 5px 5px 5px;" border="false">
		<table id="dg" title="会员管理" class="easyui-datagrid" url="member/list.do"
		        iconCls="yxb-icon datagrid-head"  SelectOnCheck = "false" 
		        CheckOnSelect = "false" singleSelect="true" pageSize="20" pageNumber="1"
				 fit="true" striped="true"
				sortName="id" sortOrder="asc"
				toolbar="#toolbar" pagination="true" data-options="onDblClickRow:onDblClickRow"
				rownumbers="true" fitColumns="false"  
				loadMsg="数据正在加载,请耐心的等待..." >
			<thead frozen="true">
				<tr>
					<th field="ck" checkbox="true"></th>
				</tr>
			</thead>
			<thead>
				<tr>
				    <th field="code" width="100">会员编号</th>
					<th field="name" width="100">会员名</th>
					<th field="sex" width="50" formatter="formatSex" >性别</th>
				    <th field="birthday" width="70" >生日</th>
					<th field="agegroup" width="100">年龄段</th>
					<th field="mobile" width="80">手机号</th>
					<th field="phone" width="80">固定电话</th>
					<th field="qq" width="100">QQ</th>
					<th field="microletter" width="100">微信</th>
					<th field="othercontactway" width="100">其他联系方式</th>
					<th field="address" width="150">地址</th>
					<th field="email" width="150">电子邮箱</th>
					<th field="idnumber" width="100">身份证号码</th>
					
					<th field="createora" width="80">办理机构</th>
					<th field="createuser" width="80">办理人</th>
					<th field="createdate" width="70">办理日期</th>
					<th field="membergrade" width="50" formatter="formatGrade">等级</th>
					<th field="point" width="80">积分</th>
				</tr>
			</thead>
		</table>
	</div>
	
	<div id="toolbar">
		<table>
			<tr>
				<%if (securityUtil.havePermission("usermanager.add")) {%>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addFun()">新增</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usermanager.edit")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editFun()">编辑</a></td>
				<%}%>
				
				<%if (securityUtil.havePermission("usermanager.delete")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="topsun.removeFun($('#dg'),'/membership/member/remove.do')">删除</a></td>
				<%}%>
				<%if (securityUtil.havePermission("usermanager.view")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon view" plain="true" onclick="viewFun()">查看</a></td>
				<%}%>
				<%if (securityUtil.havePermission("member.add")) {%>
				<td><div class="datagrid-btn-separator"></div></td>
				<td><a href="#" class="easyui-linkbutton" iconCls="yxb-icon copy" plain="true" onclick="copyFun()">复制</a></td>
				<%}%>
			</tr>
		</table>
		
	</div>
	
  </body>
</html>
