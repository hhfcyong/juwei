package topsun.rms.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import topsun.rms.common.RmsGlobal;
import topsun.rms.model.RetJson;
import topsun.rms.model.SessionInfo;
import topsun.rms.utility.WriteJsonUtil;


/**
 * Session 拦截器
 * 
 * @author yuxiaobei
 *
 */
public class SessionInterceptor extends HandlerInterceptorAdapter 
{
	
	private void writeNoSesion(String msg, HttpServletRequest request, HttpServletResponse response)
	{
		RetJson retJson = new RetJson();
		retJson.setMsg(msg);
		WriteJsonUtil.writeJsonByFilter(retJson, null, null, request, response);
	}
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String strUri = request.getRequestURI().substring(request.getContextPath().length());
		if(RmsGlobal.doNotNeedSessionUri.contains(strUri)) return true;
		
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute("sessionInfo");
		if (sessionInfo == null) 
		{
			String msg = "您还没有登录或登录已超时，请重新登录，然后再刷新本功能！";
			writeNoSesion(msg,request,response);
			return false;
		}
		else
		{
//			if (!sessionInfo.getUrlResources().contains(request.getRequestURI().substring(request.getContextPath().length())))
//			{
//				String msg = "您无当前功能操作权限";
//				writeNoSesion(msg,request,response);
//				return false;
//			}
		}
		return true;
    }
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
