package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.ShopReceiptDetail;

/**
 * 门店收货明细  Dao
 * 
 * @author Tang
 *
 */
@Repository("shopReceiptDetailDao")  
@Transactional
public class ShopReceiptDetailDao extends BaseDao<ShopReceiptDetail>{

}
