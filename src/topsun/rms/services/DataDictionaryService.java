package topsun.rms.services;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.framework.utility.HqlFilter;
import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.DataDictionary;

/**
 * 数据字典 服务实现
 * 
 * @author yuxiaobei
 *
 */
@Service("dataDictionaryService")
public class DataDictionaryService extends BaseService<DataDictionary> 
{
	@Resource(name = "dataDictionaryDao")  
	public void setDao(IBaseDao<DataDictionary> dao) 
	{  
		super.setDao(dao);  
	}

}
