package topsun.framework.web;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import topsun.framework.utility.StringHelper;

public class SRFExHttpServletContext implements ISRFExWebContext
{
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected Hashtable<String, String> paramList = new Hashtable<>();
	protected ServletContext servletContext;

	public SRFExHttpServletContext(HttpServletRequest request,HttpServletResponse response,ServletContext servletContext)
	{
		this.request = request;
		this.response = response;
		this.servletContext = servletContext;
		
		ParseRequest();
	}
	private void ParseRequest()
	{
		Enumeration enu=request.getParameterNames();  
		while(enu.hasMoreElements())
		{  
			String paraName=(String)enu.nextElement();  
			SetParamValue(paraName, request.getParameter(paraName));
		}  
	}
	@Override
	public Object GetSessionValue(String paramString)
	{
		 return request.getSession().getAttribute(paramString);
	}

	@Override
	public String getCurPagePath() 
	{
		return request.getRequestURI();
	}

	@Override
	public void RemoveParam(String paramString) 
	{
		if (paramList.containsKey(paramString.toUpperCase()))
		      paramList.remove(paramString.toUpperCase());
	}

	@Override
	public Object GetGlobalValue(String paramString) 
	{
		return getServletContext().getAttribute(paramString);
	}

	@Override
	public void SetGlobalValue(String paramString, Object paramObject)
	{
		if (!StringHelper.IsNullOrEmpty(paramString.trim()))
	    {
			getServletContext().getAttribute(paramString);
	    }
	}

	@Override
	public void SetSessionValue(String paramString, Object paramObject)
	{
		request.getSession().setAttribute(paramString, paramObject);
	}

	@Override
	public String GetPostValue(String paramString) 
	{
		
		if (!StringHelper.IsNullOrEmpty(request.getParameter(paramString)))
		{
			 return request.getParameter(paramString);
		}
		return "";
		
	}

	@Override
	public Hashtable<String, String> GetParams() 
	{
		return paramList;
	}

	@Override
	public void SetParamValue(String paramString1, String paramString2) 
	{
		if (!StringHelper.IsNullOrEmpty(paramString1.trim()) && !paramList.containsKey(paramString1.trim()))
	    {
	      paramList.put(paramString1.toUpperCase(), paramString2);
	    }
	}
	

	@Override
	public String GetParamValue(String paramString) 
	{
		if (paramList.containsKey(paramString.toUpperCase()))
	    {
	      return ((String)paramList.get(paramString.toUpperCase())).toString();
	    }
		
		return "";
	}
	
	 public ServletContext getServletContext()
	 {
	    return servletContext;
	 }

}
