<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    var membercardtype;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/membership/membercard/update.do';
				
			} else {
				url = topsun.contextPath + '/membership/membercard/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			
			if(typeof(membercardtype)!="undefined" && membercardtype!='') {
				jsonuserinfo = jsonuserinfo.substring(0,jsonuserinfo.length-1);
				jsonuserinfo =  jsonuserinfo  +',' +membercardtype+'}';
			}
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
		     		  $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
		
		$('#selmembercardtype').combobox({
			onSelect:function(row){
				membercardtype = '"membercardtype":{"id":"'+ row.id+'"}' ;
			}
		});
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/membership/membercard/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'code' : obj.code,
						'issuedate' : obj.issuedate,
						'enable' : obj.enable,
						'opendate' : obj.opendate,
						'closedate' : obj.closedate,
						'pw' : obj.pw,
						'point' : obj.point
					});
					if(obj.membercardtype !=null ){
						$('#selmembercardtype').combobox('select', obj.membercardtype.id );
					}
					if(obj.member !=null ){
						$('#member').val(obj.member.code);
					}
				}
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
		<form method="post" class="form">
			<table class="table" style="width: 90%;font-size: 12px; margin: 10px" cellpadding="5" >
				<tr>
					<th>会员卡标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>会员卡名称</th>
					<td><input name="name" class="easyui-validatebox"  /></td>
				</tr>
				<tr>
					<th>会员卡卡号</th>
					<td><input name="code" class="easyui-validatebox" data-options="required:true" /></td>
					<th>会员卡类型</th>
					<td><select id = "selmembercardtype" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/membership/membercardtype/listall.do',panelHeight:'auto'" style="width: 155px;  "></select></td>
				</tr>
				<tr>
				    <th>发卡日期</th>
					<td><input name="issuedate" class="easyui-datebox" /></td>
					<th>状态</th>
					<td>
						<select id = "selenable" name="enable" class="easyui-combobox" data-options="required:true,editable:false,panelHeight:'auto'" style="width: 155px;">
							<option value="1">启用</option>
	        				<option value="0">停用</option>
        				</select>
        		   </td>
				</tr>
				
				<tr>
				    <th>开始日期</th>
					<td><input name="opendate" class="easyui-datebox" readonly="readonly" /></td>
					<th>截止日期</th>
					<td><input name="closedate" class="easyui-datebox" readonly="readonly" /></td>
				</tr>
				
				<tr>
				    <th>会员卡编号</th>
					<td><input id="member" class="easyui-validatebox" readonly="readonly" /></td>
				
					<th>密码</th>
					<td><input name="pw"  type="password" class="easyui-validatebox"   /></td>
				</tr>
				
				<tr>
				    <th>积分</th>
					<td><input name="point" class="easyui-validatebox" readonly="readonly" /></td>
				</tr>
				
			</table>
		</form>
	
</body>
</html>