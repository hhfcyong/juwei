<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<%
	String id = request.getParameter("id");
	if (id == null) {
		id = "";
	}
	
	String showmodel = request.getParameter("showmodel");
	if (showmodel == null) {
		showmodel = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<jsp:include page="../include.jsp"></jsp:include>
<script type="text/javascript">
    
    var model = "add";
    var entityid;
    
    var submitForm = function($dialog, $grid) {
		if ($('form').form('validate')) {
			var url;
			if (model == "edit") {
				url = topsun.contextPath + '/datapage/item/update.do';
				
			} else {
				url = topsun.contextPath + '/datapage/item/add.do';
			}
			var jsonuserinfo = $.toJSON(topsun.serializeObject($('form')));
			jQuery.ajax( {   
		          type : 'POST',   
		          contentType : 'application/json',   
		          url : url,   
		          data : jsonuserinfo,   
		          dataType : 'json',   
		          success : function(data) {   
		        	  $dialog.dialog('destroy');	
					  $grid.datagrid('reload');
		          },   
		          error : function(data) {   
		        	  $.messager.show({
							title: 'Error',
							msg: 'Error'
						});
		          }   
		        });   
		}
	};
	var cancleform = function($dialog, $grid) {
		 $dialog.dialog('destroy');	
		 $grid.datagrid('reload');
	};
	
	//initialize
	$(function() {
	    entityid = '<%=id%>';
		if(entityid != ""){
			model = "edit";
		}
		
		var showmodel = '<%=showmodel%>';
		if(showmodel != ""){
			model = showmodel;
		}
		
		if(model =="edit"){
			$(':input[name="id"]').attr("readonly","readonly");
		}
		
		if (entityid.length > 0) {
			getById(entityid);
		}
	});
	//get entity by entityid
	 function getById(id)
     {
		 $.messager.progress({
				text : '数据加载中....'
			});
		 $.post(topsun.contextPath + '/datapage/item/getbyid.do', {
				id : id
			}, function(result) {
				var obj = result.obj;
				if (obj.id != undefined) {
					$('form').form('load', {
						'id' : obj.id,
						'name' : obj.name,
						'itemcode' : obj.itemcode,
						'sname' : obj.sname,
						'specification' : obj.specification,
						'expirationdate' : obj.expirationdate,
						'costprice' : obj.costprice,
						'wholesaleprice' : obj.wholesaleprice,
						'saleprice' : obj.saleprice,
						'skucode' : obj.skucode,
						'description' : obj.description
					});
				}
				$('#selunit').combobox('select', obj.unit );
				$('#selitemtype').combobox('select', obj.itemtype );
				$('#selenable').combobox('select', obj.enable );
				
				if(model =="copy"){
					$(':input[name="id"]').val('');
					entityid = "";
				}
				
				
			}, 'json');
		 $.messager.progress('close');
     }
	 
	
</script>
</head>
<body  class="easyui-layout" style="width: 100%;height:100%" >
		<form method="post" class="form">
			<table class="table" style="width: 95%;font-size: 12px; margin: 10px"  >
				<tr>
					<th>单品标识</th>
					<td><input name="id"  value="<%=id%>" /></td>
					<th>单品名称</th>
					<td><input name="name" class="easyui-validatebox" /></td>
				</tr>
				<tr>
					<th>商品编码</th>
					<td><input name="itemcode" class="easyui-validatebox"  /></td>
					<th>简称</th>
					<td><input name="sname" class="easyui-validatebox"  /></td>
				</tr>
				<tr>
					<th>规格</th>
					<td><input name="specification" class="easyui-validatebox" /></td>
					<th>保质期限</th>
					<td><input name="expirationdate" class="easyui-validatebox" /></td>
				</tr>
				<tr>
					<th>计量单位</th>
					<td><select id = "selunit" name="unit" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/dic/unit/listall.do',panelHeight:'auto'" style="width: 155px;"></select></td>
					<th>所属分类</th>
					<td><select id = "selitemtype" name="itemtype" class="easyui-combobox" data-options="required:true,editable:false,valueField:'id',textField:'name',url:'<%=contextPath%>/datapage/itemtype/listall.do',panelHeight:'auto'" style="width: 155px;  "></select></td>
				</tr>
				<tr>
					<th>成本价</th>
					<td><input name="costprice"  class="easyui-numberbox"  /></td>
					<th>批发价</th>
					<td><input name="wholesaleprice" class="easyui-numberbox"  /></td>
				</tr>
				<tr>
					<th>零售价</th>
					<td><input name="saleprice"  class="easyui-numberbox" /></td>
					<th>SKU</th>
					<td><input name="skucode" class="easyui-validatebox"   /></td>
				</tr>
				<tr>
					<th>状态</th>
					<td>
						<select id = "selenable" name="enable" class="easyui-combobox" data-options="required:true" style="width: 155px;">
							<option value="1">是</option>
	        				<option value="0">否</option>
        				</select>
        			</td>
					<th>商品描述</th>
					<td><textarea name="description"></textarea></td></td>
				</tr>
			</table>
		</form>
	
</body>
</html>