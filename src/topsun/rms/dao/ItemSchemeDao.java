package topsun.rms.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import topsun.rms.entity.ItemScheme;

/**
 * 预定义方案  Dao
 * 
 * @author Tang
 *
 */

@Repository("itemSchemeDao")  
@Transactional
public class ItemSchemeDao extends BaseDao<ItemScheme> {

}
