package topsun.rms.entity;

import java.math.BigDecimal;

/**
 * 仓库发货明细 实体模型
 * @author Tang
 *
 */
@SuppressWarnings("serial")
public class StorageDeliverDetail {

	private int id;
	private Item item;
	private String itemName;
	private BigDecimal orderQuantity;
	private BigDecimal deliverQuantity;
	private String unitId;
	private BigDecimal price;
	private BigDecimal amount;
	private StorageDeliver storageDeliver;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public BigDecimal getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	public BigDecimal getDeliverQuantity() {
		return deliverQuantity;
	}
	public void setDeliverQuantity(BigDecimal deliverQuantity) {
		this.deliverQuantity = deliverQuantity;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public StorageDeliver getStorageDeliver() {
		return storageDeliver;
	}
	public void setStorageDeliver(StorageDeliver storageDeliver) {
		this.storageDeliver = storageDeliver;
	}
}
