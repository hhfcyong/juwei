package topsun.rms.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.model.Menu;
import topsun.rms.model.SessionInfo;
import topsun.rms.model.Tree;
import topsun.rms.model.ZTree;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IResourceService;
import topsun.rms.utility.BeanUtils;

/** 
 * 资源 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/res")
public class ResourceController extends BaseController<topsun.rms.entity.Resource>
{
	@Resource(name = "resourceService")  
	public void setService(IBaseService<topsun.rms.entity.Resource> Service1) 
	{  
		super.setService(Service1);
	}
	
	/**
	 * Easyui tree menu
	 * 
	 * @param request
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/getallrestree", method = RequestMethod.POST)
	protected void getAllResTree(HttpServletRequest request, HttpServletResponse response)
	{
		IResourceService resourceService = (IResourceService)service;
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addSort("sortno");
		hqlFilter.addOrder("asc");
		
		List<topsun.rms.entity.Resource> resources = resourceService.getMenu(hqlFilter);
		List<Tree> tree = new ArrayList<Tree>();
		for (topsun.rms.entity.Resource resource : resources) 
		{
			Tree node = new Tree();
			BeanUtils.copyNotNullProperties(resource, node);
			node.setText(resource.getName());
			Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("url", resource.getUrl());
			node.setAttributes(attributes);
			tree.add(node);
		}
		
		writeJson(tree, request, response);
	}
	
	/**
	 * Easyui tree menu
	 * 
	 * @param request
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/getMainMenu", method = RequestMethod.POST)
	protected void getMainMenu(HttpServletRequest request, HttpServletResponse response)
	{
		IResourceService resourceService = (IResourceService)service;
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_t#resourcetype#id_S_EQ", "1");
		hqlFilter.addSort("sortno");
		hqlFilter.addOrder("asc");
		
		List<topsun.rms.entity.Resource> resources = resourceService.getMenu(hqlFilter);
		List<Tree> tree = new ArrayList<Tree>();
		for (topsun.rms.entity.Resource resource : resources) 
		{
			Tree node = new Tree();
			BeanUtils.copyNotNullProperties(resource, node);
			node.setText(resource.getName());
			Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("url", resource.getUrl());
			node.setAttributes(attributes);
			tree.add(node);
		}
		
		writeJson(tree, request, response);
	}
	/**
	 * Ztree menu
	 * 
	 * @param request
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/getZTreeMainMenu", method = RequestMethod.GET)
	protected void getZTreeMainMenu(HttpServletRequest request, HttpServletResponse response)
	{
		IResourceService resourceService = (IResourceService)service;
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_t#resourcetype#id_S_EQ", "1");
		hqlFilter.addSort("sortno");
		hqlFilter.addOrder("asc");
		
		List<topsun.rms.entity.Resource> resources = resourceService.getMenu(hqlFilter);
		List<ZTree> tree = new ArrayList<ZTree>();
		for (topsun.rms.entity.Resource resource : resources) 
		{
			ZTree node = new ZTree();
			BeanUtils.copyNotNullProperties(resource, node);
			if(StringUtils.isNotBlank(resource.getPid()))
			{
				node.setPId(resource.getPid());
			}
			node.setOpen(true);
			tree.add(node);
		}
		
		writeJson(tree, request, response);
	}
	
	/**
	 * 首页菜单  不支持三级菜单
	 * 
	 * @param request
	 * 
	 * @param response
	 */
	@RequestMapping(value = "/getMenu", method = RequestMethod.GET)
	protected void getMenu(HttpServletRequest request, HttpServletResponse response)
	{
		
		IResourceService resourceService = (IResourceService)service;
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute("sessionInfo");

		HqlFilter hqlFilter = new HqlFilter(request);
		if(!sessionInfo.getUser().getId().equals("system"))
		{
			hqlFilter.addFilter("QUERY_user#id_S_EQ", sessionInfo.getUser().getId());
		}
		hqlFilter.addFilter("QUERY_t#resourcetype#id_S_EQ", "1");
		hqlFilter.addSort("sortno");
		hqlFilter.addOrder("asc");
		
		List<topsun.rms.entity.Resource> resources ;
		if(!sessionInfo.getUser().getId().equals("system"))
		{ 
			resources = resourceService.getMainMenu(hqlFilter);
		}
		else
		{
			resources = resourceService.getMenu(hqlFilter);
		}
		List<Menu> tree = new ArrayList<Menu>();
		
		Hashtable<String,Menu> menus = new Hashtable<String,Menu>();
		
		for (topsun.rms.entity.Resource resource : resources) 
		{
			Menu node = new Menu();
			BeanUtils.copyNotNullProperties(resource, node);
			if(StringUtils.isNotBlank(resource.getIconCls()))
			{
				node.setIcon(resource.getIconCls());
			}
			if(!StringUtils.isNotBlank(resource.getPid()))
			{
				menus.put(node.getId(), node);
			}
			else
			{
				if(menus.containsKey(node.getPid()))
				{
					(menus.get(node.getPid())).getMenus().add(node);
				}
				else
				{
					tree.add(node);
				}
			}
		}
		for(Menu menu : tree)
		{
			if(menus.containsKey(menu.getPid()))
			{
				(menus.get(menu.getPid())).getMenus().add(menu);
			}
		}
		List<Menu> lmenu = new ArrayList<Menu>(menus.values());
		Collections.sort(lmenu, new Comparator<Menu>() 
				{// 排序
					@Override
					public int compare(Menu o1, Menu o2) {
						if (o1.getSortno() == null) {
							o1.setSortno(1000L);
						}
						if (o2.getSortno() == null) {
							o2.setSortno(1000L);
						}
						return o1.getSortno().compareTo(o2.getSortno());
					}
				});
		
		writeJson(lmenu, request, response);
	}
	@RequestMapping(value = "/getbyroleid", method = RequestMethod.POST)
	public void getMenuByRoleId(HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_role#id_S_EQ", id.toString());
		List<topsun.rms.entity.Resource> resources = ((IResourceService) service).findResByFilter(hqlFilter);
		writeJson(resources,request,response);
		
	}
	
	@RequestMapping(value = "/treeGrid", method = RequestMethod.POST)
	public void treeGrid(HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		
		HqlFilter hqlFilter = new HqlFilter();
		hqlFilter.addFilter("QUERY_role#id_S_EQ", id.toString());
		List<topsun.rms.entity.Resource> resources = ((IResourceService) service).findResByFilter(hqlFilter);
		writeJson(resources,request,response);
		
	}
	@Override
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
        title = "资源表";
        hearders = new String[] {"编号", "名称", "编码", "资源图片", "资源路径", "排序值"};//表头数组
        fields = new String[] {"id", "name","code","iconCls","url","sortno"};//对象属性数组
        super.exportExcel(request, response);
	}
	

}
