package topsun.rms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import topsun.framework.utility.HqlFilter;
import topsun.rms.entity.UserRole;
import topsun.rms.services.IBaseService;
import topsun.rms.services.IUserRoleService;

/**
 * 用户角色 Controller
 * 
 * @author yuxiaobei
 *
 */
@Controller
@RequestMapping("imsbase/role")
public class UserRoleController extends BaseController<UserRole> 
{
	@Resource(name = "userRoleService")  
	public void setService(IBaseService<UserRole> Service1) 
	{  
		super.setService(Service1);
	}
	
	@RequestMapping(value = "/getbyuserid", method = RequestMethod.POST)
	public void getRoleByUserId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_user#id_S_EQ", id);
		List<UserRole> roles = ((IUserRoleService) service).findRoleByFilter(hqlFilter);
		writeJson(roles,request,response);
	}
	
	@RequestMapping(value = "/getbyusergroupid", method = RequestMethod.POST)
	public void getRoleByUserGroupId( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		HqlFilter hqlFilter = new HqlFilter(request);
		hqlFilter.addFilter("QUERY_usergroup#id_S_EQ", id);
		List<UserRole> roles = ((IUserRoleService) service).findRoleByFilterGroup(hqlFilter);
		writeJson(roles,request,response);
	}
	
	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public void addUser( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IUserRoleService customService = (IUserRoleService)service;
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.addUser(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
	public void deleteUser( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IUserRoleService customService = (IUserRoleService)service;
		if((getId()!=null) && StringUtils.isNotBlank(getIds()))
		{
			customService.deleteUser(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/addres", method = RequestMethod.POST)
	public void addRes( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IUserRoleService customService = (IUserRoleService)service;
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.addRes(id, ids);
		}
		
		excuteSuccess(request,response);
	}
	
	@RequestMapping(value = "/deleteres", method = RequestMethod.POST)
	public void deleteRes( HttpServletRequest request, HttpServletResponse response)
	{
		parseRequest(request);
		IUserRoleService customService = (IUserRoleService)service;
		if( StringUtils.isNotBlank(id) && StringUtils.isNotBlank(ids))
		{
			customService.deleteRes(id, ids);
		}
		
		excuteSuccess(request,response);
	}
}
