package topsun.rms.ctrl;

import java.util.List;

import topsun.framework.utility.StringHelper;
import topsun.rms.dao.UserDAOEx;
import topsun.rms.entity.User;

public class DemoServiceInstance extends ServerInstance 
{

	@Override
	protected MessagePackage OnProcessRemoteAction(IRemoteAction iRemoteActionContext) throws Exception
	{
		if (StringHelper.Compare(iRemoteActionContext.getAction(), RemoteActions.RMSTEST, true) == 0)
		{
			return OnRmsTest(iRemoteActionContext);
		}
		
		throw new Exception(String.format("没有处理的远程请求[%1$s]", iRemoteActionContext.getAction()));
	}
	
	protected MessagePackage OnRmsTest(IRemoteAction iRemoteActionContext)
	{
		MessagePackage messagePackage = new MessagePackage();
		
		UserDAOEx dao = new UserDAOEx();
		List<User> list = dao.findAll();
		for(User user : list)
		{
			messagePackage.setExtInfo(user.getId(),user.getName());
		}
		

		return messagePackage;
	}
}
