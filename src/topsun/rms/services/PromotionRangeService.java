package topsun.rms.services;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import topsun.rms.dao.IBaseDao;
import topsun.rms.entity.PromotionRange;;;

/**
 * 资源类型服务 实现
 * 
 * @author zy
 *
 */
@Service("promotionRangeService")
public class PromotionRangeService extends BaseService<PromotionRange> 
{
	@Resource(name = "promotionRangeDao")  
	public void setDao(IBaseDao<PromotionRange> dao) 
	{  
		super.setDao(dao);  
	}
}
