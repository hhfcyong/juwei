<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<title>系统登录</title>
<jsp:include page="include.jsp"></jsp:include>
<script type="text/javascript">
	$(function() {

		var loginFun = function() {
			var loginTabs = $('#loginTabs').tabs('getSelected');//当前选中的tab
			var $form = loginTabs.find('form');//选中的tab里面的form
			var jsonuserinfo = $.toJSON(topsun.serializeObject($form)); 
			
			if ($form.length == 1 && $form.form('validate')) {
				$('#loginBtn').linkbutton('disable');
				jQuery.ajax( {   
			          type : 'POST',   
			          contentType : 'application/json',   
			          url : topsun.contextPath + '/imsbase/user2/login.do',   
			          data : jsonuserinfo,   
			          dataType : 'json',   
			          success : function(data) {   
			        	  location.replace(topsun.contextPath + '/index.jsp');
			          },   
			          error : function(data) {   
			        	  $.messager.show({
								title: 'Error',
								msg: 'Error'
							});
			        	  $('#loginBtn').linkbutton('enable');
			          }   
			        });   
			}
		};
		

		$('#loginDialog').show().dialog({
			resizable : false,
			modal : false,
			closable : false,
			iconCls : 'ext-icon-lock_open',
			buttons : [ {
				id : 'loginBtn',
				text : '登录',
				handler : function() {
					loginFun();
				}
			},{
				id : 'resetBtn',
				text : '重置',
				handler : function() {
					var loginTabs = $('#loginTabs').tabs('getSelected');//当前选中的tab
					var $form = loginTabs.find('form');//选中的tab里面的form
					$form[0].reset();  
				}
			}  ],
			onOpen : function() {
				$('form :input:first').focus();
				$('form :input').keyup(function(event) {
					if (event.keyCode == 13) {
						loginFun();
					}
				});
			}
		});
	});
</script>
</head>
<body>
	

	<div id="loginDialog" title="项目管理系统" style="display: none; width: 320px; height: 180px; overflow: hidden;">
		<div id="loginTabs" class="easyui-tabs" data-options="fit:true,border:false">
			<div title="用户登录" style="overflow: hidden; padding: 10px;">
				<form method="post" class="form">
					<table class="table" style="width: 100%; height: 100%;">
						<tr>
							<th width="50">登录名</th>
							<td><input name="id" class="easyui-validatebox" data-options="required:true"  style="width: 210px;" /></td>
						</tr>
						<tr>
							<th>密码</th>
							<td><input name="pwd" type="password" class="easyui-validatebox" data-options="required:true"  style="width: 210px;" /></td>
						</tr>
					</table>
				</form>
			</div>
			
		</div>
	</div>
</body>
</html>